﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class GameManager : MonoBehaviour {

    private const int TAG_SEQUENCE = 0;

    private void InitPush() {
#if UNITY_ANDROID || UNITY_IOS
        if (Application.isMobilePlatform) {
            JPush.JPushBinding.Init(name);
            List<string> tags = new List<string>();
            tags.Add(Localization.GetLanguage());
            tags.Add(Application.version);
            //tags.Add(SystemInfo.deviceModel);
            for (int i = 0; i < tags.Count; ++i) {
                Debug.Log((i + 1) + " tag: " + tags[i]);
            }
            JPush.JPushBinding.AddTags(TAG_SEQUENCE, tags);
#if UNITY_ANDROID
            JPush.JPushBinding.RequestPermission();
#endif
        }
#endif
    }

    private void CheckPush() {
        if (Application.isMobilePlatform) {
#if UNITY_ANDROID
            if (JPush.JPushBinding.IsPushStopped()) {
                Debug.Log("Resume push service");
                JPush.JPushBinding.ResumePush();
            }
#endif
        }
    }

    public void PushNotification(int id, string content, string title, int delay, object extra = null) {
        if (Application.isMobilePlatform) {
#if UNITY_ANDROID
            JPush.JPushBinding.ClearAllNotifications();
            JPush.JPushBinding.AddLocalNotification(0, content, title, id, delay, extra != null ? JsonUtility.ToJson(extra) : null);
#elif UNITY_IOS
            JPush.JPushBinding.ClearAllLocalNotifications();
            JPush.JPushBinding.SetLocalNotification(delay, content, 0, id.ToString());
#endif
        }
    }
}
