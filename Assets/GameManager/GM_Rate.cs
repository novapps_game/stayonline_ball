﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class GameManager : MonoBehaviour {

    private enum JumpStatus {
        Resumed,
        Jumping,
        Paused,
    }

    private JumpStatus rateStatus = JumpStatus.Resumed;
    private float rateTime;
    private int rateCount;

    public bool rated {
        get { return EzPrefs.GetInt(Application.version, 0) != 0; }
        set { EzPrefs.SetInt(Application.version, value ? 1 : 0); }
    }

    private System.Action onRateSucceeded;
    private System.Action onRateFailed;

    public bool CheckRate() {
        if (!rated && EzRemoteConfig.online && !string.IsNullOrEmpty(EzRemoteConfig.rateUrl) &&
            rateCount < EzRemoteConfig.rateMaxCount &&
            gameOverTimes >= EzRemoteConfig.rateFirst &&
            (gameOverTimes - EzRemoteConfig.rateFirst) % EzRemoteConfig.rateInterval == 0) {
            ++rateCount;
            OpenRate();
            return true;
        }
        return false;
    }

    public void OpenRate() {
#if UNITY_IOS
        EzNativeRate.NativeRate();
#else
        Panel.Open("RatePanel");
#endif
    }

    public void GotoRate(System.Action onRateSucceeded = null, System.Action onRateFailed = null) {
        if (string.IsNullOrEmpty(EzRemoteConfig.rateUrl)) return;
        this.onRateSucceeded = onRateSucceeded;
        this.onRateFailed = onRateFailed;
        rateStatus = JumpStatus.Jumping;
        EzAnalytics.LogEvent("Rate", "Goto");
        Application.OpenURL(EzRemoteConfig.rateUrl);
    }

    void UpdateRateStatus() {
        if (paused && rateStatus == JumpStatus.Jumping) {
            rateStatus = JumpStatus.Paused;
            rateTime = Time.realtimeSinceStartup;
        } else if (!paused && rateStatus == JumpStatus.Paused) {
            rateStatus = JumpStatus.Resumed;
            rateTime = Time.realtimeSinceStartup - rateTime;
            if (rateTime < EzRemoteConfig.rateMinTime) {
                OnRateFailed();
            } else {
                OnRateSucceeded();
            }
        }
    }

    void OnRateSucceeded() {
        rated = true;
        EzAnalytics.LogEvent("Rate", "Succeeded");
        if (onRateSucceeded != null) {
            onRateSucceeded.Invoke();
        }
    }

    void OnRateFailed() {
        EzAnalytics.LogEvent("Rate", "Failed");
        if (onRateFailed != null) {
            onRateFailed.Invoke();
        }
    }

}
