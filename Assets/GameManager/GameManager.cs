﻿using UnityEngine;

public partial class GameManager : MonoBehaviour {

    public static GameManager instance;

	public bool debug = true;
    public bool clearPrefs = false;

    public SystemLanguage testLanguage = SystemLanguage.Unknown;

    public static string uidParams { get { return "?uid=" + EzIds.deviceID + "&package=" + EzIds.appID; } }
    public static string idParams { get { return "?id=" + EzIds.deviceID + "&package=" + EzIds.appID; } }

    public static string channel {
        get {
#if _360
            return "360";
#elif BAIDU
            return "baidu";
#elif TENCENT
            return "tencent";
#elif HUAWEI
            return "huawei";
#elif XIAOMI
            return "xiaomi";
#elif VIVO
            return "vivo";
#elif OPPO
            return "oppo";
#elif WANDOUJIA
            return "wandoujia";
#elif GOOGLE_PLAY
            return "googleplay";
#else
            return "appstore";
#endif
        }
    }

    void Awake() {
		if (instance == null) {
			DontDestroyOnLoad(gameObject);
			instance = this;
            Initialize();
		} else if (instance != this) {
			Destroy(gameObject);
		}
	}

    void Initialize() {
		Debug.unityLogger.logEnabled = debug;
        if (clearPrefs) EzPrefs.DeleteAll();
        Localization.Init();
        EzImageLoader.Init();
        EzAnalytics.LogEvent("Channel", channel);
        StartCoroutine(EzRestApi.Post(promoRewardMarkUrl));
        StartCoroutine(EzRemoteConfig.Init(OnInitConfig));
        promoCounts = new int[EzRemoteConfig.promoUrls.Length];
#if UNITY_PURCHASING && USE_IAP
        InitIAP();
#endif
        InitLayers();
        InitGooglePlayGameService();
        Authenticate();
        LoadPrefabs();
        LoadSprites();
        LoadFonts();
        LoadTexts();
        LoadSounds();
        LoadPlayerData();
      
        AddSceneAction("Home", OnHome);
        Application.targetFrameRate = 60;
        InitPush();
        // Screen.sleepTimeout = SleepTimeout.NeverSleep;

        //System.DateTime currentTime = EzTime.GetNetworkTime();


        InitGuides();
    }

    public void ShowOfflinePanel() {
        System.DateTime currentTime = System.DateTime.Now;
        if (GameManager.instance.GetOfflineTime() == System.DateTime.MinValue) {
            GameManager.instance.SetOfflineTime(currentTime);
        } else {
            if ((currentTime - GameManager.instance.GetOfflineTime()).TotalMinutes >= 3) {
                Panel.Open("OfflinePanel");
            }
        }
        Debug.Log("Current time: " + currentTime.ToString());
    }


    public int defaultLayer;
	void InitLayers() {
		defaultLayer = LayerMask.NameToLayer("Default");
	}

    void OnInitConfig() {
		if (!EzRemoteConfig.noAds) {
            EzAds.Initialize(EzRemoteConfig.adInterstitialSources, EzRemoteConfig.adRewardedVideoSources);
		}
		promoCounts = new int[EzRemoteConfig.promoUrls.Length];
		StartCoroutine(EzImageLoader.CacheImages(EzRemoteConfig.promoImages));
		StartCoroutine(EzImageLoader.CacheImage(EzRemoteConfig.shareAppImage));
		StartCoroutine(EzImageLoader.CacheImage(EzRemoteConfig.shareScoreImage));

		if (EzRemoteConfig.online && currentScene == "Home") {
			OpenUpgrade();
		}
        //CheckShareResult();
    }

    void Update() {
        CheckGuides();
    }

    void OnApplicationFocus(bool hasFocus) {
        Debug.Log("OnApplicationFocus: " + hasFocus);
        if (hasFocus) {
            OnResume();
        } else {
            OnPause();
        }
    }

    void OnApplicationPause(bool pauseStatus) {
        Debug.Log("OnApplicationPause: " + pauseStatus);
    }

    public bool isQuiting { get; set; }
    void OnApplicationQuit() {
        Debug.Log("OnApplicationQuit");
        isQuiting = true;
    }

    private const string PAUSE_TIME = "PAUSE_TIME";
    public bool paused = false;

    void OnResume() {
        paused = false;
        UpdateRateStatus();
        //CheckPromoReward();
    }

    void OnPause() {
        paused = true;
        UpdateRateStatus();
    }

}
