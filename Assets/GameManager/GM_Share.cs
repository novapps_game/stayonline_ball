﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class GameManager : MonoBehaviour {

    [System.Serializable]
    public class ShareRewardData {
        public int code;
        public string msg;
    }

    [System.Serializable]
    public class ShareVirusData {
        public int code;
        public string msg;
        public ShareVirusResult res;
    }

    [System.Serializable]
    public class ShareVirusResult {
        public int total;
        public int today;
        public int gains;
    }

    public string shareRewardUrl { get { return EzRemoteConfig.shareRewardUrl + idParams; } }
    public string shareRewardCheckUrl { get { return EzRemoteConfig.shareRewardUrl + "/check" + idParams; } }

    public string shareVirusUrl { get { return EzRemoteConfig.shareVirusUrl + idParams; } }
    public string shareVirusCheckUrl { get { return EzRemoteConfig.shareVirusUrl + "/check" + idParams; } }
    public string shareVirusResetUrl { get { return EzRemoteConfig.shareVirusUrl + "/reset" + idParams; } }

    public ShareVirusResult shareVirusResult = new ShareVirusResult();

    public void ShareApp() {
        EzAnalytics.LogEvent("ShareApp", "Goto");
        string shareAppImageFile = EzImageLoader.GetCachedFile(EzRemoteConfig.shareAppImage);
        if (Localization.GetLanguage() == "CN" && !string.IsNullOrEmpty(shareAppImageFile)) {
            EzShareNative.ShareImage(shareAppImageFile,
                EzRemoteConfig.shareAppText + EzRemoteConfig.shareAppUrl, Application.installerName);
        } else {
            EzShareNative.ShareText(EzRemoteConfig.shareAppText, EzRemoteConfig.shareAppUrl, Application.installerName);
        }
    }

    public void OpenShareReward() {
        if (string.IsNullOrEmpty(EzRemoteConfig.shareRewardUrl)) return;
        Panel.Open("ShareRewardPanel");
    }

    public void ShareReward() {
        EzAnalytics.LogEvent("ShareReward", "Goto");
        EzShareNative.ShareText(EzRemoteConfig.shareRewardText, shareRewardUrl, Application.installerName);
    }

    public void OpenShareVirus() {
        if (string.IsNullOrEmpty(EzRemoteConfig.shareVirusUrl)) return;
        Panel.Open("ShareVirusPanel");
    }

    public void ShareVirus() {
        EzAnalytics.LogEvent("ShareVirus", "Goto");
        EzShareNative.ShareText(EzRemoteConfig.shareVirusText, shareVirusUrl, Application.installerName);
    }

    void CheckShareResult() {
        if (!string.IsNullOrEmpty(EzRemoteConfig.shareRewardUrl)) {
            StartCoroutine(EzRestApi.Get(shareRewardCheckUrl, (json) => {
                try {
                    ShareRewardData data = JsonUtility.FromJson<ShareRewardData>(json);
                    if (data.code == 0) {
                        OnShareRewardSucceeded();
                    }
                } catch (System.Exception ex) {
                    Debug.LogWarning("Check share reward error: " + ex.Message);
                }
            }));
        }
        if (!string.IsNullOrEmpty(EzRemoteConfig.shareVirusUrl)) {
            StartCoroutine(EzRestApi.Get(shareVirusCheckUrl, (json) => {
                try {
                    ShareVirusData data = JsonUtility.FromJson<ShareVirusData>(json);
                    if (data.code == 0) {
                        shareVirusResult = data.res;
                        StartCoroutine(EzRestApi.Post(shareVirusResetUrl, (result) => {
                            OnShareVirusSucceeded();
                        }));
                    }
                } catch (System.Exception ex) {
                    Debug.LogWarning("Check share virus error: " + ex.Message);
                }
            }));
        }
    }

    void OnShareRewardSucceeded() {
        // Todo: Unlock some skin for reward
        Debug.Log("gain skin from reward share");
    }

    void OnShareVirusSucceeded() {
        Debug.Log("gain coins from virus share: " + shareVirusResult.gains);
        GainCoins(shareVirusResult.gains, "ShareVirus");
    }

}
