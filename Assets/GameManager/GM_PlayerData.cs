﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MiniJSON;
using System;

public partial class GameManager : MonoBehaviour {

    public int testCoins = -1;
    public int initCoins = 1000;

    private static string LAST_SIGN_TIME = "last_sign_time";
    private const string PLAY_TIMES = "PLAY_TIMES";
    private const string NAME = "NAME";
    private const string COINS = "COINS";
    private const string SCORE = "SCORE";
    private const string BEST_SCORE = "BEST_SCORE";
    private const string UNLOCKED_LEVELS = "UNLOCKED_LEVELS";
    private const string SCORE_OF_LEVEL = "SCORE_OF_LEVEL";
    private const string STAR_OF_LEVEL = "STAR_OF_LEVEL";
    private const string TIME_OF_LEVEL = "TIME_OF_LEVEL";
    private const string COST_OF_LEVEL = "COST_OF_LEVEL";
    private const string VIBRATE = "VIBRATE";
    private const string NO_ADS = "NO_ADS";
    private const string MONSTER_NUM = "MONSTER_NUM";
    private const string BALL_ATTACK = "BALL_ATTACK";
    private const string BALL_ATTACK_LEVEL = "BALL_ATTACK_LEVEL";
    private const string MERCENARY_DATA = "MERCENARY_DATA";
    private const string CONTINUOUS_SIGN_COUNT = "continuous_sign_count";
    private const string CRITICAL_STRIKE = "CRITICAL_STRIKE";
    private const string GUIDESTEP1 = "GUIDESTEP1";
    private const string GUIDESTEP2 = "GUIDESTEP2";
    private const string GUIDESTEP3 = "GUIDESTEP3";
    private const string GUIDESTEP4 = "GUIDESTEP4";
    private const string GUIDESTEP5 = "GUIDESTEP5";
    private const string GUIDESTEP6 = "GUIDESTEP6";
    private const string GUIDESTEP7 = "GUIDESTEP7";
    private const string GUIDESTEP8 = "GUIDESTEP8";


    private const string CAN_GUIDESTEP8 = "CAN_GUIDESTEP8";
    private const string CAN_GUIDESTEP6 = "CAN_GUIDESTEP6";
    private const string CAN_GUIDESTEP4 = "CAN_GUIDESTEP4";

    private const string OFFLINE_TIME = "OFFLINE_TIME";

    private const string OFFLINELEVEL = "OFFLINELEVEL";
    private const string CRITLEVEL = "CRITLEVEL";
    private const string EMOJICARRYLEVEL = "EMOJICARRYLEVEL";
    private const string BOSSEXTRACOINLEVEL = "BOSSEXTRACOINLEVEL";
    private const string BOSSEXTRAATTACKLEVEL = "BOSSEXTRAATTACKLEVEL";

    private const string BOSSFAILCOUNT = "BOSSFAILCOUNT";



    public bool noAds {
        get { return EzPrefs.GetBool(NO_ADS); }
        set { EzPrefs.SetBool(NO_ADS, value); }
    }
    public int playTimes;

    private const string GUIDED = "GUIDED";
    public bool guided {
        get { return EzPrefs.GetBool(GUIDED); }
        set { EzPrefs.SetBool(GUIDED, value); }
    }

    public string playerName;
    public Sprite playerAvatar;
    public LongPersistentProperty coins;
    public int score;
    public int bestScore;
    public int unlockedLevels;
    public int monsterNum;
    public float ballAttack = 10;
    public int ballAttackLevel = 1;
    public float ballAttackPower = 1;
    public float mercenaryAttackPower = 1;
    public float boxCoinPower = 1;
    public float criticalStrike = 0;
    public bool guidestep1 = false;
    public bool guidestep2 = false;
    public bool guidestep3 = false;
    public bool guidestep4 = false;
    public bool guidestep5 = false;
    public bool guidestep6 = false;
    public bool guidestep7 = false;
    public bool guidestep8 = false;

    public bool guidestep9 = false;
    public bool guidestep10 = false;
    public bool guidestep11 = false;


    public bool canguidestep8 = false;
    public bool canguidestep6 = false;
    public bool canguidestep4 = false;



    public int offlineLevel = 1;
    public int critLevel = 1;
    public int EmojiCarryLevel = 1;
    public int BossExtraCoinLevel = 1;
    public int BossExtraAttackLevel = 1;


    public int bossFailCount = 0;

    public System.DateTime offlineTime;

    public Sprite[] qualityBgSprite;

    public List<MercenaryData> mercenaryDatas;

    public List<SkillData> skillDatas;

    public List<BuffData> buffDatas;

    public IntPersistentProperty currentMercenaryIndex;

    public IntPersistentProperty skill1Level;
    public IntPersistentProperty skill2Level;
    public IntPersistentProperty skill3Level;
    public IntPersistentProperty skill4Level;
    public IntPersistentProperty skill5Level;
    public IntPersistentProperty skill6Level;

    void LoadPlayerData() {
        playTimes = EzPrefs.GetInt(PLAY_TIMES, 0);
        EzPrefs.SetInt(PLAY_TIMES, ++playTimes);
        playerName = EzPrefs.GetString(NAME, playerName);
        coins.Value = testCoins >= 0 ? testCoins : EzPrefs.GetLong(COINS, initCoins);
        score = EzPrefs.GetInt(SCORE, 0);
        criticalStrike = EzPrefs.GetFloat(CRITICAL_STRIKE, 6);
        monsterNum = EzPrefs.GetInt(MONSTER_NUM, 1);
        ballAttack = EzPrefs.GetFloat(BALL_ATTACK, 10f);
        bestScore = EzPrefs.GetInt(BEST_SCORE, 0);
        ballAttackLevel = EzPrefs.GetInt(BALL_ATTACK_LEVEL,1);
        offlineLevel = EzPrefs.GetInt(OFFLINELEVEL, 1);
        critLevel = EzPrefs.GetInt(CRITLEVEL, 1);
        EmojiCarryLevel = EzPrefs.GetInt(EMOJICARRYLEVEL, 1);
        BossExtraCoinLevel = EzPrefs.GetInt(BOSSEXTRACOINLEVEL, 1);
        BossExtraAttackLevel = EzPrefs.GetInt(BOSSEXTRAATTACKLEVEL, 1);
        bossFailCount = EzPrefs.GetInt(BOSSFAILCOUNT,0);
        currentMercenaryIndex = new IntPersistentProperty("currentMercenaryIndex", 0);

        guidestep1 = EzPrefs.GetBool(GUIDESTEP1, false);
        guidestep2 = EzPrefs.GetBool(GUIDESTEP2, false);
        guidestep3 = EzPrefs.GetBool(GUIDESTEP3, false);
        guidestep4 = EzPrefs.GetBool(GUIDESTEP4, false);
        guidestep5 = EzPrefs.GetBool(GUIDESTEP5, false);
        guidestep6 = EzPrefs.GetBool(GUIDESTEP6, false);
        guidestep7 = EzPrefs.GetBool(GUIDESTEP7, false);
        guidestep8 = EzPrefs.GetBool(GUIDESTEP8, false);

        canguidestep8 = EzPrefs.GetBool(CAN_GUIDESTEP8, false);
        canguidestep6 = EzPrefs.GetBool(CAN_GUIDESTEP6, false);
        canguidestep4 = EzPrefs.GetBool(CAN_GUIDESTEP4, false);

        skill1Level = new IntPersistentProperty("skill1Level", 1);
        skill2Level = new IntPersistentProperty("skill2Level", 1);
        skill3Level = new IntPersistentProperty("skill3Level", 1);
        skill4Level = new IntPersistentProperty("skill4Level", 1);
        skill5Level = new IntPersistentProperty("skill5Level", 1);
        skill6Level = new IntPersistentProperty("skill6Level", 1);

        mercenaryDatas = new List<MercenaryData>();
        mercenaryDatas.Add(new MercenaryData(0, Localization.GetMultilineText("MercenaryName1"), Localization.GetMultilineText("MercenaryFirstInfo1")));
        mercenaryDatas.Add(new MercenaryData(1, Localization.GetMultilineText("MercenaryName2"), Localization.GetMultilineText("MercenaryFirstInfo2")));
        mercenaryDatas.Add(new MercenaryData(2, Localization.GetMultilineText("MercenaryName3"), Localization.GetMultilineText("MercenaryFirstInfo3")));
        mercenaryDatas.Add(new MercenaryData(3, Localization.GetMultilineText("MercenaryName4"), Localization.GetMultilineText("MercenaryFirstInfo4")));
        mercenaryDatas.Add(new MercenaryData(4, Localization.GetMultilineText("MercenaryName5"), Localization.GetMultilineText("MercenaryFirstInfo5")));
        mercenaryDatas.Add(new MercenaryData(5, Localization.GetMultilineText("MercenaryName6"), Localization.GetMultilineText("MercenaryFirstInfo6")));


        skillDatas = new List<SkillData>();
        skillDatas.Add(new SkillData(0, Localization.GetMultilineText("SkillName1")));
        skillDatas.Add(new SkillData(1, Localization.GetMultilineText("SkillName2")));
        skillDatas.Add(new SkillData(2, Localization.GetMultilineText("SkillName3")));
        skillDatas.Add(new SkillData(3, Localization.GetMultilineText("SkillName4")));
        skillDatas.Add(new SkillData(4, Localization.GetMultilineText("SkillName5")));
        skillDatas.Add(new SkillData(5, Localization.GetMultilineText("SkillName6")));

        unlockedLevels = testLevels > 0 ? testLevels : EzPrefs.GetInt(UNLOCKED_LEVELS, 1);
        if (GetMercenaryData() == null) {
            SavaMercenaryData();
        }
      
        _continuousSignCount = PlayerPrefs.GetInt(CONTINUOUS_SIGN_COUNT);
    }

    public void SetOfflineTime(System.DateTime dateTime) {
        EzPrefs.SetDateTime(OFFLINE_TIME, dateTime);
    }

    public System.DateTime GetOfflineTime() {
        return EzPrefs.GetDateTime(OFFLINE_TIME, System.DateTime.MinValue);
    }

    public bool IsSignedToday() {
        System.DateTime lastTime = System.DateTime.FromBinary(System.Convert.ToInt64(PlayerPrefs.GetString(LAST_SIGN_TIME, "0")));
        System.DateTime lastTemp = new System.DateTime(lastTime.Year, lastTime.Month, lastTime.Day);

        System.DateTime todayTime = System.DateTime.Now;
        System.DateTime todayTemp = new System.DateTime(todayTime.Year, todayTime.Month, todayTime.Day);

        System.DateTime yesdayTime = todayTemp.AddDays(-1);
        System.DateTime yesdayTemp = new System.DateTime(yesdayTime.Year, yesdayTime.Month, yesdayTime.Day);

        bool signed = lastTemp == todayTemp;

        if (!signed && yesdayTemp != lastTemp) continuousSignCount = 0;

        return signed;
    }

    public void SignToday() {
        System.DateTime todayTime = System.DateTime.Now;
        System.DateTime todayTemp = new System.DateTime(todayTime.Year, todayTime.Month, todayTime.Day);
        PlayerPrefs.SetString(LAST_SIGN_TIME, todayTemp.ToBinary().ToString());
        continuousSignCount += 1;
    }

    public void GuideStep1Comple() {
        guidestep1 = true;
        EzPrefs.SetBool(GUIDESTEP1, guidestep1);
    }
    public void GuideStep2Comple() {
        guidestep2 = true;
        EzPrefs.SetBool(GUIDESTEP2, guidestep2);
    }


    public void GuideStep3Comple() {
        guidestep3 = true;
        EzPrefs.SetBool(GUIDESTEP3, guidestep3);
    }

    public void GuideStep4Comple() {
        guidestep4 = true;
        EzPrefs.SetBool(GUIDESTEP4, guidestep4);
    }

    public void GuideStep5Comple() {
        guidestep5 = true;
        EzPrefs.SetBool(GUIDESTEP5, guidestep5);
    }

    public void GuideStep6Comple() {
        guidestep6 = true;
        EzPrefs.SetBool(GUIDESTEP6, guidestep6);
    }

    public void GuideStep7Comple() {
        guidestep7 = true;
        EzPrefs.SetBool(GUIDESTEP7, guidestep7);
      

    }

    public void GuideStep8Comple() {
        guidestep8 = true;
        EzPrefs.SetBool(GUIDESTEP8, guidestep8);
    }

    public void CanGuideStep8() {
        canguidestep8 = true;
        EzPrefs.SetBool(CAN_GUIDESTEP8,canguidestep8);
        canguidestep6 = false;
        EzPrefs.SetBool(CAN_GUIDESTEP6, canguidestep6);
    }

  

    public void CanGuideStep6() {
        canguidestep6 = true;
        EzPrefs.SetBool(CAN_GUIDESTEP6, canguidestep6);
        canguidestep4 = false;
        EzPrefs.SetBool(CAN_GUIDESTEP4, canguidestep4);
    }

    public void CanGuideStep4() {
        canguidestep4 = true;
        EzPrefs.SetBool(CAN_GUIDESTEP4, canguidestep4);
    }

    private int _continuousSignCount;
    public int continuousSignCount {
        get {
            return _continuousSignCount;
        }
        set {
            _continuousSignCount = value;

            PlayerPrefs.SetInt(CONTINUOUS_SIGN_COUNT, _continuousSignCount);
        }
    }

    public void AddBossFailCount() {
        bossFailCount++;
        if(bossFailCount > 6) {
            bossFailCount = 6;
        }
        EzPrefs.SetInt(BOSSFAILCOUNT, bossFailCount);
    }

    public void CleanBossFailCount() {
        bossFailCount = 0;
        EzPrefs.SetInt(BOSSFAILCOUNT, bossFailCount);
    }

    public class MercenaryDataClass {
        public List<MercenaryData> mercenaryDatas;
    }
    public List<int> BossEmojiIds;
    public MercenaryDataClass mercenaryDataClass;

    public void SavaMercenaryData() {
        mercenaryDataClass = new MercenaryDataClass();
        mercenaryDataClass.mercenaryDatas = mercenaryDatas;
        //string mercenaryDatasStr =  Json.Serialize(mercenaryDatas);
        string mercenaryDatasStr = JsonUtility.ToJson(mercenaryDataClass);
        EzPrefs.SetString(MERCENARY_DATA, mercenaryDatasStr);
    }

    public List<MercenaryData> GetMercenaryData() {
        string mercenaryDatasStr =  EzPrefs.GetString(MERCENARY_DATA, ""); 
        if (!"".Equals(mercenaryDatasStr)) {
            return JsonUtility.FromJson<MercenaryDataClass>(mercenaryDatasStr).mercenaryDatas;
        }
        return null;
    }

     


    public MercenaryData GetMercenaryDataForId(int id) {
        foreach (MercenaryData mer in mercenaryDatas) {
            if(mer.id == id) {
                return mer;
            }
        }
        return null;

    }


    public MercenaryData CheckUnLockMercenary(int emojiId) {
        foreach (MercenaryData mer in mercenaryDatas) {
            if (!mer.IsOwn && emojiId == mer.id) {
                    mer.IsOwn.Value = true;
                    SavaMercenaryData();
                    return mer;
            }
        }
        return null;
    }

    public bool UpdateMercenaryLevel(int mercenaryId) {
        long price = 0;
        foreach (MercenaryData mer in mercenaryDatas) {
            if(mer.id == mercenaryId) {
                price = mer.GetPrice();
                if(coins >= price && mer.Level< mer.maxLevel) {
                    CostCoins((int)price);
                    mer.Level.Value++;
                    SavaMercenaryData();
                    return true;
                }
            }
        }
        return false;
    }

    public float GetCriticalStrike() {
        return EzPrefs.GetFloat(CRITICAL_STRIKE, 0f);
    }
    public void SetCriticalStrike(float num) {
        criticalStrike = num;
        EzPrefs.SetFloat(CRITICAL_STRIKE, num);
    }


    public int GetEquipMercenary(int index) {
        return EzPrefs.GetInt("EquipMercenary" + index, -1);
    }

    public void EquipMercenary(int index ,int mercenaryId) {
        EzPrefs.SetInt("EquipMercenary" + index, mercenaryId);
    }

    public void AddOwnEmoji(int id) {
        int num =  EzPrefs.GetInt("Emoji" + id, 0);
        EzPrefs.SetInt("Emoji" + id, ++num);
    }

    public int GetOwnEmojiNum(int id) {
        return EzPrefs.GetInt("Emoji" + id, 0);
    }

    public void CostEmojiNum(int id,int num) {
        int emojiNum = EzPrefs.GetInt("Emoji" + id, 0);
        emojiNum -= num;
        EzPrefs.SetInt("Emoji" + id, emojiNum);
    }

    public List<int> GetBossRollEmoji() {
        //int index = UnityEngine.Random.Range(0, BossEmojiIds.Count);
        EzRandom.Shuffle(BossEmojiIds);
        return BossEmojiIds.GetRange(0, 3);
    }



    public void FixupPlayerName() {
        if (string.IsNullOrEmpty(playerName)) {
            UpdatePlayerName(Localization.GetText("Guest") + UnityEngine.Random.Range(123456789, 987654321));
        }
    }


    public void UpdatePlayerName(string name) {
        playerName = name;
        EzPrefs.SetString(NAME, playerName);
    }

    public void GainCoins(long gains, string from = "") {
        coins.Value += gains;
        EzPrefs.SetLong(COINS, coins);
        EzAnalytics.LogEvent("Coin", "Gain", from, gains);
    }

    public long CostCoins(long costs, string to = "") {
        if (costs > coins) return -1;
        coins.Value -= costs;
        EzPrefs.SetLong(COINS, coins);
        EzAnalytics.LogEvent("Coin", "Cost", to, costs);
        return coins;
    }


    public float GetBattleAttackLevel() {
        return ballAttackLevel;
    }

    public bool AddBattleAttackLevel() {
        long levelUPCoin = GetAttackUpPrice();
        if (coins >= levelUPCoin) {
            CostCoins(levelUPCoin);
            EzPrefs.SetInt(BALL_ATTACK_LEVEL, ++ballAttackLevel);
            AddBallAttack(BallAttackAddNum);
            if (GameManager.instance.ballAttackLevel > 1 && GameController.instance != null) {
                GameController.instance.isGameOver = false;
            }
            return true;
        }
        return false;
    }



    public  int BallAttackMaxLevel = 1000;
    public  int OfflineMaxLevel = 200;
    public  int CritMaxLevel = 30;
    public  int EmojiCarryMaxLevel = 30;




    //每次升级增加的小球攻击力
    private float BallAttackAddNum = 1;

    //升级小球攻击价格
    public long GetAttackUpPrice() {
        return (long)(60f * Mathf.Pow(ballAttackLevel, 1.01f) + 15);
    }
    //离线收益升级价格
    public long GetOfflineUpPrice() {
        return (long)(50 * Mathf.Pow(offlineLevel, 4f) + 500);
    }
    //升级暴击几率价格
    public long GetCritUpPrice() {
        return (long)(200 * Mathf.Pow(critLevel, 5f) + 3000);
    }
    //升级Emoji狂潮价格
    public long GetEmojiCarryPrice() {
        return (long)(200 * Mathf.Pow(EmojiCarryLevel, 5f) + 3000);
    }

    //升级BOSS额外金币价值
    public long GetExtraBossCoinPrice() {
        return (long)(50 * Mathf.Pow(BossExtraCoinLevel, 1.01f) + 15);
    }

    public long GetUpLevel1SkillPrice() {
        return (long)(Mathf.Pow(skill1Level.Value,1.1f));
    }
    public long GetUpLevel2SkillPrice() {
        return (long)(Mathf.Pow(skill2Level.Value, 1.1f));
    }
    public long GetUpLevel3SkillPrice() {
        return (long)(Mathf.Pow(skill3Level.Value, 1.1f));
    }
    public long GetUpLevel4SkillPrice() {
        return (long)(Mathf.Pow(skill4Level.Value, 1.1f));
    }
    public long GetUpLevel5SkillPrice() {
        return (long)(Mathf.Pow(skill5Level.Value, 1.1f));
    }
    public long GetUpLevel6SkillPrice() {
        return (long)(Mathf.Pow(skill6Level.Value, 1.1f));
    }


    //升级BOSS额外伤害
    public long GetExtraBossAttackPrice() {
        return (long)(50 * Mathf.Pow(BossExtraAttackLevel, 1.01f) + 15);
    }

    //小怪血量
    public float GetDogFaceHP() {
        return 80f * (9f + GetDogFace() * 1.18f);
    }

    //BOSS血量
    public float GetBossHP() {
        float bossHp = 150f * (9f + Mathf.Pow(GetBossNum(), 1.8f) * 6f);
        if(GetBossNum() % 4 == 0) {
            bossHp *= 3;
        }
        bossHp = bossHp * (1 - Mathf.Pow(bossFailCount, 1.2f) * 0.1f);
        return bossHp;
    }
    //Boss时间
    public float GetBossTime() {
        return 5 * Mathf.Sin(GameManager.instance.GetBossNum()) + 60;
    }

    public void AddBallAttack(float addNum) {
        ballAttack += addNum;
        EzPrefs.SetFloat(BALL_ATTACK, ballAttack);
    }

    public void AddCritBuff(float cirtNum,float buffDuration) {
        criticalStrike += cirtNum;

    }

    


    public int GetOfflineLevel() {
        return EzPrefs.GetInt(OFFLINELEVEL, 1);
    }

    public bool AddOfflineLevel() {
        long price = GetOfflineUpPrice();
        if (coins >= price && offlineLevel < OfflineMaxLevel) {
            offlineLevel++;
            EzPrefs.SetInt(OFFLINELEVEL, offlineLevel);
            CostCoins((long)price);
            return true;
        }
        return false;
    }

    public int GetBossExtraCoinLevel() {
        return EzPrefs.GetInt(BOSSEXTRACOINLEVEL, 1);
    }

    public bool AddBossExtraCoinLevel() {
        long price = GetExtraBossCoinPrice();
        if (coins >= price ) {
            BossExtraCoinLevel++;
            EzPrefs.SetInt(BOSSEXTRACOINLEVEL, BossExtraCoinLevel);
            CostCoins((long)price);
            return true;
        }
        return false;
    }

    public int GetBossExtraCoin() {
        return GetBossExtraCoinLevel();
    }


    public int GetBossExtraAttackLevel() {
        int level = EzPrefs.GetInt(BOSSEXTRAATTACKLEVEL, 1);
        return level;
    }

    public bool AddBossExtraAttackLevel() {
        long price = GetExtraBossAttackPrice();
        if (coins >= price) {
            BossExtraAttackLevel++;
            EzPrefs.SetInt(BOSSEXTRAATTACKLEVEL, BossExtraAttackLevel);
            CostCoins((long)price);
            return true;
        }
        return false;
    }

    public int GetBossExtralAttack() {
        return GetBossExtraAttackLevel();
    }


    public int GetCritLevel() {
        return EzPrefs.GetInt(CRITLEVEL, 1);
    }

    
    public bool AddCritLevel() {
        long price = GetCritUpPrice();
        if (coins >= price && critLevel < CritMaxLevel) {
            critLevel++;
            EzPrefs.SetInt(CRITLEVEL, critLevel);
            CostCoins((long)price);
            if (GameController.instance != null) {
                SetCriticalStrike((GameManager.instance.GetCritLevel() * 0.5f) + 4f);
            }
            return true;
        }
        return false;
    }

  

    public int GetEmojiCarryLevel() {
        return EzPrefs.GetInt(EMOJICARRYLEVEL, 1);
    }

    public bool AddEmojiCarrayLevel() {
        long price = GetEmojiCarryPrice();
        if (coins >= price && EmojiCarryLevel < EmojiCarryMaxLevel) {
            EmojiCarryLevel++;
            EzPrefs.SetInt(EMOJICARRYLEVEL, EmojiCarryLevel);
            CostCoins((long)price);
            if (GameController.instance != null) {
                GameController.instance.EmojiCarryCDTime = (30 - (GameManager.instance.GetEmojiCarryLevel() - 1) * 0.1f);
            }
            return true;
        }
        return false;
    }

    public bool AddSkill1Level() {
        long price = GetUpLevel1SkillPrice();
        if (GetOwnEmojiNum(0) >= price ) {
            skill1Level.Value++;
            CostEmojiNum(0, (int)price);
            return true;
        }
        return false;
    }

    public bool AddSkill2Level() {
        long price = GetUpLevel2SkillPrice();
        if (GetOwnEmojiNum(1) >= price) {
            skill2Level.Value++;
            CostEmojiNum(1, (int)price);
            return true;
        }
        return false;
    }
    public bool AddSkill3Level() {
        long price = GetUpLevel3SkillPrice();
        if (GetOwnEmojiNum(2) >= price) {
            skill3Level.Value++;
            CostEmojiNum(2, (int)price);
            return true;
        }
        return false;
    }
    public bool AddSkill4Level() {
        long price = GetUpLevel4SkillPrice();
        if (GetOwnEmojiNum(3) >= price) {
            skill4Level.Value++;
            CostEmojiNum(3, (int)price);
            return true;
        }
        return false;
    }
    public bool AddSkill5Level() {
        long price = GetUpLevel5SkillPrice();
        if (GetOwnEmojiNum(4) >= price) {
            skill5Level.Value++;
            CostEmojiNum(4, (int)price);
            return true;
        }
        return false;
    }
    public bool AddSkill6Level() {
        long price = GetUpLevel6SkillPrice();
        if (GetOwnEmojiNum(5) >= price) {
            skill6Level.Value++;
            CostEmojiNum(5, (int)price);
            return true;
        }
        return false;
    }




    public void GainScore(int gains) {
        UpdateScore(score + gains);
    }

    public int GetMonsterNum() {
        return EzPrefs.GetInt(MONSTER_NUM, 1);
    }

    public int GetCoinPrice() {
        int coinPrice = EzPrefs.GetInt(MONSTER_NUM, 1);
        if((monsterNum-1) % 6 == 0) {
            coinPrice += GetBossExtraCoinLevel();
        }
        return coinPrice;
    }

    public void AddMonsterNum() {
        EzPrefs.SetInt(MONSTER_NUM, ++monsterNum);
    }

    public void ReduceMonsterNum() {
        EzPrefs.SetInt(MONSTER_NUM, --monsterNum);
    }

    public int GetDogFace() {
        return monsterNum - monsterNum / 6;
    }

    public int GetBossNum() {
        return monsterNum / 6;
    }

    public float GetBallAttack() {
        return (int)(ballAttack * ballAttackPower);
    }
   

   

    public bool CheckIsBoss() {
        if(monsterNum % 6 == 0) {
            return true;
        } else {
            return false;
        }
    }

    public bool CheckNextIsBoss() {
        if ((monsterNum+1) % 6 == 0) {
            return true;
        } else {
            return false;
        }
    }

    public int GetBossDistance() {
        return 6 - monsterNum % 6;
    }

    public bool UpdateScore(int score) {
        this.score = score;
        EzPrefs.SetInt(SCORE, score);
        if (score > bestScore) {
            bestScore = score;
            EzPrefs.SetInt(BEST_SCORE, bestScore);
            return true;
        }
        return false;
    }

    public bool UnlockLevel() {
        if (unlockedLevels < maxLevels) {
            ++unlockedLevels;
            EzPrefs.SetInt(UNLOCKED_LEVELS, unlockedLevels);
            return true;
        }
        return false;
    }

    public int GetScoreOfCurrentLevel() {
        return GetScoreOfLevel(currentLevel);
    }

    public void UpdateScoreOfCurrentLevel(int score) {
        UpdateScoreOfLevel(currentLevel, score);
    }

    public int GetScoreOfLevel(int levelId) {
        return EzPrefs.GetInt(SCORE_OF_LEVEL + "_" + levelId);
    }

    public void UpdateScoreOfLevel(int levelId, int score) {
        string key = SCORE_OF_LEVEL + "_" + levelId;
        if (score > EzPrefs.GetInt(key)) {
            EzPrefs.SetInt(key, score);
        }
    }

    public int GetStarOfCurrentLevel() {
        return GetStarOfLevel(currentLevel);
    }

    public void UpdateStarOfCurrentLevel(int star) {
        UpdateStarOfLevel(currentLevel, star);
    }

    public int GetStarOfLevel(int levelId) {
        return EzPrefs.GetInt(STAR_OF_LEVEL + "_" + levelId);
    }


    public void UpdateStarOfLevel(int levelId, int star) {
        string key = STAR_OF_LEVEL + "_" + levelId;
        if (star > EzPrefs.GetInt(key)) {
            EzPrefs.SetInt(key, star);
        }
    }

    public float GetTimeOfCurrentLevel() {
        return GetTimeOfLevel(currentLevel);
    }

    public void UpdateTimeOfCurrentLevel(float time) {
        UpdateTimeOfLevel(currentLevel, time);
    }

    public float GetTimeOfLevel(int levelId) {
        return EzPrefs.GetFloat(TIME_OF_LEVEL + "_" + levelId, float.PositiveInfinity);
    }

    public void UpdateTimeOfLevel(int levelId, float time) {
        string key = TIME_OF_LEVEL + "_" + levelId;
        if (time < EzPrefs.GetFloat(key)) {
            EzPrefs.SetFloat(key, time);
        }
    }

    public int GetCostOfCurrentLevel() {
        return GetCostOfLevel(currentLevel);
    }

    public void UpdateCostOfCurrentLevel(int cost) {
        UpdateCostOfLevel(currentLevel, cost);
    }

    public int GetCostOfLevel(int levelId) {
        return EzPrefs.GetInt(COST_OF_LEVEL + "_" + levelId, int.MaxValue);
    }

    public void UpdateCostOfLevel(int levelId, float time) {
        string key = COST_OF_LEVEL + "_" + levelId;
        if (time < EzPrefs.GetFloat(key)) {
            EzPrefs.SetFloat(key, time);
        }
    }

    public bool vibrateEnabled {
        get { return EzPrefs.GetInt(VIBRATE, 1) != 0; }
        set { EzPrefs.SetInt(VIBRATE, value ? 1 : 0); }
    }

    public void Vibrate() {
#if UNITY_ANDROID || UNITY_IOS
        if (vibrateEnabled) Handheld.Vibrate();
#endif
    }

}
