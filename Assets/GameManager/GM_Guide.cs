﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class GameManager : MonoBehaviour {

    private Guide[] guides;

    private void InitGuides() {
        guides = GetComponentsInChildren<Guide>(true);
    }

	private void CheckGuides() {
        foreach (Guide guide in guides) {
            if (guide.IsTimeNow()) {
                GuideMask.Show();
                guide.gameObject.SetActive(true);
                break;
            }
        }
    }
}
