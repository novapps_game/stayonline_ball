﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class BossCome : Panel {
    public TweenMove tm;
    public GameObject textGo;
    public GameObject text;
	// Use this for initialization
	void Start () {
        textGo.GetComponent<Image>().DOColor(Color.red, 1.5f);
        EzTiming.CallDelayed(1.51f, () => {
            textGo.GetComponent<Image>().DOColor(Color.yellow, 1.5f);
        });
        text.GetComponent<Image>().DOColor(Color.red, 1.5f);
        EzTiming.CallDelayed(1.51f, () => {
            text.GetComponent<Image>().DOColor(Color.yellow, 1.5f);
        });
        tm.onComplete.AddListener((go) => {
            textGo.transform.localPosition = new Vector3(-1236,0,0);
            tm.position = new Vector3(1800, 0, 0);
            tm.PlayNow();
        });
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
