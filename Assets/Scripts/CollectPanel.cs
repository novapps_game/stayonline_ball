﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectPanel : Panel {

    public GameObject MercenaryGrid;

    public GameObject MyCollectGrid;

    public GameObject UnOwnMercenaryGrid;

    private List<MercenaryItem> MercenaryLists;

    private List<MercenaryItem> MyCollectGridList;

    private List<MercenaryItem> OwnList;
    List<MercenaryData> mercenaryDatas;

    public GameObject SetMercenary;

    public GameObject MyCollect;
    public GameObject MyCollectTitle;

    public ScrollList scrollList;



    bool isEquip = false;
    // Use this for initialization
    void Start () {

        //GameManager.instance.EquipMercenary(0, 5001);
        mercenaryDatas = GameManager.instance.GetMercenaryData();

        MercenaryLists = new List<MercenaryItem>();
        MyCollectGridList = new List<MercenaryItem>();
        OwnList = new List<MercenaryItem>();

        for (int i = 0;i < MercenaryGrid.transform.childCount; i++) {
            MercenaryLists.Add(MercenaryGrid.transform.GetChild(i).GetComponent<MercenaryItem>());
        }

        for (int i = 0; i < MyCollectGrid.transform.childCount; i++) {
            MyCollectGridList.Add(MyCollectGrid.transform.GetChild(i).GetComponent<MercenaryItem>());
        }

        for (int i = 0;i < 6; i++) {
            int equipId = GameManager.instance.GetEquipMercenary(i);
            if (equipId == -1) {
                //MercenaryLists[i].mercenaryImage.sprite = null;
                MercenaryLists[i].ShowEmpty();
            } else {
                MercenaryLists[i].mercenary = GameManager.instance.GetMercenaryDataForId(equipId);
                MercenaryLists[i].mercenaryImage.sprite = MercenaryLists[i].mercenary.icon;
                MercenaryLists[i].Init();
            }
            MercenaryItem mercenaryItem = MercenaryLists[i];
            int index = i;
            MercenaryLists[i].gameObject.transform.Find("Panel").GetComponent<Button>().onClick.AddListener(()=> {
                
                if (isEquip) {
                    GameManager.instance.EquipMercenary(index, this.EquipId);
                    mercenaryItem.mercenary = GameManager.instance.GetMercenaryDataForId(this.EquipId);
                    Debug.LogError("mercenaryItem"+ mercenaryItem.mercenary.icon);
                    mercenaryItem.mercenaryImage.sprite = mercenaryItem.mercenary.icon;
                    isEquip = false;
                    ShowNormalPanel();
                    GameController.instance.ShowBottomEmoji();
                    
                }                                                                                                                                               
            });




        }
     
        for (int i = 0; i < mercenaryDatas.Count; i++) {
            if (mercenaryDatas[i].IsOwn) {
                GameObject mercenaryItemGo =  GameManager.instance.Instantiate("MercenaryItem");
                mercenaryItemGo.transform.SetParent(MyCollectGrid.transform,false);
                MercenaryItem mercenaryItem = mercenaryItemGo.GetComponent<MercenaryItem>();
                mercenaryItem.mercenary = mercenaryDatas[i];
                mercenaryItem.mercenaryImage.sprite = mercenaryDatas[i].icon;
                mercenaryItem.Init();
            }
        }

        for (int i = 0; i < mercenaryDatas.Count; i++) {
            if (!mercenaryDatas[i].IsOwn) {
                GameObject mercenaryItemGo = GameManager.instance.Instantiate("MercenaryItem");
                mercenaryItemGo.transform.SetParent(UnOwnMercenaryGrid.transform, false);
                MercenaryItem mercenaryItem = mercenaryItemGo.GetComponent<MercenaryItem>();
                mercenaryItem.mercenary = mercenaryDatas[i];
                mercenaryItem.mercenaryImage.sprite = mercenaryDatas[i].icon;
                mercenaryItem.Init();
            }
        }

    }






	
	// Update is called once per frame
	void Update () {
		
	}

    public GameObject MyCollectBtn;

    public GameObject OwnBtn;

    public void ShowMyCollectCategoryBtn() {
        MyCollectBtn.SetActive(true);
    }

    public void HideMyCollectCategoryBtn() {
        MyCollectBtn.SetActive(false);
    }

    public void ShowOwnCategoryBtn() {
        OwnBtn.SetActive(true);
    }

    public void HideOwnCategoryBtn() {
        OwnBtn.SetActive(false);
    }


    public void MyCollectShowDefault() {
        mercenaryDatas.Sort((mer1, mer2) => {
            if (mer1.GetLevelUpCardNum() > mer2.GetLevelUpCardNum()) {
                return 1;
            }
            return -1;
        });
        for (int i = 0; i < MyCollectGrid.transform.childCount; i++) {
            if (mercenaryDatas[i].IsOwn) {
                GameObject mercenaryItemGo = MyCollectGrid.transform.GetChild(i).gameObject;
                MercenaryItem mercenaryItem = mercenaryItemGo.GetComponent<MercenaryItem>();
                mercenaryItem.mercenary = mercenaryDatas[i];
            }

        }
    }

    public void MyCollectShowQuality() {
        //mercenaryDatas.Sort((mer1, mer2) => {
        //    return mer1.quality.CompareTo(mer2.quality);
        //});
        for (int i = 0; i < MyCollectGrid.transform.childCount; i++) {
            if (mercenaryDatas[i].IsOwn) {
                GameObject mercenaryItemGo = MyCollectGrid.transform.GetChild(i).gameObject;
                MercenaryItem mercenaryItem = mercenaryItemGo.GetComponent<MercenaryItem>();
                mercenaryItem.mercenary = mercenaryDatas[i];
                
            }
        }
    } 

    public void UnOwnShowDefault() {
        mercenaryDatas.Sort((mer1, mer2) => {
            if (mer1.GetLevelUpCardNum() > mer2.GetLevelUpCardNum()) {
                return 1;
            }
            return -1;
        });
        for (int i = 0; i < UnOwnMercenaryGrid.transform.childCount; i++) {
            if (mercenaryDatas[i].IsOwn) {
                GameObject mercenaryItemGo = UnOwnMercenaryGrid.transform.GetChild(i).gameObject;
                MercenaryItem mercenaryItem = mercenaryItemGo.GetComponent<MercenaryItem>();
                mercenaryItem.mercenary = mercenaryDatas[i];

            }
        }
    }

    public void UnOwnShowQuality() {
        //mercenaryDatas.Sort((mer1, mer2) => {
        //    return mer1.quality.CompareTo(mer2.quality);
        //});
        for (int i = 0; i < UnOwnMercenaryGrid.transform.childCount; i++) {
            if (mercenaryDatas[i].IsOwn) {
                GameObject mercenaryItemGo = UnOwnMercenaryGrid.transform.GetChild(i).gameObject;
                MercenaryItem mercenaryItem = mercenaryItemGo.GetComponent<MercenaryItem>();
                mercenaryItem.mercenary = mercenaryDatas[i];

            }
        }
    }

    int EquipId;
    public void ShowSetMercenary(int mercenaryId) {
        SetMercenary.SetActive(true);
        MyCollect.SetActive(false);
        MyCollectTitle.SetActive(false);
        EquipId = mercenaryId;
        isEquip = true;
        for (int i = 0; i < 6; i++) {
            MercenaryLists[i].isEquip = true;
        }
    }

    public void ShowNormalPanel() {
        SetMercenary.SetActive(false);
        MyCollect.SetActive(true);
        MyCollectTitle.SetActive(true);
        isEquip = false;
        EzTiming.CallDelayed(0.3f, () => {
            for (int i = 0; i < 6; i++) {
                MercenaryLists[i].isEquip = false;
            }
        });
        
    }

}
