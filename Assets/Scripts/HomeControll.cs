﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class HomeControll : MonoBehaviour {

    public BigNumberText bigNumber;
    public GameObject StartBtn;

	// Use this for initialization
	void Start () {
        if (!GameManager.instance.IsSignedToday()) {
            Panel.Open("SignPanel");
        } else {
            GameManager.instance.ShowOfflinePanel();
        }

        bigNumber.SetValue(GameManager.instance.coins);
        StartBtnAnima();
    }

    private void StartBtnAnima() {
        if (StartBtn != null) {
            StartBtn.transform.DOScale(1.2f, 0.5f);
            EzTiming.CallDelayed(0.55f, () => {
                if (StartBtn != null) {
                    StartBtn.transform.DOScale(1f, 0.5f);
                    EzTiming.CallDelayed(0.55f, () => {
                        StartBtnAnima();
                    });
                }
            });
        }

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OpenUrl() {
        Application.OpenURL("http://game.novapps.com/web_html/novapps_privacy.html");
    }
}
