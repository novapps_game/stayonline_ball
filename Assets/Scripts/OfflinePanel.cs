﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OfflinePanel : Panel {
    public Text offlineNumText;
    public GainCoins gainCoin;
    public GameObject doubleBtn;
    private int offlineNum;
    private int offtimeMinutes;
    public GameObject getCoinsBtn;

    // Use this for initialization
    void Start () {
        System.DateTime dateTime = System.DateTime.Now;
        TimeSpan timeSpan = dateTime - GameManager.instance.GetOfflineTime();
        offtimeMinutes = 0;
        if (timeSpan.TotalMinutes >= 240) {
            offtimeMinutes = 240;
        }else {
            offtimeMinutes = (int)timeSpan.TotalMinutes;
        }

        offlineNum = (int)((GameManager.instance.GetMonsterNum() * 6 * (1 + GameManager.instance.GetOfflineLevel() * 0.005f)));
        int offlineTotalCoins = offlineNum * offtimeMinutes;
        
        if (Localization.GetLanguage() == "CN") {
            offlineNumText.text = "获得了<size=70><color=yellow>" + BigNumChange(offlineTotalCoins) + "</color></size>金币";
        }else if (Localization.GetLanguage() == "CT") {
            offlineNumText.text = "獲得了<size=70><color=yellow>" + BigNumChange(offlineTotalCoins) + "</color></size>金币";
        }else {
            offlineNumText.text = "GetCoins <size=70><color=yellow>" + BigNumChange(offlineTotalCoins) + "</color></size>";
        }
        GameManager.instance.SetOfflineTime(dateTime);
    }
    public string format = "{0}";
    public string formatKey;
    public bool displayPositiveSymbol;

    public string BigNumChange(double value) {
        return (displayPositiveSymbol && value > 0)
            ? string.Format(format, "+" + Digit.Format(value))
            : string.Format(format, Digit.Format(value));
    }

    bool isClick = false;
	
	// Update is called once per frame
	void Update () {
        doubleBtn.SetActive(EzAds.IsRewardedVideoReady());
        getCoinsBtn.SetActive(!EzAds.IsRewardedVideoReady());
    }

    public void DouchOffline() {
        if (!isClick) {
            isClick = true;
            if (EzAds.IsRewardedVideoReady()) {
                EzAds.ShowRewardedVideo(() => {
                    if (offlineNum * offtimeMinutes > 0) {
                        gainCoin.transform.SetParent(transform.parent, false);
                        gainCoin.Gain(2 * offlineNum * offtimeMinutes, offlineNum * offtimeMinutes / 50);
                        OfflinePush();
                    }
                    Close();
                },()=> {
                    if (offlineNum * offtimeMinutes > 0) {
                        gainCoin.transform.SetParent(transform.parent, false);
                        gainCoin.Gain(offlineNum * offtimeMinutes, offlineNum * offtimeMinutes / 25);
                        OfflinePush();
                    }
                    Close();
                });
            }
        }
    }

    public void ClickGetCoinBtn() {
        if (!isClick) {
            isClick = true;
            if (offlineNum * offtimeMinutes > 0) {
                gainCoin.transform.SetParent(transform.parent, false);
                gainCoin.Gain(offlineNum * offtimeMinutes, offlineNum * offtimeMinutes / 25);
                OfflinePush();
            }
            Close();
        }
    }

    private void OfflinePush() {
#if UNITY_ANDROID || UNITY_IOS
        if (Application.isMobilePlatform) {
            // nId = 0 表示离线收益推送通知
            //JPush.JPushBinding.ClearNotificationById(0);
            int maxOfflineSeconds =4 * 60 * 60;
            GameManager.instance.PushNotification(0, Localization.GetText("OfflineGainFull"), Localization.GetText("qiuqiu"), maxOfflineSeconds);
        }
#endif
    }
}
