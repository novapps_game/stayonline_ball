﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class GameController : Singleton<GameController> {

    public int TotalBallNum = 0;
    public List<BallSpawner> ballSpawners;
    private float EmojiCarryTime =3f;
    public float EmojiCarryCDTime = 30f;
    private float EmojiCarryCDTimeTemp = 0f;
    public Image emojiCarryImageCD;
    private bool EmojiCarryIsCD = true;

    public bool isEquip = false;

    public Slider BossTimeSlider;

    public int BossAndSkinID = 1;

    public Image BG1;
    public Image BG2;

    public GameObject BossTimeGo;

    public Sprite[] Bgs;

    public Image[] equipMercenarys;

    public MercenaryBallSpawner[] merBallSpawners;

    public bool isGameOver = false;
    public CountdownTime cdt;

    public Text BossDistancs;
    public Text BossNumText;

    public Canvas canvas;

    public GameObject NoTime;

    public GameObject ClickEffect;

    public int BossDistance = 6;

    public GameObject EmojiCarryGo;

    public GameObject BottomGo;

    public GameObject ReBossGo;

    public GameObject CoinGo;

    public GameObject collisionBottom;

    public float DogFaceDieCoins = 20;

    public float BossDieCoins = 120;

    public float MonsterDieCoinCoefficient = 1;

    public GameObject barrages;

    public GameObject timeFreez;
    public BoolObservableProperty isShowFir;
    public DialogMercen dialogMercen;




    // Use this for initialization
    void Start() {
        BossTimeGo.SetActive(false);
        GameManager.instance.OnGameStart();
        emojiCarryImageCD.fillAmount = 0;
        ShowBottomEmoji();
        //MercenaryAttack();
        InitBossDistance();
        NoTime.SetActive(false);
        if(Screen.height == 2732 && Screen.width == 2048) {
            //canvas.GetComponent<CanvasScaler>().matchWidthOrHeight = 1;
            BottomGo.transform.localScale = new Vector3(0.8f, 0.8f, 1);
            collisionBottom.transform.position = collisionBottom.transform.position.NewY(collisionBottom.transform.position.y + 0.243f);
        }
        if (Boss.instance.isFailNextBoss) {
            ShowReBoss();
        }
        isShowFir = new BoolObservableProperty();
        isShowFir.Value = false;
        //CheckBallFire();
    }

    //public void CheckBallFire() {
    //    isShowBallFire = (GameManager.instance.GetEquipMercenary(3) == 3);
    //}

    public int GetNextBossId() {
        return BossAndSkinID = GameManager.instance.GetBossNum() % 30 + 1;
    }
     
    public int GetMonsterId() {
        return GameManager.instance.GetMonsterNum() % 30 + 1;
    }
    public void PauseCountDown(float time) {
        isBossShow = false;
        cdt.Pause();
        EzTiming.CallDelayed(time, () => {
            cdt.Resume();
            isBossShow = GameManager.instance.CheckIsBoss() && !Boss.instance.isFailNextBoss;
        });
        ShowTimeFreez(time);
    }

    // Update is called once per frame                                                                                                                                                                  
    void Update() {
        if (Screen.height == 2732 && Screen.width == 2048) {
            collisionBottom.transform.localPosition = collisionBottom.transform.localPosition.NewY(-0.455f);
        }
        // if (Screen.height == 2436 && Screen.width == 1125) {

        int sceneScale = Mathf.RoundToInt((float)Screen.height / (float)Screen.width);
        if (sceneScale == 2) {
            // collisionBottom.transform.localPosition = collisionBottom.transform.localPosition.NewY(-0.898f);
            collisionBottom.transform.localPosition = collisionBottom.transform.localPosition.NewY(-0.86f * ((9f/16f)/ ((float)Screen.width / (float)Screen.height)));
        }
        //Debug.Log(TotalBallNum);
        if (!EmojiCarryIsCD) {
            EmojiCarryCD();
        }
        
        BossShow();
        //Panel.CheckBackButton(() => GameManager.instance.LoadScene("Home"));
        EmojiCarryGo.SetActive(GameManager.instance.guidestep6);
        BottomGo.SetActive(GameManager.instance.guidestep8);
    }
                                                                                                                                                                                          
        
  

    public void Revive() {
        // Todo: revive player
    }

    public void ShowReBoss() {
        ReBossGo.SetActive(true);
        BossDistancs.gameObject.transform.parent.gameObject.SetActive(false);
    }

    public void GameOver() {
        isGameOver = true;
        BossDie();
        GameManager.instance.AddBossFailCount();
        GameManager.instance.OpenGameOver();
        EzAnalytics.LogEvent("boss", "Lose", "Number", GameManager.instance.GetBossNum());
    }   

    public void ClickBall() {
        if (TotalBallNum < 80) {
            ballSpawners[0].transform.localPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition).NewYZ(3.5f,0f);
            ballSpawners[0].Create();
            ShowClickEffect();
        }
    }

    public void ShowClickEffect() {
        if (ClickEffect != null) {
            ClickEffect.SetActive(true);
        }
        EzTiming.CallDelayed(0.3f, () => {
            HideClickEffect();
        });
    }

    public void HideClickEffect() {
        if (ClickEffect != null) {
            ClickEffect.SetActive(false);
        }
    }

    public void SetClickEffectPosition(Vector2 pos) {
        if (ClickEffect != null) {
            ClickEffect.transform.position = pos;
        }
    }
    bool isGuideEmojiCarry = false;
    public void EmojiCarry() {
        if (EmojiCarryIsCD) {
            GameManager.instance.PlaySound("EmojiCarry");
            foreach (BallSpawner bs in ballSpawners) {
                ballSpawners[1].running = true;
                ballSpawners[2].running = true;
                ballSpawners[4].running = true;
            }
            isGuideEmojiCarry = true;
            if (!GameManager.instance.guidestep7) {
                isGameOver = false;
            }
            GameManager.instance.GuideStep7Comple();
         
            EmojiCarryTime = 3f + GameManager.instance.GetEmojiCarryLevel() * 0.5f;
            EzTiming.CallDelayed(EmojiCarryTime, () => {
                ballSpawners[1].running = false;
                ballSpawners[2].running = false;
                ballSpawners[4].running = false;
            });
            emojiCarryImageCD.fillAmount = 1;
            EmojiCarryIsCD = false;
        }
    }



    Tweener tds;
    private void EmojiCarryCD() {
        EmojiCarryCDTimeTemp += Time.deltaTime;
        emojiCarryImageCD.fillAmount = (EmojiCarryCDTime - EmojiCarryCDTimeTemp) / EmojiCarryCDTime;
        if(EmojiCarryCDTimeTemp >= EmojiCarryCDTime) {
            EmojiCarryIsCD = true;
            EmojiCarryCDTimeTemp = 0f;
            tds = EmojiCarryGo.transform.DOScale(1.3f, 0.5f).SetLoops(-1,LoopType.Yoyo);
        } else {
            tds.Pause();
            EmojiCarryGo.transform.DOScale(1f, 0.1f);
        }
    }

    float BossTimeTemp = 0f;
    public float BossTime = 25f;
    public bool isBossShow = false;
    public void BossShow() {
     
        if (isBossShow && GameManager.instance.CheckIsBoss()) {
            BossTimeGo.SetActive(true);
            BossTimeTemp += Time.deltaTime;
            BossTimeSlider.value = (BossTime - BossTimeTemp) / BossTime;
            if (BossTimeTemp >= BossTime) {
                BossTimeTemp = 0f;
                isBossShow = false;
                GameOver();
            }
            if(BossTime - BossTimeTemp <= 10) {
                NoTime.SetActive(true);
            } else {
                NoTime.SetActive(false);
            }
        }
    }

    //打死BOSS
    public void BossDie() {
        BossTimeTemp = 0f;
        isBossShow = false;
        BossTimeSlider.value = 1;       
        BossTimeGo.SetActive(false);
        //if (Panel.IsOpened("BossRollPanel")) {
        //    Panel.Close("BossRollPanel");
        //}
        //Panel.Open("BossRollPanel");
    }

   
  

    int BgIndex = 0;
    bool isNextBossColor = false;
    public void NextBoss() {
        NoTime.SetActive(false);
        if (!isNextBossColor && GameManager.instance.CheckIsBoss()) {
            isNextBossColor = true;
            BG1.gameObject.SetActive(true);
            BG2.gameObject.SetActive(true);
            if (BgIndex >= Bgs.Length - 1) {
                BgIndex = 0;
            }
            BgIndex++;
            BG2.sprite = Bgs[BgIndex];
            BG2.color = new Color(1, 1, 1, 0);
            BG1.DOFade(0, 1f);
            BG2.DOFade(1, 1f);
            //BG2.transform.localScale = Vector3.zero;
            //BG1.transform.DOScale(Vector3.zero, 1f);
            // BG2.transform.DOScale(new Vector3(1, 1, 1), 1f);

            EzTiming.CallDelayed(1.1f, () => {
                BG1.sprite = Bgs[BgIndex];
                BG1.color = new Color(1f, 1f, 1f, 1f);
                //BG1.transform.localScale = new Vector3(1, 1, 1);
                BG1.gameObject.SetActive(true);
                BG2.gameObject.SetActive(false);
                isNextBossColor = false;
            });
        }
        InitBossDistance();
        //GUIDE
        if(GameManager.instance.GetMonsterNum() >= 3 && isGuideEmojiCarry) {
            GameManager.instance.CanGuideStep8();
        }
    }
    private int DogFaceRepeatTextIndex = 0;
    private void InitBossDistance() {
            if (Localization.GetLanguage() == "CN") {
                BossDistancs.text = "距离Boss还有" + (GameManager.instance.GetBossDistance()) + "个";
            } else if (Localization.GetLanguage() == "CT") {
                BossDistancs.text = "距離Boss還有" + (GameManager.instance.GetBossDistance()) + "个";
            } else {
                BossDistancs.text = "Till Next Boss : " + (GameManager.instance.GetBossDistance());
            }


        if (GameManager.instance.GetBossDistance() == BossDistance || Boss.instance.isFailNextBoss) {
            BossDistancs.gameObject.transform.parent.gameObject.SetActive(false);
        }else {
            BossDistancs.gameObject.transform.parent.gameObject.SetActive(true);
        }
        if (GameManager.instance.monsterNum % 6 == 0) {
            //BossNumText.gameObject.SetActive(true);

            if (Localization.GetLanguage() == "CN") {
                BossNumText.text = "第<size=70>" + GameManager.instance.GetBossNum() + "</size>个BOSS";
            } else if (Localization.GetLanguage() == "CT") {
                BossNumText.text = "第<size=70>" + GameManager.instance.GetBossNum() + "</size>个BOSS";
            } else {
                BossNumText.text = "Boss #<size=70>" + GameManager.instance.GetBossNum() + "</size>";
            }
           
        } else {
            if (!Boss.instance.isFailNextBoss) {
                BossDistancs.gameObject.SetActive(true);
                if (Localization.GetLanguage() == "CN") {
                    BossNumText.text = "第<size=70>" + GameManager.instance.GetMonsterNum() + "</size>个emoji";
                } else if (Localization.GetLanguage() == "CT") {
                    BossNumText.text = "第<size=70>" + GameManager.instance.GetMonsterNum() + "</size>个emoji";
                } else {
                    BossNumText.text = "Emoji #<size=70>" + GameManager.instance.GetMonsterNum() + "</size>";
                }
            }else {
                BossDistancs.gameObject.SetActive(false);
                BossNumText.text = Localization.GetMultilineText("DogFaceRepeatText"+ DogFaceRepeatTextIndex);
                DogFaceRepeatTextIndex++;
                if(DogFaceRepeatTextIndex > 3) {
                    DogFaceRepeatTextIndex = 0;
                }
            }
            //BossNumText.gameObject.SetActive(false);
        }
    }

    public void ShowBottomEmoji() {
        for(int i = 0;i < equipMercenarys.Length;i++) {
            int mercenaryId =  GameManager.instance.GetEquipMercenary(i);
            if (mercenaryId != -1) {
                equipMercenarys[i].gameObject.SetActive(true);
                equipMercenarys[i].sprite = GameManager.instance.GetMercenaryDataForId(mercenaryId).icon;
                equipMercenarys[i].gameObject.GetComponent<Mercenary>().Init(GameManager.instance.mercenaryDatas[i],
                    GameManager.instance.skillDatas[i]);
                merBallSpawners[i].gameObject.SetActive(true);
            } else {
                equipMercenarys[i].gameObject.SetActive(false);
                merBallSpawners[i].gameObject.SetActive(false);
            }
        }
    }

    private void MercenaryAttack() {
        for (int i = 0; i < equipMercenarys.Length; i++) {
            int mercenaryId = GameManager.instance.GetEquipMercenary(i);
            if (mercenaryId != -1) {
                MercenaryData md =  GameManager.instance.GetMercenaryDataForId(mercenaryId);
                Boss.instance.BeAttack(md.attack * md.Level);
            }
        }
        //EzTiming.CallDelayed(0.3f, () => {
        //    MercenaryAttack();
        //});
    }

    public void ShowBossCome() {
        Panel.Open("BossCome");
        EzTiming.CallDelayed(3f, () => {
            Panel.Close("BossCome");
        });
    }

    public void ReBoss() {
        if (!Boss.instance.isDie) {
            int mounteraddNum = GameManager.instance.GetMonsterNum() % 6;
            //if (mounteraddNum == 0) {
            //    mounteraddNum = 1;
            //}
            mounteraddNum = 6 - mounteraddNum;
            Boss.instance.isFailNextBoss.Value = false;
            for (int i = 0; i < mounteraddNum; i++) {
                Boss.instance.NextBoss();
            }
            ReBossGo.SetActive(false);
            BossDistancs.gameObject.transform.parent.gameObject.SetActive(false);
            EzAnalytics.LogEvent("boss", "Re");

            EzAnalytics.LogEvent("boss", "Re", "Number", Boss.instance.failBossNum);
            Boss.instance.failBossNum = 0;
        }
    }

    public void ShowGetCoin() {
        Tweener tn =  CoinGo.transform.DOScale(1.2f, 0.3f);
        tn.OnComplete(() => {
            tn = CoinGo.transform.DOScale(1f, 0.3f);
        });

    }

    public void AddMonsterDieCoins(float value) {
        MonsterDieCoinCoefficient *= value;
    }

    public void RestoreMonsterDieCoins() {
        MonsterDieCoinCoefficient = 1;
    }


    public void ShowBarrages() {
        barrages.SetActive(true);
        EzTiming.CallDelayed(5f, () => {
            barrages.SetActive(false);
        });
    }

    public void ShowTimeFreez(float time) {
        timeFreez.SetActive(true);
        EzTiming.CallDelayed(time, () => {
            timeFreez.SetActive(false);
        });
    }

}
