﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MercenaryLevelUpPanel : Panel {

    public Image icon;
    public Text level;
    public Text attack;
    public GameObject skill;
    public MercenaryData mercenaryData;

	// Use this for initialization
	void Start () {
    }

        

    public void Init(MercenaryData md) {
        attack.text = mercenaryData.attack.ToString();
        level.text = mercenaryData.Level.Value.ToString();
        icon.sprite = mercenaryData.icon;

        this.mercenaryData = md;

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
