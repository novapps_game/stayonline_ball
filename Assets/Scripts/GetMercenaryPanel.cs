﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GetMercenaryPanel : Panel {

    public Image icon;
    public Image bg;
    public MercenaryData md;

    public List<Vector2> mercenarys;
    public DialogMercen dialogMercen;

    public GameObject Title;
    public GameObject BG;
    public GameObject Light;
    public Image Mercenary;
    public Text LevelInfo;


	// Use this for initialization
	void Start () {
        
    }

    public void Init(int index,MercenaryData md) {
        dialogMercen = GameController.instance.dialogMercen;
        mercenarys = new List<Vector2>();
        mercenarys.Add(new Vector2(-232.64f, -344f));
        mercenarys.Add(new Vector2(-139.17f, -344f));
        mercenarys.Add(new Vector2(-45.14f, -344f));
        mercenarys.Add(new Vector2(46.77f, -344f));
        mercenarys.Add(new Vector2(133.77f, -344f));
        mercenarys.Add(new Vector2(223.7f, -344f));
        this.md = md;
        Sprite sprite = md.icon;
        icon.sprite = sprite;
        EzTiming.CallDelayed(1f, () => {
            //1秒后隐藏BG
            Title.SetActive(false);
            BG.SetActive(false);
            Light.SetActive(false);
            Mercenary.enabled = false;
            // LevelInfo.SetActive(false);
            LevelInfo.text = md.name;
            icon.transform.DOLocalMove(mercenarys[index], 1.0f);
            icon.transform.DOScale(0, 1.0f);
            //GameController.instance.CheckBallFire();
            EzTiming.CallDelayed(1.1f, () => {
                if (GameManager.instance.CheckUnLockMercenary(index) != null) {
                    GameManager.instance.EquipMercenary(index, md.id);
                    GameController.instance.ShowBottomEmoji();
                    GameManager.instance.CostEmojiNum(index, 1);
                    EzTiming.CallDelayed(1.01f, () => {
                        dialogMercen.gameObject.SetActive(true);
                        dialogMercen.Init(sprite, md.info);
                        EzTiming.CallDelayed(1f, () => {
                            EzTiming.CallDelayed(1.5f, () => {
                                dialogMercen.gameObject.SetActive(false);
                                Panel.Close("GetMercenaryPanel");
                            });

                        });
                    });
                } else {
                    Panel.Close("GetMercenaryPanel");
                }
            });
            });
      

       
     
       

    } 

	 
	// Update is called once per frame
	void Update () {
		
	}

    public override void Close() {
        base.Close();
        Panel.Open("BossDiePanel");
    }
}
