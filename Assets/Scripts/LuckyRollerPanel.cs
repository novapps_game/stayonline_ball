﻿using I2.MiniGames;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LuckyRollerPanel : Panel {

    public PrizeWheel prizwWhell;
    public GainCoins gainCoin;
    public GameObject cheatBtn;
    public GameObject cheatAgainBtn;
    public GameObject CloseBtn;

    // Use this for initialization
    void Start() {
        gainCoin.gameObject.SetActive(false);
        cheatBtn.SetActive(true);
        cheatAgainBtn.SetActive(false);
    }

    // Update is called once per frame
    void Update() {

    }



    public void CheatBtnClick() {
        cheatBtn.SetActive(false);
        cheatAgainBtn.SetActive(true);
    }

    public void CheatAgain() {
        if (EzAds.IsRewardedVideoReady()) {
            EzAds.ShowRewardedVideo(() => {
                prizwWhell.StartSpinning(-1);
                cheatAgainBtn.SetActive(false);
                CloseBtn.SetActive(true);
            });
        }
    }

    public void OnFinishSpin() {
        int spinIndex = prizwWhell.spineIndex;
        Debug.LogError(spinIndex + "spinIndex");
        gainCoin.gameObject.SetActive(true);
        // 5,20,10,-1,15,40,25,-1
        switch (spinIndex) {
            case 0:
                prizwWhell.StartSpinning(-1);
                break;
            case 1:
                //加25
                GameManager.instance.GainCoins(25, "LuckRollerPanel");
                gainCoin.Gain(25, 1);
                break;
            case 2:
                //加40
                GameManager.instance.GainCoins(40, "LuckRollerPanel");
                gainCoin.Gain(40, 1);
                break;
            case 3:
                //加15
                GameManager.instance.GainCoins(15, "LuckRollerPanel");
                gainCoin.Gain(15, 1);
                break;
            case 4:
                prizwWhell.StartSpinning(-1);
                break;
            case 5:
                //加10
                GameManager.instance.GainCoins(10, "LuckRollerPanel");
                gainCoin.Gain(10, 1);
                break;
            case 6:
                //加20
                GameManager.instance.GainCoins(20, "LuckRollerPanel");
                gainCoin.Gain(20, 1);
                break;
            case 7:
                //加5
                GameManager.instance.GainCoins(5, "LuckRollerPanel");
                gainCoin.Gain(5, 1);
                break;
        }

    }
    public void Cheat() {
        prizwWhell.StartSpinning(-1);
    }

    public void ClickCloseBtn() {
        this.Close();
    }
}
 
