﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameBottomToggleManager : MonoBehaviour {

    public Toggle baseUpToggle;
    public Toggle attackUpToggle;
    public Toggle skillUpToggle;

    public Sprite toggleNormalSprite;
    public Sprite toggleIsOnSprite;
 

    private Color toggleNormalColor = Color.gray;
    private Color toggleIsOnColor = Color.white;                                                                                                                                                                                                                                                                                                                                                   

    public Image baseUpImage;
    public Image attackUpImage;
    public Image skillUpImage;
    public Text baseUpText;
    public Text attackUpText;
    public Text skillUpText;


    public GameObject baseUpGo;
    public GameObject attackUpGo;
    public GameObject skillUp;


    // Use this for initialization 
    void Start() {
        baseUpToggle.onValueChanged.AddListener((isOn) => {
            if (isOn) {
                baseUpImage.sprite = toggleIsOnSprite;
                baseUpText.color = toggleIsOnColor;
                baseUpGo.SetActive(true);
            } else {
                baseUpImage.sprite = toggleNormalSprite;
                baseUpText.color = toggleNormalColor;
                baseUpGo.SetActive(false);
            }
        });
        attackUpToggle.onValueChanged.AddListener((isOn) => {
              if (isOn) {
                attackUpImage.sprite = toggleIsOnSprite;
                attackUpText.color = toggleIsOnColor;
                attackUpGo.SetActive(true);
            } else {
                attackUpImage.sprite = toggleNormalSprite;
                attackUpText.color = toggleNormalColor;
                attackUpGo.SetActive(false);
            }
        });
        skillUpToggle.onValueChanged.AddListener((isOn) => {
            if (isOn) {
                skillUpImage.sprite = toggleIsOnSprite;
                skillUpText.color = toggleIsOnColor;
                skillUp.SetActive(true);
            } else {    
                skillUpImage.sprite = toggleNormalSprite;
                skillUpText.color = toggleNormalColor;
                skillUp.SetActive(false);
            }
        });
    }   
	
	// Update is called once per frame

	void Update () {
		
	}
}
