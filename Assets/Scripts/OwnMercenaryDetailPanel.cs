﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OwnMercenaryDetailPanel : Panel {

    public MercenaryData mercenaryData;
    public Text name;
    public Image icon;
    public Text emoji1Num;
    public Text emoji2Num;
    public Text emoji3Num;
    public Text attackNum;

    public Image emoji1Image;
    public Image emoji2Image;
    public Image emoji3Image;
    public GameObject EquipBtn;
    public Image LevelUpBtn;
    public Material gray;
    public Text LevelUpPrict;

    public Image qualityImage;
    public Text qualityText;





    // Use this for initialization
    void Start () {
        name.text = mercenaryData.name;
        icon.sprite = mercenaryData.icon;
        //emoji1Num.text = mercenaryData.GetEmoji1Num().ToString();
        //emoji2Num.text = mercenaryData.GetEmoji2Num().ToString();
        //emoji3Num.text = mercenaryData.GetEmoji3Num().ToString();
        //emoji1Image.sprite = GameManager.instance.LoadSprite("emoji/emoji"+mercenaryData.emoji1);
        //emoji2Image.sprite = GameManager.instance.LoadSprite("emoji/emoji" + mercenaryData.emoji2);
        //emoji3Image.sprite = GameManager.instance.LoadSprite("emoji/emoji" + mercenaryData.emoji3);
        attackNum.text = (mercenaryData.attack * mercenaryData.Level)+"";
        for(int i = 0;i < 6; i++) {
            if(GameManager.instance.GetEquipMercenary(i) == mercenaryData.id || !mercenaryData.IsOwn) {
                EquipBtn.SetActive(false);
            }
            if(mercenaryData.GetLevelUpCardNum() >= 3 && mercenaryData.GetPrice() <= GameManager.instance.coins) {
                LevelUpBtn.material = null;
            } else {
                LevelUpBtn.material = gray;
            }
        }
        LevelUpPrict.text = mercenaryData.GetPrice().ToString();

        //switch (mercenaryData.quality) {
        //    case 1:
        //        qualityImage.sprite = GameManager.instance.qualityBgSprite[0];
        //        qualityText.text = "普通";
        //        break;
        //    case 2:
        //        qualityImage.sprite = GameManager.instance.qualityBgSprite[1];
        //        qualityText.text = "高级";
        //        break;
        //    case 3:
        //        qualityImage.sprite = GameManager.instance.qualityBgSprite[2];
        //        qualityText.text = "稀有";
        //        break;
        //    case 4:
        //        qualityImage.sprite = GameManager.instance.qualityBgSprite[3];
        //        qualityText.text = "传奇";
        //        break;

        //}
    }



    public void EquipMercebary() {
        if (mercenaryData.IsOwn) {
            CollectPanel collectPanel = (CollectPanel)Panel.Open("CollectPanel");
            collectPanel.ShowSetMercenary(mercenaryData.id);
            Close();
        }
    }

    public void LevelUp() {
        if(mercenaryData.GetLevelUpCardNum() >= 3 && mercenaryData.GetPrice() <= GameManager.instance.coins) {
            GameManager.instance.UpdateMercenaryLevel(mercenaryData.id);
            Panel.Open("MercenaryLevelUpPanel");
        }
    }
}
