﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class BoxPanel : Panel {

    public Text hint;
    int boxType;
    Buff buff;
    public Image DoubleBtn;
    public Material gray;

    // Use this for initialization
    void Start() {
        GameController.instance.isGameOver = true;
        boxType = Random.Range(0, 2);
        if (boxType == 0) {
            hint.text = Localization.GetMultilineText("CoinChest");
            hint.text = (GameManager.instance.GetCoinPrice() * 100).ToString() + Localization.GetMultilineText("Coin");
        } else {
            buff = BuffManager.instance.CreateRandomBuff();
            if (buff.buffData.buffType == BuffType.AddCoin) {
                hint.text = Localization.GetMultilineText("CoinBoxInfo");
            } else {
                hint.text = Localization.GetMultilineText("AttackBoxInfo");
            }
        }
        if (!EzAds.IsRewardedVideoReady()) {
            //DoubleBtn.material = gray;
            DoubleBtn.gameObject.SetActive(false);
        } else {
            //DoubleBtn.material = null;
            DoubleBtn.gameObject.SetActive(true);
        }
            
        transform.DOScale(1.2f, 0.2f);
        EzTiming.CallDelayed(0.21f, () => {
            transform.DOScale(1f, 0.2f);
            //EzTiming.CallDelayed(0.21f, () => {
            //    transform.DOScale(1f, 0.2f);
            //});
        });
    }

    public void ClickCancelBtn() {
        if (boxType == 0) {
            CreateCoinBoss.instance.Gain(100, 1, "RewardedVideo", CoinType.Box);
            EzAnalytics.LogEvent("chest", "Coin", "Collect");
        } else {
            BuffManager.instance.StartBuff(buff);
            EzAnalytics.LogEvent("chest", "Bonus", "Collect");
        }
        Close();
    }
    public void ClickConfirmBtn() {
        if (EzAds.IsRewardedVideoReady()) {
            EzAds.ShowRewardedVideo(() => {
                if (boxType == 0) {
                    CreateCoinBoss.instance.Gain(300, 1,"RewardedVideo",CoinType.Box);
                    EzAnalytics.LogEvent("chest", "Coin", "AdsCollect");
                } else {
                    BuffManager.instance.StartDoubleBuff();
                    
                }

            }, ClickCancelBtn);
            Close();
        }
    }

    public override void Close() {
        base.Close();
        GameController.instance.isGameOver = false;
    }


}
