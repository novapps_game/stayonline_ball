﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomeCoins : MonoBehaviour {
    public BigNumberText bnt;
	// Use this for initialization
	void Start () {
        bnt = GetComponent<BigNumberText>();
       
    }
	
	// Update is called once per frame
	void Update () {
        bnt.SetValue(GameManager.instance.coins);
    }
}
