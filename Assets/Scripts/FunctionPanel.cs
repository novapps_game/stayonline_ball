﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FunctionPanel : Panel {

    public Text coinNum;
    public Text AttackNum;
    public Text Level;
    public Image btn;

    public Sprite CanClick;
    public Sprite UnClick;

	// Use this for initialization
	void Start () {
        init();
    }

    private void init() {
        float levelCoin = (25f * Mathf.Pow(GameManager.instance.ballAttackLevel, 1.18f) + 15);
        coinNum.text = ((int)levelCoin).ToString();
        AttackNum.text = GameManager.instance.GetBallAttack() + " - " + (GameManager.instance.GetBallAttack() + 1);
        Level.text = "LV"+GameManager.instance.ballAttackLevel.ToString();
        if (GameManager.instance.coins < levelCoin) {
            btn.sprite = UnClick;
        } else {
            btn.sprite = CanClick;
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void UpLevel() {
        GameManager.instance.AddBattleAttackLevel();
        init();
    }
}
