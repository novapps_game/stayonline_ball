﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MercenaryItem : MonoBehaviour {

    public Image bg;
    public Image Bottom;
    public GameObject EmojiPanel;
    public Text LevelButton;
    public Text InfoButton;
    public MercenaryData mercenary;
    public bool isSelect;
    public Image mercenaryImage;

    public GameObject LevelNumInfo;

    public Sprite[] bgSprites;

    public Material gray;

    public Sprite emptyIcon;
    public GameObject panel;
    public bool isEquip = false;


	// Use this for initialization
	void OnEnable () {
        Init();
    }



    public void Init() {
        if (mercenary.IsOwn) {
            //if (!isSelect) {
            //    if (mercenary.GetEmoji1Num() > 0 && mercenary.GetEmoji2Num() > 0 && mercenary.GetEmoji3Num() > 0) {
            //        ShowLevenButton();
            //    } else {
            //        ShowEmoji();
            //    }
            //} else {
            //    if (mercenary.GetEmoji1Num() > 0 && mercenary.GetEmoji2Num() > 0 && mercenary.GetEmoji3Num() > 0) {
            //        ShowLevenButton();
            //    } else {
            //        ShowInfoButton();
            //    }
            //}
     

        }
        if (!mercenary.IsOwn) {
            if (!isSelect) {
                ShowEmoji();
            } else {
                ShowInfoButton();
            }
         
        }
        bg.sprite = bgSprites[0];
        panel = transform.Find("Panel").gameObject;
        panel.AddComponent<Button>();
        panel.GetComponent<Button>().onClick.AddListener(() => {
            if (!isEquip && mercenary != null && mercenary.id > 0) {
                OpenDetail();
            }
        });

       
    }

	
	// Update is called once per frame
	void Update () {
		
	}


    private void ShowEmoji() {
        EmojiPanel.SetActive(true);
        LevelButton.gameObject.SetActive(false);
        InfoButton.gameObject.SetActive(false);


        //EmojiPanel.transform.GetChild(0).GetComponent<Image>().sprite = GameManager.instance.LoadSprite("emoji/emoji" + mercenary.emoji1);
        //EmojiPanel.transform.GetChild(1).GetComponent<Image>().sprite = GameManager.instance.LoadSprite("emoji/emoji" + mercenary.emoji2);
        //EmojiPanel.transform.GetChild(2).GetComponent<Image>().sprite = GameManager.instance.LoadSprite("emoji/emoji" + mercenary.emoji3);
        //if (mercenary.GetEmoji1Num() <= 0) {
        //    EmojiPanel.transform.GetChild(0).GetComponent<Image>().material = gray; 
        //} else {
        //    EmojiPanel.transform.GetChild(0).GetComponent<Image>().material = null;
        //}
        //if (mercenary.GetEmoji2Num() <= 0) {
        //    EmojiPanel.transform.GetChild(1).GetComponent<Image>().material = gray;
        //} else {
        //    EmojiPanel.transform.GetChild(1).GetComponent<Image>().material = null;
        //}
        //if (mercenary.GetEmoji3Num() <= 0) {
        //    EmojiPanel.transform.GetChild(2).GetComponent<Image>().material = gray;
        //} else {
        //    EmojiPanel.transform.GetChild(2).GetComponent<Image>().material = null;
        //}
    }

    private void ShowLevenButton() {
        EmojiPanel.SetActive(false);
        LevelButton.gameObject.SetActive(true);
        InfoButton.gameObject.SetActive(false);
    }
    
    private void ShowInfoButton() {
        EmojiPanel.SetActive(false);
        LevelButton.gameObject.SetActive(false);
        InfoButton.gameObject.SetActive(true);
        InfoButton.text = "信息";
    }

    private void ShowGetMercenary() {
        EmojiPanel.SetActive(false);
        LevelButton.gameObject.SetActive(false);
        InfoButton.gameObject.SetActive(true);
        InfoButton.text = "可获取";
    }

    public void ShowEmpty() {
        EmojiPanel.SetActive(false);
        LevelButton.gameObject.SetActive(false);
        InfoButton.gameObject.SetActive(false);
        bg.sprite = bgSprites[0];
        LevelNumInfo.SetActive(false);
        mercenaryImage.sprite = emptyIcon;
        mercenaryImage.SetNativeSize();

    }

    public void OpenDetail() {
        OwnMercenaryDetailPanel omd = (OwnMercenaryDetailPanel)Panel.Open("OwnMercenaryDetailPanel");
        omd.mercenaryData = this.mercenary;
    }
}
