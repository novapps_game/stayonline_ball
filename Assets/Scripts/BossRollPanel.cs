﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class BossRollPanel : Panel {
    public GameObject cards;
    public Image[] emojiBg;

    private bool isSelectCard = true;

    public List<int> emojiIds;

    public List<GameObject> cardGos;

    private List<Vector3> cardPoss;
    public Sprite cardBG;
    public Sprite cardFront;

    //public GameObject FrontCardsGo;

	// Use this for initialization
	void Start () {
        //RollAnima();
        emojiIds = GameManager.instance.GetBossRollEmoji();
        cardPoss = new List<Vector3>();
        GameController.instance.isGameOver = true;
        for (int i = 0;i < cardGos.Count;i++) {
            cardGos[i].gameObject.GetComponent<MercenaryCard>().MercenaryId = emojiIds[i];
            cardGos[i].transform.Find("Emoji").GetComponent<Image>().sprite = GameManager.instance.LoadSprite("mercenary/mercenary"+ emojiIds[i]);
            cardPoss.Add(cardGos[i].transform.position);
        }   
        EzTiming.CallDelayed(1f, () => {
            foreach (GameObject card in cardGos) {
                card.transform.DOLocalRotate(new Vector3(0, 180, 0), 1f);
                EzTiming.CallDelayed(0.3f, () => {
                    card.transform.Find("Emoji").GetComponent<Image>().enabled = false;
                    card.GetComponent<Image>().sprite = cardBG;
                });
            }
            EzTiming.CallDelayed(1.2f, () => {
                ShuffleAnima();
                EzTiming.CallDelayed(3f, () => {
                    isStopShuffle = true;
                    isSelectCard = false;
                    foreach (GameObject card in cardGos) {
                        card.transform.localRotation = Quaternion.Euler(0, 0, 0);
                    }
                });
            });
        });
        EzAnalytics.LogEvent("boss", "GetCard", "bossGetCard");
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void ChangeCardIndex(GameObject cardGo1,GameObject cardGo2 ,int card1Index,int card2Index) {
        cardGo1.transform.SetSiblingIndex(card2Index);
        cardGo2.transform.SetSiblingIndex(card1Index);
    }

    bool isStopShuffle = false;
    private void ShuffleAnima() {
        if (!isStopShuffle) {
            int card1RandomIndex = Random.Range(1, 3);
            cardGos[0].transform.DOMoveX(cardPoss[card1RandomIndex].x, 0.3f);
            List<int> tempIndex = new List<int>() { 0, 1, 2 };
            tempIndex.Remove(card1RandomIndex);
            int card2RandomIndex = EzRandom.Pick(tempIndex);
            cardGos[card1RandomIndex].transform.DOMoveX(cardPoss[card2RandomIndex].x, 0.3f);
            tempIndex.Remove(card2RandomIndex);
            int card3RandomIndex = EzRandom.Pick(tempIndex);
            if(card2RandomIndex == 0) {
                card2RandomIndex = card3RandomIndex;
            }
            cardGos[card2RandomIndex].transform.DOMoveX(cardPoss[card3RandomIndex].x, 0.3f);

            EzTiming.CallDelayed(0.31f, () => {
                ChangeCardIndex(cardGos[0], cardGos[card1RandomIndex],0, card1RandomIndex);
                ChangeCardIndex(cardGos[card1RandomIndex], cardGos[card2RandomIndex], card1RandomIndex, card2RandomIndex);
                ChangeCardIndex(cardGos[card2RandomIndex], cardGos[card3RandomIndex], card2RandomIndex, card3RandomIndex);
                ShuffleAnima();
            });
       
        }
    }


    public void SelectCard(Transform tf) {
        if (!isSelectCard) {
            isSelectCard = true;
            Transform selectCardTf = tf;
            selectCardTf.DOLocalRotate(new Vector3(0, 180, 0), 1f);
            //tf.DOLocalRotate(new Vector3(0, 180, 0), 1f);
            EzTiming.CallDelayed(0.3f, () => {
                tf.Find("Emoji").GetComponent<Image>().enabled = true;
                tf.GetComponent<Image>().sprite = cardFront;
            });

            EzTiming.CallDelayed(1f, () => {
                int id = tf.GetComponent<MercenaryCard>().MercenaryId;
                GameManager.instance.AddOwnEmoji(id);
               // emoji.sprite = GameManager.instance.LoadSprite("mercenary/mercenary" + id);
                //MercenaryData md = GameManager.instance.CheckUnLockMercenary(id);
                MercenaryData md =  GameManager.instance.mercenaryDatas[id];
                //if (md != null) {
                    GetMercenaryPanel getMercenaryPanel = (GetMercenaryPanel)Panel.Open("GetMercenaryPanel");
                    //getMercenaryPanel.Init(GameManager.instance.currentMercenaryIndex.Value,md);
                    getMercenaryPanel.Init(id, md);
                    GameManager.instance.currentMercenaryIndex.Value++;
                
                    EzAnalytics.LogEvent("boss", "GetCard", "FirstNumber",GameManager.instance.GetBossNum());
                //} else {

                //    Panel.Open("BossDiePanel");
                //}
                EzTiming.CallDelayed(1f, () => {
                    Panel.Close("BossRollPanel");
                });
            });
        }
    }

    protected override void OnClose() {
        base.OnClose();
        
    }


    //bool isRoll = true;
    //Image currentRollEmojiBg;




    //private void RollAnima() {
    //    if (isRoll) {   
    //        foreach (Image image in emojiBg) {
    //            image.gameObject.SetActive(false);
    //        }
    //        emojiBg[0].gameObject.SetActive(true);
    //        currentRollEmojiBg = emojiBg[0];
    //        EzTiming.CallDelayed(0.2f, () => {
    //            emojiBg[0].gameObject.SetActive(false);
    //            emojiBg[1].gameObject.SetActive(true);
    //            currentRollEmojiBg = emojiBg[1];
    //            EzTiming.CallDelayed(0.2f, () => {
    //                emojiBg[0].gameObject.SetActive(false);
    //                emojiBg[1].gameObject.SetActive(false);
    //                emojiBg[2].gameObject.SetActive(true);
    //                currentRollEmojiBg = emojiBg[2];
    //                EzTiming.CallDelayed(0.2f, () => {
    //                    RollAnima();
    //                });
    //            });
    //        });
    //    }
    //}


    public void ReGetCard() {
        EzAds.ShowRewardedVideo(() => {
            isSelectCard = false;
        });
    }

    public override void Close() {
        base.Close();
     
    }
}

