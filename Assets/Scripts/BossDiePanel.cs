﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossDiePanel : Panel {

    public Text bossNum;
    public Text coinNum;
    public Text btnText;
    public GameObject videoIcon;
	// Use this for initialization
	void Start () {
        GameController.instance.isGameOver = false;
        if (Localization.GetLanguage() == "CN") {
            bossNum.text = "第<color=red>" + GameManager.instance.GetBossNum() + "</color>个Boss";
        } else if (Localization.GetLanguage() == "CT") {
            bossNum.text = "第<color=red>" + GameManager.instance.GetBossNum() + "</color>個Boss";
        } else {
            bossNum.text = "BossNum :<color=red>" + GameManager.instance.GetBossNum() + "</color>";
        }

        coinNum.text = (GameManager.instance.GetMonsterNum() * 120).ToString();
        if (EzAds.IsRewardedVideoReady()) {
            if (Localization.GetLanguage() == "CN") {
                btnText.text = "领取 X2";
            }else if(Localization.GetLanguage() == "CT") {
                btnText.text = "领取 X2";
            }else {
                btnText.text = "Get X2";
            }
            videoIcon.SetActive(true);
        } else {
            if (Localization.GetLanguage() == "CN") {
                btnText.text = "领取";
            } else if (Localization.GetLanguage() == "CT") {
                btnText.text = "領取";
            } else {
                btnText.text = "Get";
            }
            videoIcon.SetActive(false);
        }
        GameController.instance.isGameOver = true;


    }

    public void GetCoins() {
        if (EzAds.IsRewardedVideoReady()) {
            EzAds.ShowRewardedVideo(() => {
                CreateCoinBoss.instance.Gain((int)(GameController.instance.BossDieCoins * GameController.instance.MonsterDieCoinCoefficient)*2, 1);
            });
        } else {
            CreateCoinBoss.instance.Gain((int)(GameController.instance.BossDieCoins * GameController.instance.MonsterDieCoinCoefficient), 1);
        }
        GameController.instance.isGameOver = false;
        Close();
    }

    public void ClickClose() {
        CreateCoinBoss.instance.Gain(120, 1);
        GameController.instance.isGameOver = false;
        Close();
    }

}
