﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MercenarySkillItem : ScrollListItem {
    public Text levelInfo;
    public Text Price;
    public Image icon;
    SkillData skillData;
    public Material gray;
    public Image UpBtn;
    public Image CanUp;
    public Text SkillName;
    public Text SkillInfo;
    public Image priceIcon;
    public TweenFade tf;

    public GameObject LevelUpAnima;

    // Use this for initialization
    void Start() {
        //mercenary = GameController.instance.equipMercenarys[index].gameObject.GetComponent<Mercenary>();
        skillData = GameManager.instance.skillDatas[index];
        tf = GetComponent<TweenFade>();
        levelInfo.text = "Lv" + (skillData.level.Value).ToString();
        icon.sprite = skillData.icon;
        Price.GetComponent<BigNumberText>().SetValue(skillData.GetPrice());
        SkillName.text = skillData.name;
        SkillInfo.text = GetSkillInfo();
        CheckCanUp();
        priceIcon.sprite = GameManager.instance.LoadSprite("mercenary/mercenaryCard"+ index);
        GameManager.instance.coins.AddObserver((level) => {
            levelInfo.text = "Lv"+(skillData.level.Value).ToString();
            Price.GetComponent<BigNumberText>().SetValue(skillData.GetPrice());
            SkillInfo.text = GetSkillInfo();
            CheckCanUp();
        });
        skillData.level.AddObserver((level) => {
            levelInfo.text = "Lv" + (skillData.level.Value).ToString();
            Price.GetComponent<BigNumberText>().SetValue(skillData.GetPrice());
            SkillInfo.text = GetSkillInfo();
            CheckCanUp();
        });
        //attack.text = mercenary.GetAttack().ToString();
        // Price.text = mercenary.GetPrice().ToString();
    }

    private void CheckCanUp() {
        if (skillData.GetPrice() > GameManager.instance.GetOwnEmojiNum(skillData.skillId)) {
            UpBtn.material = gray;
            icon.material = gray;   
            CanUp.gameObject.SetActive(false);
            if (tf.alpha != 0.5f) {
                tf.alpha = 0.5f;
                tf.PlayNow();
            }
        } else {
            UpBtn.material = null;
            icon.material = null;
            CanUp.gameObject.SetActive(true);
            if (tf.alpha != 1) {
                tf.alpha = 1;
                tf.PlayNow();
            }
        }
    }

    // Update is called once per frame
    void Update() {

    }

    public void UpLevel() {
        if (skillData.AddLevel()) {
            skillData.level.Value++;
            LevelUpAnima.SetActive(true);
            GameManager.instance.PlaySound("levelup");
            EzAnalytics.LogEvent("cardSkill", "Upgrade", (index + 1).ToString());
        }
  
    }

    public string GetSkillInfo() {
        //switch (skillData.skillId) {
        //    case 0:

        //        return "持续5秒对BOSS造成每秒伤害"+ (Mathf.Pow(skillData.level, 2) * 10 + 85);
        //    case 1:
        //        return "冻结BOSS挑战的时间进度" + (skillData.level+5)+"秒";
        //    case 2:
        //        return "必定刷新暴击球，持续" + (skillData.level + 5) + "秒（暴击球会收到被动加成）";
        //    case 3:
        //        return "增加全体球球伤害*(1+" + (skillData.level * 50) + "%)";
        //    case 4:
        //        return "增加全体佣兵伤害*(1+" + (skillData.level * 50) + "%)";
        //    case 5:
        //        return "增加宝箱金币价值*(1+" + (skillData.level * 50) + "%)";
        //}
        switch (skillData.skillId) {
            case 0:

                return Localization.GetMultilineText("Skill1Info");
            case 1:
                return Localization.GetMultilineText("Skill2Info");
            case 2:
                return Localization.GetMultilineText("Skill3Info");
            case 3:
                return Localization.GetMultilineText("Skill4Info") + (100+skillData.level * 50) + "%";
            case 4:
                return Localization.GetMultilineText("Skill5Info") + (100+skillData.level * 50) + "%";
            case 5:
                return Localization.GetMultilineText("Skill6Info") + (100+skillData.level * 80) + "%";
        }
        return "";
    }
}
