﻿using UnityEngine;

public class BossCameraShake : MonoBehaviour {

    public float shake = 2;

    float setShake;

    bool shakeSwitch = false;

    Vector3 originalPos;

    void Start() {

        setShake = shake;

        originalPos = gameObject.transform.position;

    }

    void Update() {

        if (Input.GetKeyDown(KeyCode.S)) {

            shake = setShake;

            shakeSwitch = true;

        }

        if (shakeSwitch == true) {

            CameraShakeFun();

        }

    }

    public void Shake() {
        shake = setShake;
        shakeSwitch = true;
    }

    void CameraShakeFun() {

        gameObject.transform.position = new Vector3(

            Random.Range(0f, shake * 1.3f) - shake + originalPos.x,

            Random.Range(0f, shake * 0.8f) - shake + originalPos.y,

            Random.Range(0f, shake * 1.3f) - shake + originalPos.z);

        shake = shake / 1.05f;

        if (shake < 0.05) {

            shake = 0;

            shakeSwitch = false;

            gameObject.transform.position = originalPos;

        }

    }

}
