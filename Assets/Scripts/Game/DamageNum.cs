﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class DamageNum : MonoBehaviour {
    float attack;
    // Use this for initialization
    void OnEnable() {
        transform.localScale = new Vector3(1, 1, 1);
        attack = (int)GameManager.instance.GetBallAttack();
        if (GameManager.instance.CheckIsBoss()) {
            attack += GameManager.instance.GetBossExtralAttack();
        }
        transform.GetComponent<Text>().text = attack.ToString();
        
    }

    public void ShowNomalDamage(float damageNum = 0) {
        if(damageNum == 0) {
            damageNum = attack;
        }
        transform.GetComponent<Text>().text = damageNum.ToString();
        transform.GetComponent<Text>().transform.DOScale(2f, 0.05f);
        EzTiming.CallDelayed(0.06f, () => {
            transform.DOScale(1f, 0.15f);
        });
        EzTiming.CallDelayed(0.16f, () => {
            transform.DOLocalMoveY(800f, 2.0f);
        });
        EzTiming.CallDelayed(0.2f, () => {
            transform.GetComponent<Text>().DOFade(0, 2.0f);

        });
        EzTiming.CallDelayed(3f, () => {
            DamageNumSpawner.instance.Recycle(this);
        });
    }

    public void ShowCritDamage() {
        transform.GetComponent<Text>().color = Color.red;
        transform.GetComponent<Text>().text = ((int)(attack * 5f)).ToString();


        transform.GetComponent<Text>().transform.DOScale(6f, 0.05f);
        EzTiming.CallDelayed(0.06f, () => {
            transform.DOScale(2f, 0.15f);
        });
        EzTiming.CallDelayed(0.16f, () => {
            transform.DOLocalMoveY(800f, 2.0f);
        });
        EzTiming.CallDelayed(0.2f, () => {
            transform.GetComponent<Text>().DOFade(0, 2.0f);
        });
        EzTiming.CallDelayed(3f, () => {
            DamageNumSpawner.instance.Recycle(this);
        });
    }

    // Update is called once per frame
    void Update() {

    }


    


  
}
