﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSpawner : EzSpawner<Ball, BallSpawner> {

    public override Ball Create() {
        if (GameController.instance.TotalBallNum < 80 && !GameController.instance.isGameOver) {
            GameController.instance.TotalBallNum++;
            Ball ball = base.Create();
            ball.ballspawner = this;
            ball.gameObject.transform.localPosition = Vector3.zero;
            EzTiming.CallDelayed(0.1f, () => {
                ball.gameObject.transform.SetParent(transform.parent, true);
            });
            return ball;
        }

        return null;
     
    }

    protected override void Update() {
        base.Update();
        if(GameController.instance.TotalBallNum >= 80) {
            paused = true;
        }else {
            paused = false;
        }
    }

    public override void Recycle(Ball obj) {
        base.Recycle(obj);
        obj.GetComponent<Rigidbody2D>().isKinematic = false;
        obj.CanAttackTimeTemp = 0f;
        obj.isAttack = false;
        obj.transform.localPosition = Vector3.zero;
        obj.transform.localScale = new Vector3(0.6f, 0.6f, 1);
        obj.goneEffect.SetActive(false);
        obj.GetComponent<SpriteRenderer>().enabled = true;
        obj.isCrit = false;
        obj.goneEffect.transform.localPosition = new Vector3(0, 0, 0);
        obj.goneEffect.transform.localScale = new Vector3(3.33f, 3.33f, 2f);
        obj.CritClickEffect.SetActive(false);
        obj.CritClickEffect.transform.localPosition = new Vector3(0, 0, 0);
        obj.CritClickEffect.transform.localScale = new Vector3(3.33f, 3.33f, 2f);
        // obj.daji4.SetActive(false);
    }
}
