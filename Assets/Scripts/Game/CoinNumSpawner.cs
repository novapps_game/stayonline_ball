﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinNumSpawner : EzPool<CoinNum,CoinNumSpawner> {
    public override void Recycle(CoinNum comp) {
        base.Recycle(comp);
        comp.transform.GetComponent<Text>().color = new Color(0, 1, 0, 1);
    }
}
