﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReloadBtn : Singleton<ReloadBtn> {

    public Sprite ReloadCN1;
    public Sprite ReloadCN2;

    public Sprite ReloadEN1;
    public Sprite ReloadEN2;

    public Sprite ReloadTW1;
    public Sprite ReloadTW2;

    public SpriteRenderer sr;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public void DownBtn() {
        if(Localization.GetLanguage() == "CN") {
            sr.sprite = ReloadCN2;
        } else if (Localization.GetLanguage() == "CT") {
            sr.sprite = ReloadTW2;
        } else {
            sr.sprite = ReloadEN2;
        }
        GameManager.instance.PlaySound("reload");
    }

    public void UpBtn() {
        if (Localization.GetLanguage() == "CN") {
            sr.sprite = ReloadCN1;
        } else if (Localization.GetLanguage() == "CT") {
            sr.sprite = ReloadTW1;
        } else {
            sr.sprite = ReloadEN1;
        }
    }
}
