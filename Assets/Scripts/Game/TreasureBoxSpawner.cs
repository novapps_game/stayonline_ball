﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreasureBoxSpawner : EzSpawner<GameTreasureBos, TreasureBoxSpawner> {

    public override void Recycle(GameTreasureBos obj) {
        base.Recycle(obj);
        obj.GetComponent<Rigidbody2D>().isKinematic = false;
        obj.CanAttackTimeTemp = 0f;
        obj.transform.localPosition = Vector3.zero;
    }

    public override GameTreasureBos Create() {
        if (GameManager.instance.guidestep8) {
            return base.Create();
        }
        return null;
    }





}
