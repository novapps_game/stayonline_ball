﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Ball : MonoBehaviour {

    public TweenMove tm;
    private RaycastHit ObjHit;
    private Ray CustomRay;
    public bool isAttack = false;

    public BallSpawner ballspawner;

    private float attack = 9f;
    private float CanAttackTime = 1.2f;
    public float CanAttackTimeTemp = 0f;

    public SpriteRenderer sr;

    public GameObject goneEffect;

    public GameObject ClickEffect;
    public GameObject CritClickEffect;
    public GameObject tuowei;
    private bool isAttacking = false;

    //public GameObject daji4;

    
    // Use this for initialization
    void Start () {
        //daji4.SetActive(false);
        //sr.sprite = Boss.instance.BossSr;
    }
    public bool isCrit = false;
    private void OnEnable() {
        //  gameObject.transform.localPosition = Vector3.zero;
        //if (SkinManager.instance.isEquipSkin) {
        //    sr.sprite = Boss.instance.BossSr.sprite;
        //}
        isAttacking = false;
        attack = (int)GameManager.instance.GetBallAttack();
        sr.sprite = GameManager.instance.LoadSprite("emoji/"+Random.Range(1, 73));
        float criticalStrike = Random.Range(0.0f, 101.0f);
        if(criticalStrike <= GameManager.instance.criticalStrike) {
            sr.sprite = GameManager.instance.LoadSprite("emoji/" + 100);
            attack *= 5f;
            isCrit = true;
        }
    }
    // Update is called once per frame
    void Update () {
        CanAttackTimeTemp += Time.deltaTime;
        if (CanAttackTimeTemp > CanAttackTime && !GameController.instance.isGameOver && Input.touchCount > 0) {
            //CustomRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            Collider2D[] cols = Physics2D.OverlapCapsuleAll(Camera.main.ScreenToWorldPoint(Input.mousePosition),new Vector2(1,1),CapsuleDirection2D.Horizontal,90);
            if (cols.Length > 0) {
                foreach (Collider2D col in cols) {
                    if (col != null && col.gameObject == this.gameObject && !isAttack) {
                        GetComponent<Rigidbody2D>().isKinematic = true;
                        isAttacking = true;
                        tm.PlayNow();
                        gameObject.transform.DOScale(new Vector3(0.5f,0.5f,0.5f), 0.2f);
                        isAttack = true;
                        if (isCrit) {
                            GameManager.instance.PlaySound("hit_heavy");
                        } else {
                            GameManager.instance.PlaySound("hit_light");
                        }
                        ClickEffect.SetActive(true);
                        //daji4.SetActive(true);
                        EzTiming.CallDelayed(0.2f, () => {
                            GetComponent<SpriteRenderer>().enabled = false;
                            ClickEffect.SetActive(false);
                        });
                        tuowei.SetActive(true);
                       

                        EzTiming.CallDelayed(0.3f, () => {
                            GameController.instance.TotalBallNum--;
                            Debug.Log(GameController.instance.TotalBallNum);
                            Boss.instance.BeAttack(attack);
                            //DestroyImmediate(this.gameObject);
                            DamageNum damageNum = DamageNumSpawner.instance.Create();
                            if (isCrit) {
                                damageNum.ShowCritDamage();
                                GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
                                CritClickEffect.SetActive(true);
                            } else {
                                damageNum.ShowNomalDamage();
                            }
                            damageNum.GetComponent<RectTransform>().anchoredPosition = Camera.main.WorldToScreenPoint(transform.position) * (1f / GameController.instance.canvas.scaleFactor);
                            goneEffect.transform.localPosition = goneEffect.transform.localPosition.NewXY(goneEffect.transform.localPosition.x + Random.Range(-1.5f, 1.5f),
                            goneEffect.transform.localPosition.y + Random.Range(-1.5f, 1.5f));
                            goneEffect.transform.localScale = goneEffect.transform.localScale.NewXY(3.33f * Random.Range(0.6f, 1f),
                                3.33f * Random.Range(0.6f, 1f));

                            CritClickEffect.transform.localPosition = CritClickEffect.transform.localPosition.NewXY(CritClickEffect.transform.localPosition.x + Random.Range(-1f, 1f),
                            CritClickEffect.transform.localPosition.y + Random.Range(-1f, 1f));
                            CritClickEffect.transform.localScale = CritClickEffect.transform.localScale.NewXY(6.33f * Random.Range(0.6f, 1f),
                                6.33f * Random.Range(0.6f, 1f));
                           
                            tm.Stop();
                            goneEffect.SetActive(true);
                            tuowei.SetActive(false);
                            //transform.SetParent(Boss.instance.transform, true);
                            EzTiming.CallDelayed(0.21f, () => {
                                if (ballspawner != null) {
                                    ballspawner.Recycle(this);
                                } else {
                                    BallSpawner.instance.Recycle(this);
                                }
                            });
                        });
                    }
                }
            }
        }
        if (!isAttacking && transform.position.y >= 1.54f) {
            if(ballspawner != null) {
                GameController.instance.TotalBallNum--;
                ballspawner.Recycle(this);
            } else {
                GameController.instance.TotalBallNum--;
                BallSpawner.instance.Recycle(this);
            }
        }

    }

     
}
