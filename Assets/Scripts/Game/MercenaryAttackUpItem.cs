﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MercenaryAttackUpItem : ScrollListItem {

    public Text attack;
    public Text Price;
    public Image icon;

    MercenaryData mercenary;

    public Material gray;
    public Image UpBtn;
    public Image CanUp;
    public Text LevelInfo;
    public Text name;
    public TweenFade tf;
    public GameObject LevelUpAnima;

    // Use this for initialization
    void Start () {
        //mercenary = GameController.instance.equipMercenarys[index].gameObject.GetComponent<Mercenary>();
        mercenary = GameManager.instance.mercenaryDatas[index];
        attack.text = (50 * mercenary.Level).ToString();
        attack.GetComponent<BigNumberText>().SetValue(50 * mercenary.Level);
        icon.sprite = mercenary.icon;
        Price.GetComponent<BigNumberText>().SetValue(mercenary.GetPrice());
        LevelInfo.text = "Lv"+mercenary.Level.Value.ToString() ;
        name.text = mercenary.name;
        tf = GetComponent<TweenFade>();
        CheckCanUp();
        GameManager.instance.coins.AddObserver((level)=> {
            attack.text = (mercenary.GetAttack()).ToString();
            attack.GetComponent<BigNumberText>().SetValue(50 * mercenary.Level);
            Price.GetComponent<BigNumberText>().SetValue(mercenary.GetPrice());
            LevelInfo.text = "Lv" + mercenary.Level.Value.ToString();
            CheckCanUp();

        });
        mercenary.Level.AddObserver((level) => {
            attack.text = (mercenary.GetAttack()).ToString();
            attack.GetComponent<BigNumberText>().SetValue(50 * mercenary.Level);
            Price.GetComponent<BigNumberText>().SetValue(mercenary.GetPrice());
            LevelInfo.text = "Lv" + mercenary.Level.Value.ToString();
            CheckCanUp();

        });
        //attack.text = mercenary.GetAttack().ToString();
        // Price.text = mercenary.GetPrice().ToString();
    }
	
	// Update is called once per frame
	void Update () {

    }

    public void UpLevel() {
        long price = mercenary.GetPrice();
        if (GameManager.instance.coins >= price && GameManager.instance.GetEquipMercenary(index) != -1) {
            GameManager.instance.CostCoins(price);
            mercenary.Level.Value++;
            LevelUpAnima.SetActive(true);
            GameManager.instance.PlaySound("levelup");
            EzAnalytics.LogEvent("cardDamage", "Upgrade", (index+1).ToString());
        }
    }

    private void CheckCanUp() {
        if (mercenary.GetPrice() > GameManager.instance.coins || GameManager.instance.GetEquipMercenary(index) == -1) {
            UpBtn.material = gray;
            CanUp.gameObject.SetActive(false);
            icon.material = gray;
            if (tf.alpha != 0.5f) {
                tf.alpha = 0.5f;
                tf.PlayNow();
            }
        } else {
            UpBtn.material = null;
            CanUp.gameObject.SetActive(true);
            icon.material = null;
            if (tf.alpha != 1) {
                tf.alpha = 1;
                tf.PlayNow();
            }
        }
    }
}
