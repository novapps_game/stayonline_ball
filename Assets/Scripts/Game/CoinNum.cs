﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class CoinNum : MonoBehaviour {



	// Use this for initialization
	void OnEnable() {
        transform.GetComponent<Text>().text = GameManager.instance.GetCoinPrice().ToString();
        transform.GetComponent<Text>().transform.DOScale(3f, 0.05f);
        EzTiming.CallDelayed(0.06f, () => {
            transform.DOScale(1f, 0.15f);
        });
        //EzTiming.CallDelayed(0.16f, () => {
        //    transform.DOLocalMoveY(10f,1.0f);
        //});
        EzTiming.CallDelayed(0.2f, () => {
            transform.GetComponent<Text>().DOFade(0, 1.0f);
        });
        EzTiming.CallDelayed(2.1f, () => {
            CoinNumSpawner.instance.Recycle(this);
        });



    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
