﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffManager : Singleton<BuffManager> {

    public List<Buff> allBuffs;

    private Vector2[] buffPos = new Vector2[] {new Vector2(-9,150), new Vector2(145, 150)
    ,new Vector2(301,150),new Vector2(466,150)};

    private List<Buff> buffColumn = new List<Buff>();


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    int buffIndex;
    public Buff CreateRandomBuff() {
        buffIndex = Random.Range(0, 2);
        allBuffs[buffIndex].InitBuff(GameManager.instance.buffDatas[buffIndex]);
        return allBuffs[buffIndex];
    }

    public void StartDoubleBuff() {
        allBuffs[buffIndex+2].InitBuff(GameManager.instance.buffDatas[buffIndex+2]);
        allBuffs[buffIndex+2].gameObject.SetActive(true);
    }

    public void StartBuff(Buff buff) {
        buff.gameObject.SetActive(true);
        buff.InitDiration();
    }

    public void ResetBuffPos() {
        for(int i = 0;i < buffColumn.Count; i++) {
            buffColumn[i].CalculatePos(buffPos[i]);
        }
    }

    public void AddBuff(Buff buff) {
        if (buffColumn.Contains(buff)) {
            return;
        }
        buffColumn.Add(buff);
        ResetBuffPos();
    }

    public void RemoveBuff(Buff buff) {
        buffColumn.Remove(buff);
        ResetBuffPos();
    }
}
