﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkinManager : Singleton<SkinManager> {

    public Image skin;
    public Image SkinBg;
    public Sprite UnEquipSprite;
    public Sprite EquipSprite;
    public bool isEquipSkin = false;

	// Use this for initialization
	void Start () {
        skin.sprite = GameManager.instance.LoadPrefab(GameController.instance.BossAndSkinID.ToString()).GetComponent<SpriteRenderer>().sprite;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void UpdataSkin() {
        skin.sprite = GameManager.instance.LoadPrefab(GameController.instance.BossAndSkinID.ToString()).GetComponent<SpriteRenderer>().sprite;
        isEquipSkin = false;
        SkinBg.sprite = UnEquipSprite;
    }

    public void EquipSkin() {
        isEquipSkin = true;
        SkinBg.sprite = EquipSprite;
    }
}
