﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTreasureBos : MonoBehaviour {

    private float CanAttackTime = 0f;
    public float CanAttackTimeTemp = 0f;
    private float AutoTime = 5f;
    public float AutoTimeTemp = 0f;

    public GameObject smearing;
    protected EzTweenBase _tween;

    public Vector3[] ends = new Vector3[] { new Vector3(-3.48f,1.46f,0f),
    new Vector3(-3.48f,2.38f,0f),
    new Vector3(-3.48f,3.43f,0f),
    new Vector3(-3.48f,4.48f,0f)};


    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame
    void Update() {
        CanAttackTimeTemp += Time.deltaTime;
        if (CanAttackTimeTemp > CanAttackTime) {
            //CustomRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            Collider2D[] cols = Physics2D.OverlapCapsuleAll(Camera.main.ScreenToWorldPoint(Input.mousePosition), new Vector2(1, 1), CapsuleDirection2D.Horizontal, 90);
            if (cols.Length > 0) {
                foreach (Collider2D col in cols) {
                    if (col != null && col.gameObject == this.gameObject) {
                        PickupCoin();
                        Panel.Open("BoxPanel");
                    }
                }
            }
        }
        //AutoPickup();
    }
    public Transform cube;
    public Transform optionalLookTarget;
    private void PickupCoin() {
        if (!GameController.instance.isGameOver) {
            GetComponent<Rigidbody2D>().isKinematic = true;
            //smearing.SetActive(true);
            //tm.PlayNow();
            Vector3[] vectors;
            if (transform.position.x >= 0) {
                vectors = new Vector3[] { transform.position,
                new Vector3(1.5f+Random.Range(0.1f,0.6f), 2, 0),
                new Vector3(2.5f+Random.Range(0.1f,0.6f), 4, 0),
                new Vector3(-0.5f, 5.3f,0) };
            } else {
                vectors = new Vector3[] { transform.position,
                new Vector3(-2.5f+Random.Range(-0.1f,-0.6f), 2, 0),
                new Vector3(-1.5f+Random.Range(-0.1f,-0.6f), 4, 0),
                new Vector3(-0.5f, 5.3f,0) };
            }

            var path = new EzSpline(vectors);
            _tween = EzTweenManager.To(transform, 1.0f, new EzTweenConfig()
          .PositionPath(path, false).SetEaseType(EzEaseType.CubicIn));

            EzTiming.CallDelayed(1.1f, () => {
                TreasureBoxSpawner.instance.Recycle(this);
            });
        } 
    }

    private void AutoPickup() {
        AutoTimeTemp += Time.deltaTime;
        if (AutoTimeTemp >= AutoTime) {
            //PickupCoin();
        }
    }
}
