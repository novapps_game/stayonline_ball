﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateCoinBoss : EzPool<GameCoin, CreateCoinBoss> {

    public Transform target;

    public float explodeDelay = 0f;
    public float explodeTime = 0.5f;
    public float moveDelay = 0.5f;
    public float moveInterval = 0.01f;
    public float moveTime = 1f;
    public float scaleTime = 0.5f;
    public float minScale = 1f;
    public float maxScale = 2f;
    public float endScale = 0.8f;
    public float spawnInterval = 0f;
    public float minSpawnRadius = 10f;
    public float maxSpawnRadius = 150f;

    private int effectCount;

    public void Gain(int coins, int coinsPerEffect, string reason = "RewardedVideo",CoinType coinType = CoinType.DogFace) {
        StartCoroutine(SpawnLoop(coins, coinsPerEffect, reason, coinType));

    }

    IEnumerator SpawnLoop(int coins, int coinsPerEffect, string reason, CoinType coinType = CoinType.DogFace) {
        effectCount = 0;
        int effects = coins / coinsPerEffect;
        while (effectCount++ < effects) {
            if (spawnInterval > 0) {
                yield return new WaitForSeconds(spawnInterval);
            }
            SpawnEffect(coinsPerEffect, reason, coinType);
        }
        int leftCoins = coins % coinsPerEffect;
        if (leftCoins > 0) {
            if (spawnInterval > 0) {
                yield return new WaitForSeconds(spawnInterval);
            }
            SpawnEffect(leftCoins, reason,coinType);
        }
    }

    void SpawnEffect(int coins, string reason,CoinType coinType = CoinType.DogFace) {
        //CoinEffect effect = Create();
        GameCoin gameCoin = Create();
        gameCoin.coinType = coinType;
        Vector3 p =  gameCoin.gameObject.transform.localPosition;
        gameCoin.gameObject.transform.localPosition = p.NewXY(p.x + Random.Range(-1.5f, 1.5f), p.y + Random.Range(-1.5f, 1.5f));
        gameCoin.GetComponent<Rigidbody2D>().AddExplosionForce(200,new Vector3(0,-10,0),200);
        //gameCoin.transform.position = transform.position;
     
    }
    public override void Recycle(GameCoin obj) {
        base.Recycle(obj);
        obj.GetComponent<Rigidbody2D>().isKinematic = false;
        obj.CanAttackTimeTemp = 0f;
        obj.isAttack = false;
        obj.transform.localPosition = Vector3.zero;
        obj.AutoTimeTemp = 0f;
    }
}
