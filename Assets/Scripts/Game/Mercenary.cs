﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MercenaryData {
    public int id;
    public string name;
    public Sprite icon;
    //public int[] skillIds;
    public int maxLevel;
    public IntPersistentProperty skillId;
    //public int emoji1;
    //public int emoji2;
    //public int emoji3;
    //public int emoji1Num;
    //public int emoji2Num;
    //public int emoji3Num;
    public FloatPersistentProperty attackInterval;
    public IntPersistentProperty Level;
    public BoolPersistentProperty IsOwn;
    public int attack;
    public int upLevelSkillCardNum;
    public int upLevelAttackCoinNum;

    public IntPersistentProperty cardNum;

    public IntPersistentProperty upLevelAttackLevel;
    public IntPersistentProperty upLevelSkillLevel;

    public string info;

    public MercenaryData(int id, string name, string info) {
        this.id = id;
        Level = new IntPersistentProperty("MercenaryLevel" + id, 1);
        this.name = name;
        icon = GameManager.instance.LoadSprite("mercenary/mercenary"+id);
        IsOwn = new BoolPersistentProperty("MercenaryIsOwn" + id, false);
        this.info = info;
    }

    public long GetPrice() {
        long price = (long)(360*Mathf.Pow(Level.Value,1.01f)+15);
        
        return price;
    }

    public long GetAttack() {
        return 50 * Level.Value;
    }

    public bool AddAttackLevel() {
        long price = GetPrice();
        if(GameManager.instance.coins >= price) {
            GameManager.instance.CostCoins(price);
            Level.Value++;
            return true;
        }
        return false;

    }

    //可升级的卡片拥有数
    public int GetLevelUpCardNum() {
        int levelUpCardNum = 0;
        //if(GetEmoji1Num() > 0) {
        //    levelUpCardNum++;
        //}
        //if (GetEmoji2Num() > 0) {
        //    levelUpCardNum++;
        //}
        //if (GetEmoji3Num() > 0) {
        //    levelUpCardNum++;
        //}
        return levelUpCardNum;
    }
}

public class SkillData {
    public int skillId;
    public string name;
    public float effectNum;
    public float effectDuration;
    public float UpLevelprice;
    public IntPersistentProperty level;
    public Sprite icon;


    public SkillData(int skillId, string name) {
        this.skillId = skillId;
        level = new IntPersistentProperty("SkillLevel" + skillId, 1);
        this.name = name;
        icon = GameManager.instance.LoadSprite("skill/" + (skillId+1));
    }

    public long GetPrice() {
        long price = (long)(Mathf.Pow(level.Value, 1.1f));
        switch (skillId) {
            case 0:
                price = GameManager.instance.GetUpLevel1SkillPrice();
                break;
            case 1:
                price = GameManager.instance.GetUpLevel2SkillPrice();
                break;
            case 2:
                price = GameManager.instance.GetUpLevel3SkillPrice();
                break;
            case 3:
                price = GameManager.instance.GetUpLevel4SkillPrice();
                break;
            case 4:
                price = GameManager.instance.GetUpLevel5SkillPrice();
                break;
            case 5:
                price = GameManager.instance.GetUpLevel6SkillPrice();
                break;
        }
        
        return price;
    }

    public bool AddLevel() {
        long price = (long)(Mathf.Pow(level.Value, 1.1f));
        switch (skillId) {
            case 0:
                return GameManager.instance.AddSkill1Level();
            case 1:
                return GameManager.instance.AddSkill2Level();
            case 2:
                return GameManager.instance.AddSkill3Level();
            case 3:
                return GameManager.instance.AddSkill4Level();
            case 4:
                return GameManager.instance.AddSkill5Level();
            case 5:
                return GameManager.instance.AddSkill6Level();
        }

        return false;
    }

    public long GetAttack() {
        return 50 * level.Value;
    }

}


public class Mercenary : MonoBehaviour {

    public float attack;
    public MercenaryData mercenaryData;
    public float attackInterval;

    public int skillId;

    public Image SkillCD;

    public SkillData skillData;

    public MercenaryBallSpawner mercenaryBallSpawner;

    public GameObject fire;
    private float attackBase = 50;


    private void Awake() {
        GameController.instance.isShowFir.AddObserver((isShow) => {
            fire.SetActive(isShow);
        });
    }

    void Start () {
        //SkillCD = transform.Find("CD").gameObject.GetComponent<Image>();
        //SkillCD.enabled = false;
      
    }

  
    public void Init(MercenaryData mercenaryData, SkillData skillData) {
        this.mercenaryData = mercenaryData;
        this.skillData = skillData;
        this.skillId = skillData.skillId;
        attack = mercenaryData.Level * attackBase;
        mercenaryBallSpawner = transform.parent.Find("BallSpawner").GetComponent<MercenaryBallSpawner>();
        AttackAction();
     
        if (skillData.skillId == 3) {
            AddBallAttack();
            GameController.instance.isShowFir.Value = true;
        }
        if (skillData.skillId == 4) {
            AddMercenAttack();
       
        }
        if (skillData.skillId == 5) {
            AddBoxCoin();
        }
        GetComponent<Button>().onClick.RemoveAllListeners();
        GetComponent<Button>().onClick.AddListener(() => {
            switch (skillId) {
                case 0:
                    BarrageAttack();
                    break;
                case 1:
                    TimeFreeze();
                    break;
                case 2:
                    CirtEmoji();
                    break;
            }
        });


    }


	// Update is called once per frame
	void Update () {
        if(!SkillIsCD) {
            SkillCD.enabled = true;
            SkillCD.fillAmount += Time.deltaTime / 15f;
            if(SkillCD.fillAmount >= 1) {
                SkillIsCD = true;
                SkillCD.fillAmount = 1;
            }
        }
       
    }

    bool SkillIsCD = true;

    private void AttackAction() {
        mercenaryBallSpawner.Create((mercenaryData), mercenaryData.id);
    }

    public void BarrageAttack() {
        if (SkillIsCD) {
            SkillIsCD = false;
            SkillCD.fillAmount = 0;
            GameController.instance.ShowBarrages();
            EzTiming.CallAtTimes(50, 0.1f, () => {
                Boss.instance.BeAttack((Mathf.Pow(skillData.level,2)*10 +85f)/10f);
            });
        }
    }

    public void TimeFreeze() {
        if (SkillIsCD && GameManager.instance.CheckIsBoss()) {
            SkillIsCD = false;
            SkillCD.fillAmount = 0;
            GameController.instance.PauseCountDown(5f + 0.5f* skillData.level);
        }
    }   

    public void CirtEmoji() {
        if (SkillIsCD) {
            SkillIsCD = false;
            SkillCD.fillAmount = 0;
            GameManager.instance.criticalStrike += 100;
            EzTiming.CallDelayed(5f + 0.5f * skillData.level, () => {
                GameManager.instance.criticalStrike -= 100;
            });
        }
    }

    public void AddBallAttack() {
        GameManager.instance.ballAttackPower = 1f+(skillData.level * 0.5f);
    }

    public void AddMercenAttack() {
        GameManager.instance.mercenaryAttackPower = 1f + (skillData.level * 0.5f);
    }
    public void AddBoxCoin() {
        GameManager.instance.boxCoinPower = 1f + (skillData.level * 0.8f);
    }







}

