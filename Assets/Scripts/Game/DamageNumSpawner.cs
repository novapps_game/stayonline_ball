﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageNumSpawner  : EzPool<DamageNum, DamageNumSpawner> {

    public override void Recycle(DamageNum comp) {
        base.Recycle(comp);
        comp.transform.GetComponent<Text>().color = new Color(1, 1, 1, 1);
    }


}
