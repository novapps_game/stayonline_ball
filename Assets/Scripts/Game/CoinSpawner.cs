﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinSpawner : EzSpawner<GameCoin, CoinSpawner> {
    public override GameCoin Create() {
        //GameController.instance.TotalBallNum++;
        if (!GameController.instance.isGameOver) {
            GameCoin gameCoin = base.Create();
            gameCoin.gameObject.transform.localPosition = Vector3.zero;
            //EzTiming.CallDelayed(0.3f, () => {
            //    gameCoin.gameObject.transform.SetParent(transform.parent, true);
            //});
            return gameCoin;
        }
        return null;

    }

    protected override void Update() {
        base.Update();
    }

    public override void Recycle(GameCoin obj) {
        base.Recycle(obj);
        obj.GetComponent<Rigidbody2D>().isKinematic = false;
        obj.CanAttackTimeTemp = 0f;
        obj.isAttack = false;
        obj.transform.localPosition = Vector3.zero;
        obj.AutoTimeTemp = 0f;
    }
}
