﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum ADTreasureType {
    CoinBox = 0,
    AdsCoinBox,
    CritBox,
    AdsCritBox,
    ExtraCoinBox,
    AdsExtraCoinBox

}


public class ADTreasureBox : MonoBehaviour {

    private ADTreasureType aDTreasureType;
    private TweenMove tm;



    private int adTreasure;
    private int adTyupe;


	void Start () {
		
	}

	
	// Update is called once per frame
	void Update () {
		
	}

    public void OpenBox() {
        switch (aDTreasureType) {
            case ADTreasureType.AdsCoinBox:
                CoinSpawner.instance.Create();
                break;
            case ADTreasureType.AdsCritBox:
                break;
            case ADTreasureType.AdsExtraCoinBox:
                break;
            case ADTreasureType.CoinBox:
                break;
            case ADTreasureType.CritBox:
                break;
            case ADTreasureType.ExtraCoinBox:
                break;
            default:
                break;
        }
    }
}
