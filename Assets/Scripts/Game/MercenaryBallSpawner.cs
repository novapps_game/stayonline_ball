﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MercenaryBallSpawner : EzSpawner<MercenaryBall, MercenaryBallSpawner> {
    long damage;
    int id;
    private float attackBase = 50;
    private MercenaryData mercenaryData;
    public override MercenaryBall Create() {
        if (GameController.instance.isGameOver) {
            return null;
        }
        MercenaryBall mercenaryBall = base.Create();
        this.damage = (long)(mercenaryData.Level.Value * attackBase * GameManager.instance.mercenaryAttackPower);
        if (mercenaryBall != null) {
            mercenaryBall.ballspawner = this;
            mercenaryBall.Init(damage,id);
        }
        return mercenaryBall;
    }

    public void Create(MercenaryData mercenaryData,int id) {
        this.mercenaryData = mercenaryData;
        this.damage = (long)(mercenaryData.Level.Value * attackBase * GameManager.instance.mercenaryAttackPower);
        this.id = id;
    }


    public override void Recycle(MercenaryBall obj) {
        base.Recycle(obj);
        obj.goneEffect.SetActive(false);
        obj.transform.localPosition = Vector3.zero;
        obj.goneEffect.transform.localPosition = new Vector3(0, 0, 0);
    }
}
