﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CoinType {
    DogFace,
    Box
}

public class GameCoin : MonoBehaviour {
    public TweenMove tm;

    public bool isAttack = false;

    public int price = 1;
    private float CanAttackTime = 0f;
    public float CanAttackTimeTemp = 0f;
    private float AutoTime = 5f;
    public float AutoTimeTemp = 0f;

    public GameObject smearing;


    protected EzTweenBase _tween;
    public CoinType coinType;


    // Use this for initialization
    void Start () {
		
	}

    private void OnEnable() {
        gameObject.transform.localPosition = Vector3.zero;
        price = GameManager.instance.GetCoinPrice();
        if(coinType == CoinType.Box) {
            price = (int)(GameManager.instance.boxCoinPower * price);
        }
        smearing.SetActive(false);
    }

    // Update is called once per frame
    void Update () {
        CanAttackTimeTemp += Time.deltaTime;
        if (CanAttackTimeTemp > CanAttackTime) {    
            //CustomRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            Collider2D[] cols = Physics2D.OverlapCapsuleAll(Camera.main.ScreenToWorldPoint(Input.mousePosition), new Vector2(1, 1), CapsuleDirection2D.Horizontal, 90);
            if (cols.Length > 0) {
                foreach (Collider2D col in cols) {
                    if (col != null && col.gameObject == this.gameObject && !isAttack) {
                        PickupCoin();
                    }
                }
            }
        }
        AutoPickup();
    }

    public Transform cube;
    public Transform optionalLookTarget;

    private void PickupCoin() {
        if (!isAttack && !GameController.instance.isGameOver) {
            GetComponent<Rigidbody2D>().isKinematic = true;
            CoinNum coinNum = CoinNumSpawner.instance.Create();
            smearing.SetActive(true);
            coinNum.GetComponent<RectTransform>().anchoredPosition = Camera.main.WorldToScreenPoint(transform.position) * (1f/ GameController.instance.canvas.scaleFactor);
            isAttack = true;
            //tm.PlayNow();
            Vector3[] vectors;
            if (transform.position.x >= 0) {
                vectors = new Vector3[] { transform.position,
                new Vector3(1.5f+Random.Range(0.1f,0.6f), 2, 0),
                new Vector3(2.5f+Random.Range(0.1f,0.6f), 4, 0),
                new Vector3(-0.5f, 5.3f,0) };
            } else {
                vectors = new Vector3[] { transform.position,
                new Vector3(-2.5f+Random.Range(-0.1f,-0.6f), 2, 0),
                new Vector3(-1.5f+Random.Range(-0.1f,-0.6f), 4, 0),
                new Vector3(-0.5f, 5.3f,0) };
            }
            
            var path = new EzSpline(vectors);
            _tween = EzTweenManager.To(transform, 1.0f, new EzTweenConfig()
          .PositionPath(path, false).SetEaseType(EzEaseType.CubicIn));
            GameManager.instance.PlaySound("coin");
            EzTiming.CallDelayed(1.1f, () => {
                GameController.instance.ShowGetCoin();
                GameManager.instance.GainCoins(price);
                CreateCoinBoss.instance.Recycle(this);
        });
    }
    }

    private void AutoPickup() {
        AutoTimeTemp += Time.deltaTime;
        if(AutoTimeTemp >= AutoTime) {
            PickupCoin();
        }
    }

    public void Explode(Vector2 position, Vector3 scale, float time, float delay) {
   
        iTween.MoveUITo(gameObject, iTween.Hash("position", position, "time", time,
            "delay", delay, "easetype", iTween.EaseType.easeOutCirc));
        iTween.ScaleTo(gameObject, iTween.Hash("scale", scale, "time", time,
            "delay", delay));
    }

}
