﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class LevelUpManager : Singleton<LevelUpManager> {
    public Text AttackCoinNum;
    public Text AttackNum;
    public Image AttackBtn;
    public Material gray;

    public Text BossExralCoinNum;
    public Text BossExralCoiNum;
    public Image BossExralCoiBtn;


    public Text BossExralAttackCoinNum;
    public Text BossExralAttackNum;
    public Image BossExralAttackBtn;

    public Text OfflineCoinNum;
    public Text OfflineNum;
    public Image OfflineBtn;



    public GameObject AttackCanUp;
    public GameObject OfflineCanUp;
    public GameObject BossExralCoinUp;
    public GameObject BossExralAttackUp;

    public GameObject AttackLevelUp;
    public GameObject OfflineLevelUp;
    public GameObject BossExralCoinLevelUp;
    public GameObject BossExralAttackLevelUp;

    public GameObject AttackLevelMax;
    public GameObject OfflineLevelMax;
    public GameObject BossExralCoinLevelMax;
    public GameObject BossExralAttackLevelMax;



    public GameObject canLevelUpHint1;
    public GameObject canLevelUpHint2;

    public TweenShake[] tss;

    //public Sprite CanClick;
    //public Sprite UnClick;
    // Use this for initialization
    void Start () {
        Init();
	}
	
	// Update is called once per frame
	void Update () {
        Check();
    }
    public void UpAttack() {
        if (GameManager.instance.AddBattleAttackLevel()) {
            AttackLevelUp.SetActive(true);
            GameManager.instance.PlaySound("levelup");
        }
        Init();
        EzAnalytics.LogEvent("skill", "Upgrade", "Damage");
    }

    public void UpBossExralCoin() {
        if (GameManager.instance.AddBossExtraCoinLevel()) {
            BossExralCoinLevelUp.SetActive(true);
            GameManager.instance.PlaySound("levelup");
        }
        Init();
        EzAnalytics.LogEvent("skill", "Upgrade", "BossRevenue");

    }

    public void UpBossExralAttack() {
        if (GameManager.instance.AddBossExtraAttackLevel()) {
            BossExralAttackLevelUp.SetActive(true);
            GameManager.instance.PlaySound("levelup");
        }
        Init();
        EzAnalytics.LogEvent("skill", "Upgrade", "BossDamage");
    }

    public void UpOfflineEmoji() {
        if (GameManager.instance.AddOfflineLevel()) {
            OfflineLevelUp.SetActive(true);
            GameManager.instance.PlaySound("levelup");  
        }
        Init();
        EzAnalytics.LogEvent("skill", "Upgrade", "OfflineRevenue");
    }


    long attackLevelCoin;
    long bossExralCoinPrice;
    long offlineLevelCoin;
    long bossExralAttackCoin;

    private void Init() {
        attackLevelCoin = GameManager.instance.GetAttackUpPrice();
        AttackCoinNum.text = ((long)attackLevelCoin).ToString();
        AttackCoinNum.GetComponent<BigNumberText>().SetValue(attackLevelCoin);
        AttackNum.text =  (GameManager.instance.GetBallAttack() + 1).ToString();

        bossExralCoinPrice = GameManager.instance.GetExtraBossCoinPrice();
        BossExralCoinNum.text = bossExralCoinPrice.ToString();
        BossExralCoinNum.GetComponent<BigNumberText>().SetValue(bossExralCoinPrice);
        BossExralCoiNum.text = GameManager.instance.GetBossExtraCoinLevel().ToString();
        //if (GameManager.instance.GetEmojiCarryLevel() >= GameManager.instance.EmojiCarryMaxLevel) {
        //    BossExralCoinLevelMax.SetActive(true);
        //}

        bossExralAttackCoin = GameManager.instance.GetExtraBossAttackPrice();
        BossExralAttackCoinNum.text = bossExralAttackCoin.ToString();
        BossExralAttackCoinNum.GetComponent<BigNumberText>().SetValue(bossExralAttackCoin);
    
        BossExralAttackNum.text = GameManager.instance.GetBossExtraAttackLevel().ToString();

        //if (GameManager.instance.GetCritLevel() >= GameManager.instance.CritMaxLevel) {
        //    BossExralAttackLevelMax.SetActive(true);
        //}


        offlineLevelCoin = GameManager.instance.GetOfflineUpPrice();
        OfflineCoinNum.text = offlineLevelCoin.ToString();
        OfflineCoinNum.GetComponent<BigNumberText>().SetValue(offlineLevelCoin);
        OfflineNum.text = ((long)(GameManager.instance.GetMonsterNum() * 6 *(1+GameManager.instance.GetOfflineLevel() * 0.005f))).ToString() + "/m";

        if (GameManager.instance.GetOfflineLevel() >= GameManager.instance.OfflineMaxLevel) {
            OfflineLevelMax.SetActive(true);
            if (Localization.GetLanguage() == "CN") {
                OfflineNum.text = "满级";
            } else if (Localization.GetLanguage() == "CT") {
                OfflineNum.text = "滿級";
            } else {
                OfflineNum.text = "lv.max";
            }
             
        }
        Check();
    }

    private void Check() {
        if (GameManager.instance.coins < attackLevelCoin) {
            AttackBtn.GetComponent<Image>().material = gray;
            AttackBtn.transform.Find("Icon").GetComponent<Image>().material = gray;
            AttackBtn.transform.parent.Find("Icon").GetComponent<Image>().material = gray;
            AttackCanUp.SetActive(false);
        } else {
            AttackBtn.GetComponent<Image>().material = null;
            AttackBtn.transform.Find("Icon").GetComponent<Image>().material = null;
            AttackBtn.transform.parent.Find("Icon").GetComponent<Image>().material = null;
            AttackCanUp.SetActive(true);
        }
        if(GameManager.instance.coins < offlineLevelCoin) {
            OfflineBtn.GetComponent<Image>().material = gray;
            OfflineBtn.transform.Find("Icon").GetComponent<Image>().material = gray;
            OfflineBtn.transform.parent.Find("Icon").GetComponent<Image>().material = gray;
            OfflineCanUp.SetActive(false);
        } else {
            OfflineBtn.GetComponent<Image>().material = null;
            OfflineBtn.transform.Find("Icon").GetComponent<Image>().material = null;
            OfflineBtn.transform.parent.Find("Icon").GetComponent<Image>().material = null;
            OfflineCanUp.SetActive(true);
        }
        if (GameManager.instance.coins < bossExralAttackCoin) {
            BossExralAttackBtn.GetComponent<Image>().material = gray;
            BossExralAttackBtn.transform.Find("Icon").GetComponent<Image>().material = gray;
            BossExralAttackBtn.transform.parent.Find("Icon").GetComponent<Image>().material = gray;
            BossExralAttackUp.SetActive(false);
        } else {
            BossExralAttackBtn.GetComponent<Image>().material = null;
            BossExralAttackBtn.transform.Find("Icon").GetComponent<Image>().material = null;
            BossExralAttackBtn.transform.parent.Find("Icon").GetComponent<Image>().material = null;
            BossExralAttackUp.SetActive(true);
        }
        if (GameManager.instance.coins < bossExralCoinPrice) {
            BossExralCoiBtn.GetComponent<Image>().material = gray;
            BossExralCoiBtn.transform.Find("Icon").GetComponent<Image>().material = gray;
            BossExralCoiBtn.transform.parent.Find("Icon").GetComponent<Image>().material = gray;
            BossExralCoinUp.SetActive(false);
        } else {
            BossExralCoiBtn.GetComponent<Image>().material = null;
            BossExralCoiBtn.transform.Find("Icon").GetComponent<Image>().material = null;
            BossExralCoiBtn.transform.parent.Find("Icon").GetComponent<Image>().material = null;
            BossExralCoinUp.SetActive(true);
        }
    }


    public void UpLevel() {
        GameManager.instance.AddBattleAttackLevel();
        Init();
    }

    public void ShakeAnima() {
        foreach(TweenShake ts in tss) {
            ts.Play();
        }
    }

    public void ShowCanLevelUpHint() {
        canLevelUpHint1.SetActive(true);
        canLevelUpHint2.SetActive(true);
        canLevelUpHint1.transform.DOScale(1.5f, 1f);
        canLevelUpHint1.GetComponent<Image>().DOFade(0, 1f);
        EzTiming.CallDelayed(0.5f, () => {
            canLevelUpHint2.transform.DOScale(1.5f, 1f);
            canLevelUpHint2.GetComponent<Image>().DOFade(0, 1f); 
        });
        EzTiming.CallDelayed(2f, () => {
            canLevelUpHint1.SetActive(false);
            canLevelUpHint2.SetActive(false);
            canLevelUpHint1.GetComponent<Image>().color = Color.white;
            canLevelUpHint2.GetComponent<Image>().color = Color.white;
            canLevelUpHint1.transform.localScale = new Vector3(1, 1, 1);
            canLevelUpHint2.transform.localScale = new Vector3(1, 1, 1);

        });

    }

}
