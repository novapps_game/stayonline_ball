﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Barrage : MonoBehaviour {

    public SpriteRenderer sprite;
	// Use this for initialization
	void Start () {
        sprite = GetComponent<SpriteRenderer>();
        sprite.sprite = GameManager.instance.LoadSprite("skill/danmu" + Random.Range(1,12));
    }
	
	// Update is called once per frame
	void Update () {
        AutoPickup();
    }

    protected EzTweenBase _tween;
    public bool isAttack = false;
    public GameObject smearing;
    private float AutoTime = 0.8f;
    public float AutoTimeTemp = 0f;

    private void PickupCoin() {
        if (!isAttack && !GameController.instance.isGameOver) {
            //GetComponent<Rigidbody2D>().isKinematic = true;
            //CoinNum coinNum = CoinNumSpawner.instance.Create();
            smearing.SetActive(true);
            //coinNum.GetComponent<RectTransform>().anchoredPosition = Camera.main.WorldToScreenPoint(transform.position) * (1f / GameController.instance.canvas.scaleFactor);
            isAttack = true;
            //tm.PlayNow();
            //Vector3[] vectors;
            //  if (transform.position.x >= 0) {
            //      vectors = new Vector3[] { transform.position,
            //      new Vector3(1.5f+Random.Range(0.1f,0.6f), 2, 0),
            //      new Vector3(2.5f+Random.Range(0.1f,0.6f), 4, 0),
            //      new Vector3(-0.5f, 4.3f,0) };
            //  } else {
            //      vectors = new Vector3[] { transform.position,
            //      new Vector3(-2.5f+Random.Range(-0.1f,-0.6f), 2, 0),
            //      new Vector3(-1.5f+Random.Range(-0.1f,-0.6f), 4, 0),
            //      new Vector3(-0.5f, 4.3f,0) };
            //  }

            //  var path = new EzSpline(vectors);
            //  _tween = EzTweenManager.To(transform, 1.0f, new EzTweenConfig()
            //.PositionPath(path, false).SetEaseType(EzEaseType.CubicIn));
            //GameManager.instance.PlaySound("coin");

            transform.DOLocalMoveX(-5, 0.3f);

            EzTiming.CallDelayed(0.35f, () => {
                //GameController.instance.ShowGetCoin();
                BarrageSpawner.instance.Recycle(this);
                //DestroyImmediate(this.gameObject);
            });
        }
    }



    private void AutoPickup() {
        AutoTimeTemp += Time.deltaTime;
        if (AutoTimeTemp >= AutoTime) {
            PickupCoin();
        }
    }
}
