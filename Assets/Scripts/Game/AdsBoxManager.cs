﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdsBoxManager : MonoBehaviour {

    public List<ADTreasureBox> adTreasureBoxs;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public void ChangeBoxPos() {
        if(adTreasureBoxs.Count > 0) {

            foreach(ADTreasureBox aDTreasureBox in adTreasureBoxs) {
                if(aDTreasureBox == null) {
                    adTreasureBoxs.RemoveAt(adTreasureBoxs.Count - 1);
                }
            }   
        }
    }
}
