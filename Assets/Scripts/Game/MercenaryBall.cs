﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MercenaryBall : MonoBehaviour {
    public TweenMove tm;
    public SpriteRenderer image;
    public GameObject goneEffect;
    public MercenaryBallSpawner ballspawner;
    // Use this for initialization
    void Start () {
    
    }

    public void Init(long damage,int id) {
        tm = GetComponent<TweenMove>();
        image.sprite = GameManager.instance.LoadSprite("mercenary/Attack"+id);
        transform.localEulerAngles = new Vector3(0, 0, ChangeAngle(id));
        transform.localPosition = transform.localPosition.NewZ(10f);
        tm.PlayNow();
        EzTiming.CallDelayed(1.62f, () => {

        goneEffect.transform.localPosition = goneEffect.transform.localPosition.NewXY(goneEffect.transform.localPosition.x + Random.Range(-0.5f, 0.5f),
                    goneEffect.transform.localPosition.y + Random.Range(-0.5f, 0.5f));
            //goneEffect.transform.localScale = goneEffect.transform.localScale.NewXY(3.33f * Random.Range(0.6f, 1f),
            //    3.33f * Random.Range(0.6f, 1f));
         
            goneEffect.SetActive(true);
            DamageNum damageNum = DamageNumSpawner.instance.Create();
            damageNum.ShowNomalDamage(damage);
            damageNum.GetComponent<RectTransform>().anchoredPosition = Camera.main.WorldToScreenPoint(transform.position) * (1f / GameController.instance.canvas.scaleFactor);
            Boss.instance.BeAttack(damage);
        });
        EzTiming.CallDelayed(1.75f, () => {
            if (ballspawner != null) {
                ballspawner.Recycle(this);
            } else {
                MercenaryBallSpawner.instance.Recycle(this);
            }
        });
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private float ChangeAngle(int id) {
        switch (id) {
            case 0:
                return -22;
            case 1:
                return -12;
            case 2:
                return -6;
            case 3:
                return 6;
            case 4:
                return 12;
            case 5:
                return 22;
        }
        return 0;
    }
}
