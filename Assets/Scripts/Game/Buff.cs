﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public enum BuffType {
    AddCrit = 0,
    AddCoin
}
[System.Serializable]
public class BuffData {
    public float duration;

    public float effect;

    public BuffType buffType;
}


public class Buff : MonoBehaviour {


    private float durationTemp = 0;

    public BuffData buffData;

    private RectTransform rtf;

    public Image CD;
    private CoroutineHandle ch1;
    private CoroutineHandle ch2;

    public void StartBuff() {
        CD.fillAmount = 1;
        if (ch1 != null) {
            EzTiming.KillCoroutines(ch1);
        }
        if (ch2 != null) {
            EzTiming.KillCoroutines(ch2);
        }

        ch1 = EzTiming.CallDelayed(durationTemp, () => {
            FinishBuff();
        });
        ch2 = EzTiming.CallAtTimes((int)(durationTemp *100), 0.01f, () => {
            CD.fillAmount -= 1f / (durationTemp * 100f);
        });
        switch (buffData.buffType) {
            case BuffType.AddCoin:
                EzAnalytics.LogEvent("chest", "Bonus", "AdsCollect");
                break;
            case BuffType.AddCrit:
                GameManager.instance.criticalStrike += 30;
                EzAnalytics.LogEvent("chest", "Crits", "Collect");
                break;
        }
    }


    public void InitBuff(BuffData buffData) {
        this.buffData = buffData;
        //if (CD != null) {
        //    CD.fillAmount = 1;
        //}
    }

    public void FinishBuff() {
        switch (buffData.buffType) {
            case BuffType.AddCoin:
                break;
            case BuffType.AddCrit:
                GameManager.instance.criticalStrike -= 30;
                break;
        }
        CD.fillAmount = 1;
        BuffManager.instance.RemoveBuff(this);
        gameObject.SetActive(false);
    }


	// Use this for initialization
	void Start () {
       
    }

    private void OnEnable() {
        CD = GetComponent<Image>();
        CD.fillAmount = 1;
        rtf = GetComponent<RectTransform>();
        durationTemp += buffData.duration;
        BuffManager.instance.AddBuff(this);
        StartBuff();
    }

    public void InitDiration() {
        CD.fillAmount = 1;
    }

    public void CalculatePos(Vector2 pos) {
        rtf.anchoredPosition = pos;
    }

    // Update is called once per frame
    void Update () {
		
	}
}
