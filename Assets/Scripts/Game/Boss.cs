﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Boss : Singleton<Boss> {

    public float CurrentHp = 20;
    public float MaxHp = 20;

    public Slider HpSlider;

    public SpriteRenderer BossSr;

    public GameObject shouji;

    public GameObject dieEffect;

    public GameObject BossDieEffect;

    public GameObject MonsterBG;
    public GameObject BossBG;

    private BossCameraShake cameraShake;

	// Use this for initialization
	void Start () {
        cameraShake = Camera.main.GetComponent<BossCameraShake>();
        isFailNextBoss = new BoolPersistentProperty("isFailNextBoss", false);
        //NextBoss();
        if (GameManager.instance.CheckIsBoss()) {
            int BossId = GameController.instance.GetNextBossId();
            BossSr.sprite = GameManager.instance.LoadSprite("boss/boss" + BossId.ToString());
            BossSr.transform.localScale = new Vector3(1f, 1f, 1f);
            BeAttackScalLR = new Vector3(0.9f, 1.1f, 1f);
            BeAttackScalUB = new Vector3(1.1f, 0.9f, 1f);
            BeAttackScalNM = new Vector3(1f, 1f, 1f);
         

            BossBG.SetActive(true);
            MonsterBG.SetActive(false);
        } else {
            int BossId = GameController.instance.GetMonsterId();
            BossSr.sprite = GameManager.instance.LoadSprite("emojiMouster/" + BossId.ToString());
            BossSr.transform.localScale = new Vector3(2f/5f, 2f/5f, 2f/5f);
            BeAttackScalLR = new Vector3(1.8f/5f, 2.2f / 5f, 1f);
            BeAttackScalUB = new Vector3(2.2f / 5f, 1.8f /5f, 1f);
            BeAttackScalNM = new Vector3(2.6f / 5f, 2.6f / 5f, 1f);
            BossBG.SetActive(false);
            MonsterBG.SetActive(true);
        }
        SetMonsterHP();
        ts = GetComponent<TweenMove>();
     
        //shouji.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {

	}
    public BoolPersistentProperty isFailNextBoss;
    public int failBossNum  = 0;
    //失败时的下一个BOSS
    public void FailNextBoss() {
        for (int i = 0; i < 6; i++) {
            GameManager.instance.ReduceMonsterNum();
        }
        isFailNextBoss.Value = true;
        NextBoss();
        failBossNum++;
    }

    bool isBeAttack = false;
    Vector3 nextBeAttackPos;
    TweenMove ts;
    bool BeAttackAnimaOver = false;
    Tweener tscale;

    Vector3 BeAttackScalLR = new Vector3(0.9f, 1.1f, 1f);
    Vector3 BeAttackScalUB = new Vector3(1.1f, 0.9f, 1f);
    Vector3 BeAttackScalNM = new Vector3(1f, 1f, 1f);
    public void BeAttack(float attack) {
        Tweener tc = BossSr.DOColor(Color.red, 0.05f);
        tc.OnComplete(() => {
            Tweener tcw = BossSr.DOColor(Color.white, 0.05f);
        });
        if (GameManager.instance.CheckIsBoss()) {
            attack += GameManager.instance.GetBossExtralAttack();
        }
        CurrentHp -= attack;
        HpSlider.value = (CurrentHp / MaxHp);
        if (CurrentHp <= 0) {
            Die();
        }
        if (!BeAttackAnimaOver) {
            if (!isBeAttack) {
                ts.position = new Vector3(Random.Range(0f, 0.3f), Random.Range(0f, 0.3f), 0);
                nextBeAttackPos = ts.position.NewXY(-ts.position.x, -ts.position.y);
            } else {
                ts.position = nextBeAttackPos;
            }
            isBeAttack = !isBeAttack;
            BeAttackAnimaOver = true;
            EzTiming.CallDelayed(0.1f, () => {
                BeAttackAnimaOver = false;
            });
           // ts.PlayNow();
            //TweenParams tp = new TweenParams().SetLoops(-1, LoopType.Yoyo);
        }
        if(tscale != null && !tscale.IsComplete()) {
            tscale.Restart();
        }
        tscale = transform.DOScale(BeAttackScalLR, 0.08f);
        tscale.SetEase<Tweener>(DG.Tweening.Ease.InOutQuad);
        tscale.SetLoops(2, LoopType.Yoyo);
        tscale.OnComplete(() => {
            tscale = transform.DOScale(BeAttackScalUB, 0.08f);
            tscale.SetEase<Tweener>(DG.Tweening.Ease.InOutQuad);
            tscale.SetLoops(2, LoopType.Yoyo);
            tscale.OnComplete(() => {
                transform.DOScale(BeAttackScalNM, 0.08f);
                tscale.SetEase<Tweener>(DG.Tweening.Ease.InOutQuad);
                tscale.SetLoops(2, LoopType.Yoyo);
            });
        });
        //int num = Random.Range(0, 2);
        //TweenShake ts =  GetComponent<TweenShake>();
        //if (num == 0) {
        //    ts.amount = new Vector3(0.3f, 0, 0);
        //} else {
        //    ts.amount = new Vector3(0, 0.3f, 0);
        //}



        //    shouji.SetActive(true);
        //    EzTiming.CallDelayed(0.20f, () => {
        //        shouji.SetActive(false);
        //    });
    }
    public bool isDie = false;
    private void Die() {
        if (!isDie) {
            isDie = true;
            if (GameManager.instance.CheckIsBoss()) {
                GameController.instance.BossDie();
                GameManager.instance.CleanBossFailCount();
                //CameraShake.ShakeFor(2.0f, 0.3f);
                cameraShake.shake = 1.5f;
                cameraShake.Shake();
                GameManager.instance.PlaySound("boss_explo");
                EzAnalytics.LogEvent("boss", "Win", "Collect");
            } else {
                CameraShake.ShakeFor(1.0f, 0.3f);
                GameManager.instance.PlaySound("enemy_explo");
            }
          
            GameManager.instance.GameOver();
        if (GameManager.instance.CheckNextIsBoss() && !isFailNextBoss) {
      
                BossSr.sprite = null;
                //GameController.instance.BossDie();
                BossDieEffect.SetActive(true);
                EzTiming.CallDelayed(1.0f, () => {
                    BossDieEffect.SetActive(false);
                });
                DropOutCoin();
                GameController.instance.ShowBossCome();
                EzTiming.CallDelayed(2f, () => {
                    NextBoss();
                    isDie = false;
                });
    
            } else {
                BossSr.sprite = null;
                dieEffect.SetActive(true);
                    EzTiming.CallDelayed(1.0f, () => {
                        dieEffect.SetActive(false);
                    });
                DropOutCoin();
                EzTiming.CallDelayed(1f, () => {
                    NextBoss();
                    isDie = false;
                });
            }
        }

    }



    private void DropOutCoin() {
        if (GameManager.instance.CheckIsBoss()) {
            Panel.Open("BossRollPanel");
        } else {
            CreateCoinBoss.instance.Gain((int)(GameController.instance.DogFaceDieCoins * GameController.instance.MonsterDieCoinCoefficient), 1);
        }
    }
    Tweener mousterTs;
    public void NextBoss() {
            GameManager.instance.AddMonsterNum();
            GameController.instance.NextBoss();
            if (GameManager.instance.CheckIsBoss()) {
            if (isFailNextBoss) {
                FailNextBoss();
                return;
            }
            int BossId = GameController.instance.GetNextBossId();
            BossSr.sprite = GameManager.instance.LoadSprite("boss/boss" + BossId.ToString());
            BossSr.transform.localScale = new Vector3(0f, 0f, 0f);
            BossSr.color = Color.white;
            mousterTs = BossSr.transform.DOScale(new Vector3(1.3f, 1.3f, 1.3f), 0.5f);
      
            mousterTs.OnComplete(() => {
                BossSr.transform.DOScale(new Vector3(1f, 1f, 1f), 0.5f);
            });
            BeAttackScalLR = new Vector3(0.9f, 1.1f, 1f);
            BeAttackScalUB = new Vector3(1.1f, 0.9f, 1f);
            BeAttackScalNM = new Vector3(1f, 1f, 1f);
            BossBG.SetActive(true);
            MonsterBG.SetActive(false);
        } else {
            int BossId = GameController.instance.GetMonsterId();
            BossSr.sprite = GameManager.instance.LoadSprite("emojiMouster/" + BossId.ToString());
            BossSr.transform.localScale = new Vector3(0f, 0f, 0f);
            mousterTs = BossSr.transform.DOScale(new Vector3(2.9f / 5f, 2.9f / 5f, 2.9f / 5f), 0.5f);
            mousterTs.OnComplete(() => {
                BossSr.transform.DOScale(new Vector3(2.6f / 5f, 2.6f / 5f, 2f / 5f), 0.5f);
            });
          
            BeAttackScalLR = new Vector3(1.8f / 5f, 2.2f / 5f, 1f);
            BeAttackScalUB = new Vector3(2.2f / 5f, 1.8f / 5f, 1f);
            BeAttackScalNM = new Vector3(2.6f / 5f, 2.6f / 5f, 1f);
            BossBG.SetActive(false);
            MonsterBG.SetActive(true);
        }
    
        SetMonsterHP();
        HpSlider.value = (CurrentHp / MaxHp);
         
    }
        
    public void PreMonster() {
        GameManager.instance.ReduceMonsterNum();
    }


    

    private void SetMonsterHP() {
        if (GameManager.instance.CheckIsBoss()) {
            CurrentHp = GameManager.instance.GetBossHP();
            GameController.instance.isBossShow = true;
        } else {
            CurrentHp = GameManager.instance.GetDogFaceHP();
        }
        MaxHp = CurrentHp;
        GameController.instance.BossTime = GameManager.instance.GetBossTime();
        GameController.instance.cdt.seconds = (int)GameController.instance.BossTime;
        GameController.instance.cdt.Launch();
        
  
        //GameController.instance.cdt.countTime = (int)GameController.instance.BossTime;

    }

    private void RandomBossSprite() {
        int BossSpriteNum = Random.Range(1, 31);
        GameManager.instance.LoadSprite("boss"+ BossSpriteNum);
    }


   
}
