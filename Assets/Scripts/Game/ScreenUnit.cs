﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScreenUnit : MonoBehaviour {


    float width = 1080f;

    float height = 1920f;

    // Use this for initialization
    void Start() {
        float orthographicSize = this.GetComponent<Camera>().orthographicSize;
        orthographicSize *= (Screen.height / (float)Screen.width) / (height / width);
        this.GetComponent<Camera>().orthographicSize = orthographicSize;

    }
}
    