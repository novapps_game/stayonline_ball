﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrageSpawner : EzSpawner<Barrage, BarrageSpawner> {
    public override void Recycle(Barrage obj) {
        base.Recycle(obj);
        obj.isAttack = false;
        obj.transform.localPosition = Vector3.zero;
        obj.AutoTimeTemp = 0f;
    }
}
