﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SignItem : MonoBehaviour {

    private GameObject daySign;

    public int signCoinNum = 0;
    public int DayNum = 0;

    private Text addCoinText;

    private GainCoins gainCoins;


    // Use this for initialization
    void Start () {
        daySign = transform.Find("SignSuccess").gameObject;
        daySign.SetActive(false);
        addCoinText = transform.Find("AddCoinNum").GetComponent<Text>();
        addCoinText.text = "+" + signCoinNum;
        gainCoins = transform.parent.parent.Find("GainCoins").GetComponent<GainCoins>();
        gainCoins.target = GameObject.Find("Coin").gameObject.transform;
        if(GameManager.instance.continuousSignCount >= DayNum) {
            IsSign();
        }
        if(GameManager.instance.continuousSignCount + 1 == DayNum) {
            GetComponent<TweenScale>().Play();
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Sign() {
        if (!GameManager.instance.IsSignedToday() && GameManager.instance.continuousSignCount+1 == DayNum) {
            IsSign();
            gainCoins.transform.SetParent(transform.parent.parent.parent, false);
            gainCoins.Gain(signCoinNum, 100, "Sign");
            GameManager.instance.SignToday();
            transform.parent.parent.GetComponent<SignPanel>().UpdateSignDay();
            GetComponent<TweenScale>().Stop();
            transform.localScale = new Vector3(1, 1, 1);
            Panel.Close("SignPanel");
   
        }
    }

    private void IsSign() {
        daySign.SetActive(true);
        GetComponent<Image>().color = new Color(128f / 255f, 128f / 255f, 128f / 255f, 1f);
    
    }
}
