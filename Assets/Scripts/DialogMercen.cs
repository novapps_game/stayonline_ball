﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogMercen : MonoBehaviour {

    public Image icon;
    public Text info;

    public void Init(Sprite icon,string text) {
        this.icon.sprite = icon;
        this.info.text = text;
    }



}
