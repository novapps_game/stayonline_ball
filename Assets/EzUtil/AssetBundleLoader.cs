﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class AssetBundleLoader {

    public static IEnumerator DownloadAssetBundle(string url, int version, 
        System.Action<AssetBundle, string> onComplete, 
        System.Action<string> onError = null, 
        System.Action<float> onProgress = null) {
        // Wait for the Caching system to be ready
        while (!Caching.ready)
            yield return null;

        // Start the download
        using (WWW www = WWW.LoadFromCacheOrDownload(url, version)) {
            while (!www.isDone) {
                if (onProgress != null) {
                    onProgress.Invoke(www.progress);
                }
                yield return null;
            }
            if (www.error != null) {
                if (onError != null) {
                    onError.Invoke(www.error);
                }
            } else {
                if (onProgress != null) {
                    onProgress.Invoke(1);
                }
                onComplete.Invoke(www.assetBundle, url);
            }
        } // memory is freed from the web stream (www.Dispose() gets called implicitly)
    }

    public static IEnumerator DownloadObjectFromAssetBundle<T>(string url, string name, int version, 
        System.Action<T, string, string> onComplete,
        System.Action<string> onError = null,
        System.Action<float> onProgress = null) where T : Object {
        yield return DownloadAssetBundle(url, version, (assetBundle, assetUrl) => {
            T obj = assetBundle.LoadAsset<T>(name);
            if (obj == null) {
                if (onError != null) {
                    onError.Invoke("Cannot find obj [" + name +"] in asset bundle: " + url);
                }
            } else {
                onComplete.Invoke(obj, assetUrl, name);
            }
        }, onError, onProgress);
    }

}
