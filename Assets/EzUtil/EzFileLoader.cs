﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class EzFileLoader : MonoBehaviour {

    public const string FILE_CACHE = "FileCache";
    public static readonly string fileCacheDir = Application.persistentDataPath + Path.DirectorySeparatorChar + FILE_CACHE;

    public static void Init() {
        if (!Directory.Exists(fileCacheDir)) {
            Directory.CreateDirectory(fileCacheDir);
        }
    }

    public static string GetCacheFilePath(string url) {
        return fileCacheDir + Path.DirectorySeparatorChar + url.GetHashCode() + Path.GetExtension(url);
    }

    public static string GetCachedFile(string url) {
        if (string.IsNullOrEmpty(url)) {
            return null;
        }
        string file = GetCacheFilePath(url);
        if (!File.Exists(file)) {
            return null;
        }
        return file;
    }

    public static bool IsCached(string url) {
        if (string.IsNullOrEmpty(url)) {
            return false;
        }
        string file = GetCacheFilePath(url);
        return File.Exists(file);
    }

    public static IEnumerator DownloadFile(string url,
        System.Action<byte[]> onSuccess,
        System.Action<string> onError = null,
        System.Action<float> onProgress = null) {
        if (!string.IsNullOrEmpty(url)) {
            Debug.Log("downloading file: " + url);
            WWW www = new WWW(url);
            while (!www.isDone) {
                if (onProgress != null) {
                    onProgress.Invoke(www.progress);
                }
                yield return null;
            }
            if (!string.IsNullOrEmpty(www.error)) {
                Debug.LogWarning("download failed: " + url);
                if (onError != null) {
                    onError.Invoke(www.error);
                }
            } else {
                Debug.Log("downloaded file: " + url + " [" + www.bytesDownloaded + "] bytes");
                if (www.bytes != null && www.bytesDownloaded > 0 && onSuccess != null) {
                    onSuccess.Invoke(www.bytes);
                }
            }
        }
    }

    public static IEnumerator DownloadFile(string url, string file,
        System.Action<string> onSuccess = null,
        System.Action<string> onError = null,
        System.Action<float> onProgress = null) {
        yield return DownloadFile(url, (bytes) => {
            Debug.Log("file downloaded: " + url + " ==> " + file);
            try {
                File.WriteAllBytes(file, bytes);
                if (onSuccess != null) {
                    onSuccess.Invoke(file);
                }
            } catch (System.Exception e) {
                if (onError != null) {
                    onError.Invoke(e.ToString());
                }
            }
            
        }, onError, onProgress);
    }

    public static IEnumerator CacheFile(string url,
        System.Action<string> onSuccess = null,
        System.Action<string> onError = null,
        System.Action<float> onProgress = null) {
        if (!string.IsNullOrEmpty(url)) {
            string file = GetCacheFilePath(url);
            if (!File.Exists(file)) {
                yield return DownloadFile(url, file, onSuccess, onError, onProgress);
            } else {
                Debug.Log(url + " file already cached in: " + file);
                if (onProgress != null) {
                    onProgress.Invoke(1);
                }
                if (onSuccess != null) {
                    onSuccess.Invoke(file);
                }
            }
        }
    }

    public static IEnumerator CacheFiles(string[] urls,
        System.Action<int> onComplete = null,
        System.Action<float> onProgress = null) {
        if (urls != null && urls.Length > 0) {
            int cachedCount = 0;
            for (int i = 0; i < urls.Length; ++i) {
                yield return CacheFile(urls[i], (file) => {
                    ++cachedCount;
                    if (onProgress != null) {
                        onProgress.Invoke((i + 1) / urls.Length);
                    }
                }, (error) => {
                }, (progress) => {
                    if (onProgress != null) {
                        onProgress.Invoke((i + progress) / urls.Length);
                    }
                });
            }
            if (onComplete != null) {
                onComplete.Invoke(cachedCount);
            }
        }
    }

    public static IEnumerator LoadFile(string url,
        System.Action<byte[]> onSuccess,
        System.Action<string> onError = null,
        System.Action<float> onProgress = null) {
        string file = GetCacheFilePath(url);
        if (!File.Exists(file)) {
            yield return DownloadFile(url, (bytes) => {
                Debug.Log("cache file to: " + file);
                try {
                    File.WriteAllBytes(file, bytes);
                } catch (System.Exception) {
                }
                if (onProgress != null) {
                    onProgress.Invoke(1);
                }
                if (onSuccess != null) {
                    onSuccess.Invoke(bytes);
                }
            }, onError, onProgress);
        } else {
            try {
                byte[] bytes = File.ReadAllBytes(file);
                if (onProgress != null) {
                    onProgress.Invoke(1);
                }
                if (onSuccess != null) {
                    onSuccess.Invoke(bytes);
                }
            } catch (System.Exception e) {
                if (onError != null) {
                    onError.Invoke(e.ToString());
                }
            }
        }
    }
}
