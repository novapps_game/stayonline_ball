﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class VoxelWorldGenerator : EditorWindow {

    [MenuItem("Window/EzUtil/VoxelWorldGenerator")]
    static void Init() {
        VoxelWorldGenerator window = GetWindow<VoxelWorldGenerator>(false, "VoxelWorldGenerator", true);
        window.Show();
    }

    [System.Serializable]
    public class VoxelInfo {
        public GameObject prefab;
        public int layer;
        public Vector3 size = Vector3.one;
        public Vector3 rotation = Vector3.zero;
        public Vector3 scale = Vector3.one;
    }
    public VoxelInfo[] voxels;
    private VoxelInfo randomVoxel { get { return voxels[Random.Range(0, voxels.Length)]; } }

    public Transform parent;

    public Bounds bounds;

    private class Voxel {
        public Transform transform;
        public VoxelInfo info;
        public Voxel right;
        public Voxel left;
        public Voxel up;
        public Voxel down;
        public Voxel forward;
        public Voxel backward;

        public Voxel(VoxelInfo info, Transform parent, Vector3 position) {
            this.info = info;
            transform = Instantiate(info.prefab).transform;
            transform.SetParent(parent);
            transform.gameObject.name = info.prefab.name;
            transform.gameObject.layer = info.layer;
            transform.localPosition = position;
            transform.localEulerAngles = info.rotation;
            transform.localScale = info.scale;
        }

        public bool NewRight(VoxelInfo newInfo, Transform parent, Bounds bounds) {
            if (right != null) return false;
            Vector3 position = transform.position.OffsetX((info.size.x + newInfo.size.x) * 0.5f);
            Bounds newBounds = new Bounds(position, newInfo.size);
            if (!bounds.Contains(newBounds)) return false;
            right = new Voxel(newInfo, parent, position);
            right.left = this;
            if (up != null && up.right != null) {
                up.right.down = right;
                right.up = up.right;
            }
            if (down != null && down.right != null) {
                down.right.up = right;
                right.down = down.right;
            }
            if (forward != null && forward.right != null) {
                forward.right.backward = right;
                right.forward = forward.right;
            }
            if (backward != null && backward.right != null) {
                backward.right.forward = right;
                right.backward = backward.right;
            }
            return true;
        }

        public bool NewLeft(VoxelInfo newInfo, Transform parent, Bounds bounds) {
            if (left != null) return false;
            Vector3 position = transform.position.OffsetX((info.size.x + newInfo.size.x) * -0.5f);
            Bounds newBounds = new Bounds(position, newInfo.size);
            if (!bounds.Contains(newBounds)) return false;
            left = new Voxel(newInfo, parent, position);
            left.right = this;
            if (up != null && up.left != null) {
                up.left.down = left;
                left.up = up.left;
            }
            if (down != null && down.left != null) {
                down.left.up = left;
                left.down = down.left;
            }
            if (forward != null && forward.left != null) {
                forward.left.backward = left;
                left.forward = forward.left;
            }
            if (backward != null && backward.left != null) {
                backward.left.forward = left;
                left.backward = backward.left;
            }
            return true;
        }

        public bool NewUp(VoxelInfo newInfo, Transform parent, Bounds bounds) {
            if (up != null) return false;
            Vector3 position = transform.position.OffsetY((info.size.y + newInfo.size.y) * 0.5f);
            Bounds newBounds = new Bounds(position, newInfo.size);
            if (!bounds.Contains(newBounds)) return false;
            up = new Voxel(newInfo, parent, position);
            up.down = this;
            if (right != null && right.up != null) {
                right.up.left = up;
                up.right = right.up;
            }
            if (left != null && left.up != null) {
                left.up.right = up;
                up.left = left.up;
            }
            if (forward != null && forward.up != null) {
                forward.up.backward = up;
                up.forward = forward.up;
            }
            if (backward != null && backward.up != null) {
                backward.up.forward = up;
                up.backward = backward.up;
            }
            return true;
        }

        public bool NewDown(VoxelInfo newInfo, Transform parent, Bounds bounds) {
            if (down != null) return false;
            Vector3 position = transform.position.OffsetY((info.size.y + newInfo.size.y) * -0.5f);
            Bounds newBounds = new Bounds(position, newInfo.size);
            if (!bounds.Contains(newBounds)) return false;
            down = new Voxel(newInfo, parent, position);
            down.up = this;
            if (right != null && right.down != null) {
                right.down.left = down;
                down.right = right.down;
            }
            if (left != null && left.down != null) {
                left.down.right = down;
                down.left = left.down;
            }
            if (forward != null && forward.down != null) {
                forward.down.backward = down;
                down.forward = forward.down;
            }
            if (backward != null && backward.down != null) {
                backward.down.forward = down;
                down.backward = backward.down;
            }
            return true;
        }

        public bool NewForward(VoxelInfo newInfo, Transform parent, Bounds bounds) {
            if (forward != null) return false;
            Vector3 position = transform.position.OffsetZ((info.size.z + newInfo.size.z) * 0.5f);
            Bounds newBounds = new Bounds(position, newInfo.size);
            if (!bounds.Contains(newBounds)) return false;
            forward = new Voxel(newInfo, parent, position);
            forward.backward = this;
            if (right != null && right.forward != null) {
                right.forward.left = forward;
                forward.right = right.forward;
            }
            if (left != null && left.forward != null) {
                left.forward.right = forward;
                forward.left = left.forward;
            }
            if (up != null && up.forward != null) {
                up.forward.down = forward;
                forward.up = up.forward;
            }
            if (down != null && down.forward != null) {
                down.forward.up = forward;
                forward.down = down.forward;
            }
            return true;
        }

        public bool NewBackward(VoxelInfo newInfo, Transform parent, Bounds bounds) {
            if (backward != null) return false;
            Vector3 position = transform.position.OffsetZ((info.size.z + newInfo.size.z) * -0.5f);
            Bounds newBounds = new Bounds(position, newInfo.size);
            if (!bounds.Contains(newBounds)) return false;
            backward = new Voxel(newInfo, parent, position);
            backward.forward = this;
            if (right != null && right.backward != null) {
                right.backward.left = backward;
                backward.right = right.backward;
            }
            if (left != null && left.backward != null) {
                left.backward.right = backward;
                backward.left = left.backward;
            }
            if (up != null && up.backward != null) {
                up.backward.down = backward;
                backward.up = up.backward;
            }
            if (down != null && down.backward != null) {
                down.backward.up = backward;
                backward.down = down.backward;
            }
            return true;
        }
    }

    private void OnGUI() {
        ScriptableObject target = this;
        SerializedObject so = new SerializedObject(target);
        EditorGUILayout.PropertyField(so.FindProperty("voxels"), true);
        EditorGUILayout.PropertyField(so.FindProperty("parent"));
        EditorGUILayout.PropertyField(so.FindProperty("bounds"));
        so.ApplyModifiedProperties();
        if (voxels != null && voxels.Length > 0) {
            if (GUILayout.Button("Generate")) {
                Spread(new Voxel(randomVoxel, parent, Vector3.zero));
            }
        }
    }

    void Spread(Voxel center) {
        Voxel current = center;
        while (current != null) {
            if (current.NewRight(randomVoxel, parent, bounds)) {
                current = current.right;
            } else if (current.NewLeft(randomVoxel, parent, bounds)) {
                current = current.left;
            } else if (current.NewUp(randomVoxel, parent, bounds)) {
                current = current.up;
            } else if (current.NewDown(randomVoxel, parent, bounds)) {
                current = current.down;
            } else if (current.NewForward(randomVoxel, parent, bounds)) {
                current = current.forward;
            } else if (current.NewBackward(randomVoxel, parent, bounds)) {
                current = current.backward;
            } else if (current != center) {
                current = center;
            } else {
                current = null;
            }
        }
    }
}
