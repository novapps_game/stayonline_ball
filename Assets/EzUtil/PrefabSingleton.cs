﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabSingleton<T> : MonoBehaviour where T : Component {

    public static T instance {
        get {
            if (_instance == null) {
                _instance = FindObjectOfType<T>();
                if (_instance == null) {
                    _instance = GameManager.instance.Instantiate<T>(typeof(T).Name);
                }
            } 
            return _instance;
        }
    }
    private static T _instance;

    public static T GetInstance() { return instance; }

}
