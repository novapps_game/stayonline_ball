﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public class EzPrefs {

    private const bool safeMode = true;

    // Each encrypted player pref key is given a prefix (this helps the Editor to identify them)
    private const string KEY_PREFIX = "KEY-";

    // Each encrypted value is prefixed with a type identifier (because encrypted values are stored as strings)
    private const string FLOAT_PREFIX   = ":float:";
    private const string DOUBLE_PREFIX  = ":double:";
    private const string INT_PREFIX     = ":int:";
    private const string LONG_PREFIX    = ":long:";
    private const string STRING_PREFIX  = ":string:";
    private const string BOOL_PREFIX    = ":bool:";
    private const string COLOR_PREFIX   = ":Color:";
    private const string VECTOR2_PREFIX = ":Vector2:";
    private const string VECTOR3_PREFIX = ":Vector3:";
    private const string RECT_PREFIX    = ":Rect:";
    private const string BOUNDS_PREFIX  = ":Bounds:";

    private static bool IsEncryptedKey      (string encryptedKey)   { return encryptedKey.StartsWith(KEY_PREFIX); }

    private static bool IsEncryptedFloat    (string encryptedValue) { return encryptedValue.StartsWith(FLOAT_PREFIX     ); }
    private static bool IsEncryptedDouble   (string encryptedValue) { return encryptedValue.StartsWith(DOUBLE_PREFIX    ); }
    private static bool IsEncryptedInt      (string encryptedValue) { return encryptedValue.StartsWith(INT_PREFIX       ); }
    private static bool IsEncryptedLong     (string encryptedValue) { return encryptedValue.StartsWith(LONG_PREFIX      ); }
    private static bool IsEncryptedString   (string encryptedValue) { return encryptedValue.StartsWith(STRING_PREFIX    ); }
    private static bool IsEncryptedBool     (string encryptedValue) { return encryptedValue.StartsWith(BOOL_PREFIX      ); }
    private static bool IsEncryptedColor    (string encryptedValue) { return encryptedValue.StartsWith(COLOR_PREFIX     ); }
    private static bool IsEncryptedVector2  (string encryptedValue) { return encryptedValue.StartsWith(VECTOR2_PREFIX   ); }
    private static bool IsEncryptedVector3  (string encryptedValue) { return encryptedValue.StartsWith(VECTOR3_PREFIX   ); }
    private static bool IsEncryptedRect     (string encryptedValue) { return encryptedValue.StartsWith(RECT_PREFIX      ); }
    private static bool IsEncryptedBounds   (string encryptedValue) { return encryptedValue.StartsWith(BOUNDS_PREFIX    ); }

    private static string EncryptKey        (string key) { return KEY_PREFIX + EzCrypto.EncryptString(key); }

    private static string EncryptFloat      (float    value)        { return FLOAT_PREFIX    + EzCrypto.EncryptFloat     (value); }
    private static string EncryptDouble     (double   value)        { return DOUBLE_PREFIX   + EzCrypto.EncryptDouble    (value); }
    private static string EncryptInt        (int      value)        { return INT_PREFIX      + EzCrypto.EncryptInt       (value); }
    private static string EncryptLong       (long     value)        { return LONG_PREFIX     + EzCrypto.EncryptLong      (value); }
    private static string EncryptString     (string   value)        { return STRING_PREFIX   + EzCrypto.EncryptString    (value); }
    private static string EncryptBool       (bool     value)        { return BOOL_PREFIX     + EzCrypto.EncryptBool      (value); }
    private static string EncryptColor      (Color    value)        { return COLOR_PREFIX    + EzCrypto.EncryptColor     (value); }
    private static string EncryptVector2    (Vector2  value)        { return VECTOR2_PREFIX  + EzCrypto.EncryptVector2   (value); }
    private static string EncryptVector3    (Vector3  value)        { return VECTOR3_PREFIX  + EzCrypto.EncryptVector3   (value); }
    private static string EncryptRect       (Rect     value)        { return RECT_PREFIX     + EzCrypto.EncryptRect      (value); }
    private static string EncryptBounds     (Bounds   value)        { return BOUNDS_PREFIX   + EzCrypto.EncryptBounds    (value); }

    private static string DecryptKey        (string encryptedKey)   { return EzCrypto.DecryptString (encryptedKey.Substring(KEY_PREFIX.Length)); }

    private static float    DecryptFloat    (string encryptedValue) { return EzCrypto.DecryptFloat  (encryptedValue.Substring(FLOAT_PREFIX  .Length)); }
    private static double   DecryptDouble   (string encryptedValue) { return EzCrypto.DecryptDouble (encryptedValue.Substring(DOUBLE_PREFIX .Length)); }
    private static int      DecryptInt      (string encryptedValue) { return EzCrypto.DecryptInt    (encryptedValue.Substring(INT_PREFIX    .Length)); }
    private static long     DecryptLong     (string encryptedValue) { return EzCrypto.DecryptLong   (encryptedValue.Substring(LONG_PREFIX   .Length)); }
    private static string   DecryptString   (string encryptedValue) { return EzCrypto.DecryptString (encryptedValue.Substring(STRING_PREFIX .Length)); }
    private static bool     DecryptBool     (string encryptedValue) { return EzCrypto.DecryptBool   (encryptedValue.Substring(BOOL_PREFIX   .Length)); }
    private static Color    DecryptColor    (string encryptedValue) { return EzCrypto.DecryptColor  (encryptedValue.Substring(COLOR_PREFIX  .Length)); }
    private static Vector2  DecryptVector2  (string encryptedValue) { return EzCrypto.DecryptVector2(encryptedValue.Substring(VECTOR2_PREFIX.Length)); }
    private static Vector3  DecryptVector3  (string encryptedValue) { return EzCrypto.DecryptVector3(encryptedValue.Substring(VECTOR3_PREFIX.Length)); }
    private static Rect     DecryptRect     (string encryptedValue) { return EzCrypto.DecryptRect   (encryptedValue.Substring(RECT_PREFIX   .Length)); }
    private static Bounds   DecryptBounds   (string encryptedValue) { return EzCrypto.DecryptBounds (encryptedValue.Substring(BOUNDS_PREFIX .Length)); }

    public static bool HasKey(string key, bool encrypt = safeMode) {
        return PlayerPrefs.HasKey(encrypt ? EncryptKey(key) : key);
    }

    public static void DeleteKey(string key, bool encrypt = safeMode) {
        PlayerPrefs.DeleteKey(encrypt ? EncryptKey(key) : key);
    }

    public static void DeleteAll() {
        PlayerPrefs.DeleteAll();
    }

    public static void Save() {
        PlayerPrefs.Save();
    }

    public static void SetFloat(string key, float value, bool encrypt = safeMode) {
        if (encrypt) {
            PlayerPrefs.SetString(EncryptKey(key), EncryptFloat(value));
        } else {
            PlayerPrefs.SetFloat(key, value);
        }
    }

    public static float GetFloat(string key, float defaultValue = 0f, bool encrypt = safeMode) {
        if (encrypt) {
            string encryptedValue = PlayerPrefs.GetString(EncryptKey(key));
            if (string.IsNullOrEmpty(encryptedValue)) return defaultValue;
            return DecryptFloat(encryptedValue);
        } else {
            return PlayerPrefs.GetFloat(key, defaultValue);
        }
    }

    public static void SetDouble(string key, double value, bool encrypt = safeMode) {
        if (encrypt) {
            PlayerPrefs.SetString(EncryptKey(key), EncryptDouble(value));
        } else {
            PlayerPrefs.SetString(key, value.ToString());
        }
    }

    public static double GetDouble(string key, double defaultValue = 0.0, bool encrypt = safeMode) {
        if (encrypt) {
            string encryptedValue = PlayerPrefs.GetString(EncryptKey(key));
            if (string.IsNullOrEmpty(encryptedValue)) return defaultValue;
            return DecryptDouble(encryptedValue);
        } else {
            double value = defaultValue;
            if (!double.TryParse(PlayerPrefs.GetString(key), out value)) {
                return defaultValue;
            }
            return value;
        }
    }

    public static void SetInt(string key, int value, bool encrypt = safeMode) {
        if (encrypt) {
            PlayerPrefs.SetString(EncryptKey(key), EncryptInt(value));
        } else {
            PlayerPrefs.SetInt(key, value);
        }
    }

    public static int GetInt(string key, int defaultValue = 0, bool encrypt = safeMode) {
        if (encrypt) {
            string encryptedValue = PlayerPrefs.GetString(EncryptKey(key));
            if (string.IsNullOrEmpty(encryptedValue)) return defaultValue;
            return DecryptInt(encryptedValue);
        } else {
            return PlayerPrefs.GetInt(key, defaultValue);
        }
    }

    public static void SetLong(string key, long value, bool encrypt = safeMode) {
        if (encrypt) {
            PlayerPrefs.SetString(EncryptKey(key), EncryptLong(value));
        } else {
            PlayerPrefs.SetString(key, value.ToString());
        }
    }

    public static long GetLong(string key, long defaultValue = 0L, bool encrypt = safeMode) {
        if (encrypt) {
            string encryptedValue = PlayerPrefs.GetString(EncryptKey(key));
            if (string.IsNullOrEmpty(encryptedValue)) return defaultValue;
            return DecryptLong(encryptedValue);
        } else {
            long value = defaultValue;
            if (!long.TryParse(PlayerPrefs.GetString(key), out value)) {
                return defaultValue;
            }
            return value;
        }
    }

    public static void SetString(string key, string value, bool encrypt = safeMode) {
        if (encrypt) {
            PlayerPrefs.SetString(EncryptKey(key), EncryptString(value));
        } else {
            PlayerPrefs.SetString(key, value);
        }
    }

    public static string GetString(string key, string defaultValue = "", bool encrypt = safeMode) {
        if (encrypt) {
            string encryptedValue = PlayerPrefs.GetString(EncryptKey(key));
            if (string.IsNullOrEmpty(encryptedValue)) return defaultValue;
            return DecryptString(encryptedValue);
        } else {
            return PlayerPrefs.GetString(key, defaultValue);
        }
    }

    public static void SetBool(string key, bool value, bool encrypt = safeMode) {
        if (encrypt) {
            PlayerPrefs.SetString(EncryptKey(key), EncryptBool(value));
        } else {
            PlayerPrefs.SetInt(key, value ? 1 : 0);
        }
    }

    public static bool GetBool(string key, bool defaultValue = false, bool encrypt = safeMode) {
        if (encrypt) {
            string encryptedValue = PlayerPrefs.GetString(EncryptKey(key));
            if (string.IsNullOrEmpty(encryptedValue)) return defaultValue;
            return DecryptBool(encryptedValue);
        } else {
            return PlayerPrefs.GetInt(key, defaultValue ? 1 : 0) == 1;
        }
    }

    public static void SetColor(string key, Color value, bool encrypt = safeMode) {
        if (encrypt) {
            PlayerPrefs.SetString(EncryptKey(key), EncryptColor(value));
        } else {
            PlayerPrefs.SetString(key, value.r + "," + value.g + "," + value.b + "," + value.a);
        }
    }

    public static Color GetColor(string key, Color defaultValue = default(Color), bool encrypt = safeMode) {
        if (encrypt) {
            string encryptedValue = PlayerPrefs.GetString(EncryptKey(key));
            if (string.IsNullOrEmpty(encryptedValue)) return defaultValue;
            return DecryptColor(encryptedValue);
        } else {
            string color = PlayerPrefs.GetString(key);
            if (string.IsNullOrEmpty(color)) return defaultValue;
            string[] rgba = color.Split(',');
            if (rgba.Length != 4) {
                Debug.LogWarning("Invalid color string: " + color);
                return default(Color);
            }
            return new Color(float.Parse(rgba[0]), float.Parse(rgba[1]), float.Parse(rgba[2]), float.Parse(rgba[3]));
        }
    }

    public static void SetVector2(string key, Vector2 value, bool encrypt = safeMode) {
        if (encrypt) {
            PlayerPrefs.SetString(EncryptKey(key), EncryptVector2(value));
        } else {
            PlayerPrefs.SetString(key, value.x + "," + value.y);
        }
    }

    public static Vector2 GetVector2(string key, Vector2 defaultValue = default(Vector2), bool encrypt = safeMode) {
        if (encrypt) {
            string encryptedValue = PlayerPrefs.GetString(EncryptKey(key));
            if (string.IsNullOrEmpty(encryptedValue)) return defaultValue;
            return DecryptVector2(encryptedValue);
        } else {
            string vector2 = PlayerPrefs.GetString(key);
            if (string.IsNullOrEmpty(vector2)) return defaultValue;
            string[] xy = vector2.Split(',');
            if (xy.Length != 2) {
                Debug.LogWarning("Invalid vector2 string: " + vector2);
                return default(Vector2);
            }
            return new Vector2(float.Parse(xy[0]), float.Parse(xy[1]));
        }
    }

    public static void SetVector3(string key, Vector3 value, bool encrypt = safeMode) {
        if (encrypt) {
            PlayerPrefs.SetString(EncryptKey(key), EncryptVector3(value));
        } else {
            PlayerPrefs.SetString(key, value.x + "," + value.y + "," + value.z);
        }
    }

    public static Vector3 GetVector3(string key, Vector3 defaultValue = default(Vector3), bool encrypt = safeMode) {
        if (encrypt) {
            string encryptedValue = PlayerPrefs.GetString(EncryptKey(key));
            if (string.IsNullOrEmpty(encryptedValue)) return defaultValue;
            return DecryptVector3(encryptedValue);
        } else {
            string vector3 = PlayerPrefs.GetString(key);
            if (string.IsNullOrEmpty(vector3)) return defaultValue;
            string[] xyz = vector3.Split(',');
            if (xyz.Length != 3) {
                Debug.LogWarning("Invalid vector3 string: " + vector3);
                return default(Vector3);
            }
            return new Vector3(float.Parse(xyz[0]), float.Parse(xyz[1]), float.Parse(xyz[2]));
        }
    }

    public static void SetRect(string key, Rect value, bool encrypt = safeMode) {
        if (encrypt) {
            PlayerPrefs.SetString(EncryptKey(key), EncryptRect(value));
        } else {
            PlayerPrefs.SetString(key, value.x + "," + value.y + "," + value.width + "," + value.height);
        }
    }

    public static Rect GetRect(string key, Rect defaultValue = default(Rect), bool encrypt = safeMode) {
        if (encrypt) {
            string encryptedValue = PlayerPrefs.GetString(EncryptKey(key));
            if (string.IsNullOrEmpty(encryptedValue)) return defaultValue;
            return DecryptRect(encryptedValue);
        } else {
            string rect = PlayerPrefs.GetString(key);
            if (string.IsNullOrEmpty(rect)) return defaultValue;
            string[] xywh = rect.Split(',');
            if (xywh.Length != 4) {
                Debug.LogWarning("Invalid rect string: " + rect);
                return default(Rect);
            }
            return new Rect(float.Parse(xywh[0]), float.Parse(xywh[1]), float.Parse(xywh[2]), float.Parse(xywh[3]));
        }
    }

    public static void SetBounds(string key, Bounds value, bool encrypt = safeMode) {
        if (encrypt) {
            PlayerPrefs.SetString(EncryptKey(key), EncryptBounds(value));
        } else {
            PlayerPrefs.SetString(key, value.center.x + "," + value.center.y + "," + value.center.z + 
                ";" + value.size.x + "," + value.size.y + "," + value.size.z);
        }
    }

    public static Bounds GetBounds(string key, Bounds defaultValue = default(Bounds), bool encrypt = safeMode) {
        if (encrypt) {
            string encryptedValue = PlayerPrefs.GetString(EncryptKey(key));
            if (string.IsNullOrEmpty(encryptedValue)) return defaultValue;
            return DecryptBounds(encryptedValue);
        } else {
            string bounds = PlayerPrefs.GetString(key);
            if (string.IsNullOrEmpty(bounds)) return defaultValue;
            string[] values = bounds.Split(';');
            if (values.Length != 2) {
                Debug.LogWarning("Invalid bounds string: " + bounds);
                return default(Bounds);
            }
            string[] center = values[0].Split(',');
            string[] size = values[1].Split(',');
            if (center.Length != 3 || size.Length != 3) {
                Debug.LogWarning("Invalid bounds string: " + bounds);
                return default(Bounds);
            }
            return new Bounds(new Vector3(float.Parse(center[0]), float.Parse(center[1]), float.Parse(center[2])), 
                new Vector3(float.Parse(size[0]), float.Parse(size[1]), float.Parse(size[2])));
        }
    }

    public static void SetEnum(string key, Enum value, bool encrypt = safeMode) {
        SetString(key, value.ToString(), encrypt);
    }

    public static T GetEnum<T>(string key, T defaultValue = default(T), bool encrypt = safeMode) where T : struct {
        string stringValue = GetString(key, "", encrypt);
        if (string.IsNullOrEmpty(stringValue)) return defaultValue;
        return (T)Enum.Parse(typeof(T), stringValue);
    }

    public static object GetEnum(string key, Type enumType, object defaultValue, bool encrypt = safeMode) {
        string stringValue = GetString(key, "", encrypt);
        if (string.IsNullOrEmpty(stringValue)) return defaultValue;
        return Enum.Parse(enumType, stringValue);
    }

    public static void SetDateTime(string key, DateTime value, bool encrypt = safeMode) {
        // Convert to an ISO 8601 compliant string ("o"), so that it's fully qualified
        SetString(key, value.ToString("o", CultureInfo.InvariantCulture));
    }

    public static DateTime GetDateTime(string key, DateTime defaultValue = default(DateTime), bool encrypt = safeMode) {
        string stringValue = GetString(key, "", encrypt);
        if (string.IsNullOrEmpty(stringValue)) return defaultValue;
        return DateTime.Parse(stringValue, CultureInfo.InvariantCulture, DateTimeStyles.RoundtripKind);
    }

    public static void SetTimeSpan(string key, TimeSpan value) {
        // Use the TimeSpan's ToString() method to encode it as a string
        SetString(key, value.ToString());
    }

    public static TimeSpan GetTimeSpan(string key, TimeSpan defaultValue = default(TimeSpan), bool encrypt = safeMode) {
        string stringValue = GetString(key, "", encrypt);
        if (string.IsNullOrEmpty(stringValue)) return defaultValue;
        return TimeSpan.Parse(stringValue);
    }
}
