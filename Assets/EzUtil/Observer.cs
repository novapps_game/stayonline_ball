﻿using UnityEngine;

public abstract class Observer<T> : MonoBehaviour {

    protected abstract ObservableProperty<T> observableProperty { get; }

    protected virtual void OnEnable() {
        if (observableProperty != null) {
            observableProperty.AddObserver(UpdateValue);
        }
    }

    protected virtual void OnDisable() {
        if (observableProperty != null) {
            observableProperty.RemoveObserver(UpdateValue);
        }
    }

    public abstract void UpdateValue(T value);
}
