﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class CollisionEnterEvent : UnityEvent<Collision> { }

[System.Serializable]
public class CollisionStayEvent : UnityEvent<Collision> { }

[System.Serializable]
public class CollisionExitEvent : UnityEvent<Collision> { }

public class CollisionHandler : BaseHandler {

    public CollisionEnterEvent onCollisionEnter;
    [SerializeField]
    public LayerMask enterLayerMask;
    public bool enterTriggerOnce = false;

    public CollisionStayEvent onCollisionStay;
    [SerializeField]
    public LayerMask stayLayerMask;
    public bool stayTriggerOnce = false;

    public CollisionExitEvent onCollisionExit;
    [SerializeField]
    public LayerMask exitLayerMask;
    public bool exitTriggerOnce = false;

    private bool enterTriggered = false;
    private bool stayTriggered = false;
    private bool exitTriggered = false;

    void OnCollisionEnter(Collision collision) {
        if (!enterTriggered && onCollisionEnter != null && ((1 << collision.gameObject.layer) & enterLayerMask.value) != 0) {
            enterTriggered = enterTriggerOnce;
            onCollisionEnter.Invoke(collision);
        }
    }

    void OnCollisionStay(Collision collision) {
        if (!stayTriggered && onCollisionStay != null && ((1 << collision.gameObject.layer) & stayLayerMask.value) != 0) {
            stayTriggered = stayTriggerOnce;
            onCollisionStay.Invoke(collision);
        }
    }

    void OnCollisionExit(Collision collision) {
        if (!exitTriggered && onCollisionExit != null && ((1 << collision.gameObject.layer) & exitLayerMask.value) != 0) {
            exitTriggered = exitTriggerOnce;
            onCollisionExit.Invoke(collision);
        }
    }
}
