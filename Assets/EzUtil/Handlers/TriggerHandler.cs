﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class TriggerEnterEvent : UnityEvent<Collider> { }

[System.Serializable]
public class TriggerStayEvent : UnityEvent<Collider> { }

[System.Serializable]
public class TriggerExitEvent : UnityEvent<Collider> { }

public class TriggerHandler : BaseHandler {

    public TriggerEnterEvent onTriggerEnter;
    [SerializeField]
    public LayerMask enterLayerMask;
    public bool enterTriggerOnce = false;

    public TriggerStayEvent onTriggerStay;
    [SerializeField]
    public LayerMask stayLayerMask;
    public bool stayTriggerOnce = false;

    public TriggerExitEvent onTriggerExit;
    [SerializeField]
    public LayerMask exitLayerMask;
    public bool exitTriggerOnce = false;

    private bool enterTriggered = false;
    private bool stayTriggered = false;
    private bool exitTriggered = false;

    void OnTriggerEnter(Collider collider) {
        if (!enterTriggered && onTriggerEnter != null && ((1 << collider.gameObject.layer) & enterLayerMask.value) != 0) {
            enterTriggered = enterTriggerOnce;
            onTriggerEnter.Invoke(collider);
        }
    }

    void OnTriggerStay(Collider collider) {
        if (!stayTriggered && onTriggerStay != null && ((1 << collider.gameObject.layer) & stayLayerMask.value) != 0) {
            stayTriggered = stayTriggerOnce;
            onTriggerStay.Invoke(collider);
        }
    }

    void OnTriggerExit(Collider collider) {
        if (!exitTriggered && onTriggerExit != null && ((1 << collider.gameObject.layer) & exitLayerMask.value) != 0) {
            exitTriggered = exitTriggerOnce;
            onTriggerExit.Invoke(collider);
        }
    }
}
