﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MouseHandler : BaseHandler {

    [SerializeField]
    private UnityEvent onMouseDown = new UnityEvent();

    [SerializeField]
    private UnityEvent onMouseDrag = new UnityEvent();

    [SerializeField]
    private UnityEvent onMouseUp = new UnityEvent();

    public bool mouseDisabled { get; set; }

    public void OnMouseDown() {
        if (!mouseDisabled) {
            onMouseDown.Invoke();
        }
    }

    public void OnMouseDrag() {
        if (!mouseDisabled) {
            onMouseDrag.Invoke();
        }
    }

    public void OnMouseUp() {
        if (!mouseDisabled) {
            onMouseUp.Invoke();
        }
    }
}
