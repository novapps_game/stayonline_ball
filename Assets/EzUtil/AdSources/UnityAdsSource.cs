﻿#if UNITY_ADS
using System;
using UnityEngine;
using UnityEngine.Advertisements;

public class UnityAdsSource : AdSource {

#if UNITY_ANDROID
    private const string GAME_ID = "2765621";
#elif UNITY_IOS
    private const string GAME_ID = "2765622";
#else
    private const string GAME_ID = "";
#endif

    private const string INTERSTITIAL_ID = "video";
    private const string REWARDED_VIDEO_ID = "rewardedVideo";

    public string name { get { return "unity"; } }

    public static UnityAdsSource instance = new UnityAdsSource();

    private UnityAdsSource() { }

    public void Initialize() {
        Advertisement.Initialize(GAME_ID);
        Debug.Log("[UnityAds] initialized");
    }

    public bool IsInterstitialReady() {
        return Advertisement.IsReady(INTERSTITIAL_ID);
    }

    public bool ShowInterstitial(System.Action onClosed = null) {
        EzAnalytics.LogEvent("UnityAds", "Interstitial", "Check");
        if (IsInterstitialReady()) {
            ShowOptions options = new ShowOptions();
            options.resultCallback = (ShowResult result) => {
                switch (result) {
                    case ShowResult.Failed:
                        Debug.LogError("[UnityAds] interstitial failed to show");
                        EzAnalytics.LogEvent("UnityAds", "Interstitial", "Failed");
                        break;
                    case ShowResult.Skipped:
                        Debug.LogWarning("[UnityAds] interstitial was skipped");
                        EzAnalytics.LogEvent("UnityAds", "Interstitial", "Skipped");
                        if (onClosed != null) onClosed.Invoke();
                        break;
                    case ShowResult.Finished:
                        Debug.Log("[UnityAds] interstitial completed");
                        EzAnalytics.LogEvent("UnityAds", "Interstitial", "Finished");
                        if (onClosed != null) onClosed.Invoke();
                        break;
                }
            };
            Debug.Log("[UnityAds] show interstitial");
            EzAnalytics.LogEvent("UnityAds", "Interstitial", "Show");
            EzAnalytics.LogEvent("Interstitial", "Show", "UnityAds");
            Advertisement.Show(INTERSTITIAL_ID);
            return true;
        }
        Debug.Log("[UnityAds] interstitial is not ready");
        return false;
    }

    public bool IsRewardedVideoReady() {
        return Advertisement.IsReady(REWARDED_VIDEO_ID);
    }

    public bool ShowRewardedVideo(System.Action onRewarded, System.Action onCanceled = null) {
        EzAnalytics.LogEvent("UnityAds", "RewardedVideo", "Check");
        if (IsRewardedVideoReady()) {
            ShowOptions options = new ShowOptions();
            options.resultCallback = (ShowResult result) => {
                switch (result) {
                    case ShowResult.Failed:
                        Debug.LogError("[UnityAds] rewarded video failed to show");
                        EzAnalytics.LogEvent("UnityAds", "RewardedVideo", "Failed");
                        break;
                    case ShowResult.Skipped:
                        Debug.LogWarning("[UnityAds] rewarded video was skipped");
                        EzAnalytics.LogEvent("UnityAds", "RewardedVideo", "Skipped");
                        if (onCanceled != null) onCanceled.Invoke();
                        break;
                    case ShowResult.Finished:
                        Debug.Log("[UnityAds] rewarded video completed");
                        EzAnalytics.LogEvent("UnityAds", "RewardedVideo", "Finished");
                        if (onRewarded != null) onRewarded.Invoke();
                        break;
                }
            };
            Debug.Log("[UnityAds] show rewarded video");
            EzAnalytics.LogEvent("UnityAds", "RewardedVideo", "Show");
            EzAnalytics.LogEvent("RewardedVideo", "Show", "UnityAds");
            Advertisement.Show(REWARDED_VIDEO_ID, options);
            return true;
        }
        Debug.Log("[UnityAds] rewarded video is not ready");
        return false;
    }

    public void OnPause() {
    }

    public void OnResume() {
    }
}
#endif