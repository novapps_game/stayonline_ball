using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VungleSource : AdSource {

#if UNITY_IPHONE
    private const string appId = "5b7d14f3b8f0b00f7b4520cc";
    private const string interstitialPlacement = "_______IOS___-8410010";
    private const string rewardedVideoPlacement = "_______IOS___-5800047";
#elif UNITY_ANDROID
    private const string appId = "580828f7d6b05265190000f3";
    private const string interstitialPlacement = "DEFAULT-1892269";
    private const string rewardedVideoPlacement = "DEFAULT-1892269";
#else
    private const string appId = "";
    private const string interstitialPlacement = "";
    private const string rewardedVideoPlacement = "";
#endif

    private int orientation {
        get {
            switch (Screen.orientation) {
                case ScreenOrientation.Portrait: return 1;
                case ScreenOrientation.PortraitUpsideDown: return 4;
                case ScreenOrientation.Landscape: return 5;
                case ScreenOrientation.LandscapeRight: return 3;
            }
            return 6;
        }
    }

    private Action onInterstitialClose;
    private Action onRewardedVideoFinish;
    private Action onRewardedVideoCancel;

    public string name { get { return "vungle"; } }

    public static VungleSource instance = new VungleSource();

    private VungleSource() { }

    public void Initialize() {
        Vungle.onAdFinishedEvent += OnAdFinished;
        Vungle.init(appId, new string[] { interstitialPlacement, rewardedVideoPlacement });
        Debug.Log("[Vungle] initialized");
    }

    public bool IsInterstitialReady() {
        return Vungle.isAdvertAvailable(interstitialPlacement);
    }

    public bool ShowInterstitial(Action onClose = null) {
        EzAnalytics.LogEvent("Vungle", "Interstitial", "Check");
        if (IsInterstitialReady()) {
            onInterstitialClose = onClose;
            Debug.Log("[Vungle] show interstitial");
            EzAnalytics.LogEvent("Vungle", "Interstitial", "Show");
            EzAnalytics.LogEvent("Interstitial", "Show", "Vungle");
            Vungle.playAd(interstitialPlacement);
            return true;
        }
        Debug.Log("[Vungle] interstitial is not ready");
        return false;
    }

    public bool IsRewardedVideoReady() {
        return Vungle.isAdvertAvailable(rewardedVideoPlacement);
    }

    public bool ShowRewardedVideo(Action onRewarded, Action onCanceled = null) {
        EzAnalytics.LogEvent("Vungle", "RewardedVideo", "Check");
        if (IsRewardedVideoReady()) {
            onRewardedVideoFinish = onRewarded;
            onRewardedVideoCancel = onCanceled;
            Debug.Log("[Vungle] show rewarded video");
            EzAnalytics.LogEvent("Vungle", "RewardedVideo", "Show");
            EzAnalytics.LogEvent("RewardedVideo", "Show", "Vungle");
            Vungle.playAd(rewardedVideoPlacement);
            return true;
        }
        Debug.Log("[Vungle] rewarded video is not ready");
        return false;
    }

    void OnAdFinished(string finishStr, AdFinishedEventArgs args) {
        if (args.IsCompletedView && onRewardedVideoFinish != null) {
            Debug.Log("[Vungle] rewarded video completed"+ finishStr);
            EzAnalytics.LogEvent("Vungle", "RewardedVideo", "Finished" + finishStr);
            onRewardedVideoFinish.Invoke();
            onRewardedVideoFinish = null;
        } else if (onRewardedVideoCancel != null) {
            Debug.LogWarning("[Vungle] rewarded video was skipped" + finishStr);
            EzAnalytics.LogEvent("Vungle", "RewardedVideo", "Skipped" + finishStr);
            onRewardedVideoCancel.Invoke();
            onRewardedVideoCancel = null;
        } else if (onInterstitialClose != null) {
            Debug.Log("[Vungle] interstitial completed" + finishStr);
            EzAnalytics.LogEvent("Vungle", "Interstitial", "Closed" + finishStr);
            onInterstitialClose.Invoke();
            onInterstitialClose = null;
        }
    }

    public void OnPause() {
        Vungle.onPause();
    }

    public void OnResume() {
        Vungle.onResume();
    }
}
