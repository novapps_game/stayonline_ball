﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net;
using System.Net.Sockets;
using System.Globalization;
using UnityEngine;

public class EzTime {

    private const string WEB_TIME_URL = "http://www.baidu.com";

    /// <summary>  
    /// 获取网页提供的时间
    /// 通过分析网页报头，查找Date对应的值，获得GMT格式的时间，并转化为本地时间传递给onFetched回调
    /// 该接口在协程中异步执行，不会阻塞UI
    /// </summary>
    public static IEnumerator FetchNetTime(Action<DateTime> onFetched, Action<string> onFailed = null) {
        WWW www = new WWW(WEB_TIME_URL);
        yield return www;
        if (!string.IsNullOrEmpty(www.error)) {
            if (onFailed != null) {
                onFailed.Invoke(www.error);
            }
        } else if (www.responseHeaders == null) {
            if (onFailed != null) {
                onFailed.Invoke("Response headers is empty!");
            }
        } else { 
            string gmt = "";
            if (!www.responseHeaders.TryGetValue("Date", out gmt)) {
                if (onFailed != null) {
                    onFailed.Invoke("Invalid response headers of url: " + www.url);
                }
            } else {
                DateTime dt = GMT2Local(gmt);
                if (onFetched != null) {
                    onFetched.Invoke(dt);
                }
            }
        }
    }

    /// <summary>    
    /// GMT时间转成本地时间   
    /// DateTime dt1 = GMT2Local("Thu, 29 Sep 2014 07:04:39 GMT");
    /// 转换后的dt1为：2014-9-29 15:04:39  
    /// DateTime dt2 = GMT2Local("Thu, 29 Sep 2014 15:04:39 GMT+0800");  
    /// 转换后的dt2为：2014-9-29 15:04:39
    /// </summary>    
    public static DateTime GMT2Local(string gmt) {
        DateTime dt = DateTime.MinValue;
        try {
            string pattern = "";
            if (gmt.IndexOf("+0") != -1) {
                gmt = gmt.Replace("GMT", "");
                pattern = "ddd, dd MMM yyyy HH':'mm':'ss zzz";
            }
            if (gmt.ToUpper().IndexOf("GMT") != -1) {
                pattern = "ddd, dd MMM yyyy HH':'mm':'ss 'GMT'";
            }
            if (pattern != "") {
                dt = DateTime.ParseExact(gmt, pattern, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
                dt = dt.ToLocalTime();
            } else {
                dt = Convert.ToDateTime(gmt);
            }
        } catch (Exception e) {
            Debug.LogWarning("Failed to convert GMT to local date time: " + e.Message);
        }
        return dt;
    }

    /// <summary>    
    /// 本地时间转成GMT时间    
    /// string s = ToGMTString(DateTime.Now);  
    /// 本地时间为：2014-9-29 15:04:39  
    /// 转换后的时间为：Thu, 29 Sep 2014 07:04:39 GMT  
    /// </summary>
    public static string LocalToGMTString(DateTime dt) {
        return dt.ToUniversalTime().ToString("r");
    }

    /// <summary>
    /// 本地时间转成GMT格式的时间  
    /// string s = ToGMTFormat(DateTime.Now);  
    /// 本地时间为：2014-9-29 15:04:39  
    /// 转换后的时间为：Thu, 29 Sep 2014 15:04:39 GMT+0800  
    /// </summary>
    public static string LocalToGMTFormat(DateTime dt) {
        return dt.ToString("r") + dt.ToString("zzz").Replace(":", "");
    }

    private const string NTP_SERVER = "time.pool.aliyun.com";

    public static DateTime GetNetworkTime() {
        DateTime dateTime = DateTime.Now;
        try {
            // NTP message size - 16 bytes of the digest (RFC 2030)
            var ntpData = new byte[48];

            //Setting the Leap Indicator, Version Number and Mode values
            ntpData[0] = 0x1B; //LI = 0 (no warning), VN = 3 (IPv4 only), Mode = 3 (Client Mode)

            var addresses = Dns.GetHostEntry(NTP_SERVER).AddressList;

            //The UDP port number assigned to NTP is 123
            var ipEndPoint = new IPEndPoint(addresses[0], 123);
            //NTP uses UDP

            using (var socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp)) {
                socket.Connect(ipEndPoint);

                //Stops code hang if NTP is blocked
                socket.ReceiveTimeout = 3000;

                socket.Send(ntpData);
                socket.Receive(ntpData);
                socket.Close();
            }

            //Offset to get to the "Transmit Timestamp" field (time at which the reply 
            //departed the server for the client, in 64-bit timestamp format."
            const byte serverReplyTime = 40;

            //Get the seconds part
            ulong intPart = BitConverter.ToUInt32(ntpData, serverReplyTime);

            //Get the seconds fraction
            ulong fractPart = BitConverter.ToUInt32(ntpData, serverReplyTime + 4);

            //Convert From big-endian to little-endian
            intPart = SwapEndianness(intPart);
            fractPart = SwapEndianness(fractPart);

            var milliseconds = (intPart * 1000) + ((fractPart * 1000) / 0x100000000L);

            //**UTC** time
            dateTime = (new DateTime(1900, 1, 1, 0, 0, 0, DateTimeKind.Utc)).AddMilliseconds((long)milliseconds);
        } catch (Exception e) {
            Debug.LogError(e);
        }
        return dateTime;
    }

    private static uint SwapEndianness(ulong x) {
        return (uint)(((x & 0x000000ff) << 24) +
                       ((x & 0x0000ff00) << 8) +
                       ((x & 0x00ff0000) >> 8) +
                       ((x & 0xff000000) >> 24));
    }
}
