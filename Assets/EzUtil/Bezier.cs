﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Bezier {

	public static Vector3 GetPoint(Vector3 start, Vector3 control, Vector3 end, float t) {
        t = Mathf.Clamp01(t);
        float oneMinusT = 1f - t;
        return oneMinusT * oneMinusT * start + 
            2f * oneMinusT * t * control + 
            t * t * end;
    }

    public static Vector3 GetFirstDerivative(Vector3 start, Vector3 control, Vector3 end, float t) {
        return 2f * (1f - t) * (control - start) + 
            2f * t * (end - control);
    }

    public static Vector3 GetPoint(Vector3 start, Vector3 control1, Vector3 control2, Vector3 end, float t) {
        t = Mathf.Clamp01(t);
        float oneMinusT = 1f - t;
        return oneMinusT * oneMinusT * oneMinusT * start +
            3f * oneMinusT * oneMinusT * t * control1 +
            3f * oneMinusT * t * t * control2 +
            t * t * t * end;
    }

    public static Vector3 GetFirstDerivative(Vector3 start, Vector3 control1, Vector3 control2, Vector3 end, float t) {
        t = Mathf.Clamp01(t);
        float oneMinusT = 1f - t;
        return 3f * oneMinusT * oneMinusT * (control1 - start) +
            6f * oneMinusT * t * (control2 - control1) +
            3f * t * t * (end - control2);
    }
}
