﻿using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : Component {

    public static T instance {
        get {
            if (_instance == null) {
                _instance = FindObjectOfType<T>();
                if (_instance == null) {
                    _instance = GameManager.instance.Instantiate<T>(typeof(T).Name);
                    if (_instance == null) {
                        string error = "Cannot find singleton instance of " + typeof(T).Name + " in scene or resources";
                        Debug.Log(error);
                    }
                }
            }
            return _instance;
        }
    }
    private static T _instance;

    public static T GetInstance() { return instance; }

}
