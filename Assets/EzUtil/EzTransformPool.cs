﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EzTransformPool : MonoBehaviour {

    public GameObject prefab;
    public Transform rootTransform;
    public int maxSize = 0;
    public float chance = 1;

    public Transform root { get { return rootTransform != null ? rootTransform : transform; } }

    public virtual Transform Create() {
        if (!gameObject.activeSelf) {
            return null;
        }
        if (prefab == null) {
            prefab = root.GetChild(0).gameObject;
        }
        Transform child = root.FindFirstInactiveChild();
        if (child == null) {
            if (maxSize <= 0 || root.childCount < maxSize) {
                child = Instantiate(prefab).transform;
                child.gameObject.name = prefab.name;
                child.SetParent(root, false);
            }
        }
        if (child != null) {
            child.gameObject.SetActive(true);
        }
        return child;
    }

    public T Create<T>() where T : Component {
        Transform child = Create();
        return (child != null) ? child.GetComponent<T>() : null;
    }

    public virtual void Recycle(Transform child) {
        child.gameObject.SetActive(false);
        child.SetParent(root);
    }
}
