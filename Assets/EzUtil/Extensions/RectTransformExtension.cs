﻿using UnityEngine;
using System.Collections;

public static class RectTransformExtension {

	public static Vector2 GetWorldSize(this RectTransform self) {
        Vector3[] corners = new Vector3[4];
        self.GetWorldCorners(corners);
        Vector2 size = corners[2] - corners[0];
        size.x = Mathf.Abs(size.x);
        size.y = Mathf.Abs(size.y);
        return size;
	}

	public static Rect GetScreenRect(this RectTransform self, Canvas canvas = null) {
		if (canvas == null) canvas = self.GetComponentInParent<Canvas>();
		Vector3[] corners = new Vector3[4];
		self.GetWorldCorners(corners);
		if (canvas.renderMode == RenderMode.ScreenSpaceCamera || canvas.renderMode == RenderMode.WorldSpace) {
            corners[1] = RectTransformUtility.WorldToScreenPoint(canvas.worldCamera, corners[0]);
            corners[3] = RectTransformUtility.WorldToScreenPoint(canvas.worldCamera, corners[2]);
		} else {
            corners[1] = RectTransformUtility.WorldToScreenPoint(null, corners[0]);
            corners[3] = RectTransformUtility.WorldToScreenPoint(null, corners[2]);
		}
		corners[1].x = Mathf.Round(corners[1].x);
		corners[1].y = Mathf.Round(corners[1].y);
		corners[3].x = Mathf.Round(corners[3].x);
        corners[3].y = Mathf.Round(corners[3].y);
        Vector2 min = new Vector2(Mathf.Min(corners[1].x, corners[3].x), Mathf.Min(corners[1].y, corners[3].y));
        Vector2 max = new Vector2(Mathf.Max(corners[1].x, corners[3].x), Mathf.Max(corners[1].y, corners[3].y));
        return new Rect(min, max - min);
	}

	public static void SetSize(this RectTransform self, Vector2 newSize) {
		Vector2 oldSize = self.rect.size;
		Vector2 deltaSize = newSize - oldSize;
		self.offsetMin = self.offsetMin - new Vector2(deltaSize.x * self.pivot.x, deltaSize.y * self.pivot.y);
		self.offsetMax = self.offsetMax + new Vector2(deltaSize.x * (1f - self.pivot.x), deltaSize.y * (1f - self.pivot.y));
	}

    public static bool HitTest(this RectTransform self, Vector2 screenPosition) {
        Rect screenRect = self.GetScreenRect();
        return screenRect.Contains(screenPosition);
    }

}