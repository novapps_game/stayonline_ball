﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DoubleExtension {

    public static string ToDigits(this double self) {
        return Digit.Format(self, "G", Digit.DIGITS);
    }

    public static string ToDigits(this double self, Digit[] digits) {
        return Digit.Format(self, "G", digits);
    }

    public static string ToDigits(this double self, string format) {
        return Digit.Format(self, format, Digit.DIGITS);
    }

    public static string ToDigits(this double self, string format, Digit[] digits) {
        return Digit.Format(self, format, digits);
    }
}
