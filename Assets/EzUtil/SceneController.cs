﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneController<T> : Singleton<T> where T : Component {

    // Use this for initialization
    void Start() {
        OnStart();
        GameManager.instance.OnStartScene();
    }

    protected virtual void OnStart() {

    }

}
