﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LerpDouble : MonoBehaviour {

    public double changeSpeed = 10;

    public double targetValue {
        get { return _targetValue; }
        set {
            if (_targetValue != value) {
                _targetValue = value;
                OnTargetValueChanged();
            }
        }
    }
    private double _targetValue;

    [System.Serializable]
    public class LerpDoubleEvent : UnityEvent<double> { }
    public LerpDoubleEvent onValueChanging = new LerpDoubleEvent();
    public LerpDoubleEvent onValueChanged = new LerpDoubleEvent();

    public double value { get; private set; }

    private double minDiff;

    private const double TOL = 0.01;

    public void SetValue(double value) {
        this.value = targetValue = value;
        minDiff = 0;
        OnValueChanging();
        OnValueChanged();
    }

    protected virtual void Update() {
        double diff = EzMath.Abs(targetValue - value);
        if (diff < minDiff) {
            value = targetValue;
            OnValueChanging();
            OnValueChanged();
        } else {
            value = EzMath.Lerp(value, targetValue, Time.deltaTime * changeSpeed);
            OnValueChanging();
        }
    }

    protected virtual void OnTargetValueChanged() {
        minDiff = EzMath.Abs(targetValue - value) * TOL;
    }

    protected virtual void OnValueChanging() {
        onValueChanging.Invoke(value);
    }

    protected virtual void OnValueChanged() {
        onValueChanged.Invoke(value);
    }
}
