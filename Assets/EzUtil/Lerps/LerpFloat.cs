﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LerpFloat : MonoBehaviour {

    public float targetValue;
    public float changeSpeed = 10;

    [System.Serializable]
    public class LerpFloatEvent : UnityEvent<float> { }
    public LerpFloatEvent onValueChanging = new LerpFloatEvent();
    public LerpFloatEvent onValueChanged = new LerpFloatEvent();

    public float value { get; private set; }

    private const float TOL = 1e-5f;

    public void SetValue(float value) {
        this.value = targetValue = value;
        OnValueChanging();
        OnValueChanged();
    }

    protected virtual void Update() {
        float diff = Mathf.Abs(targetValue - value);
        if (diff < Mathf.Abs(value) * TOL) {
            if (diff > 0) {
                value = targetValue;
                OnValueChanging();
                OnValueChanged();
            }
        } else {
            value = Mathf.Lerp(value, targetValue, Time.deltaTime * changeSpeed);
            OnValueChanging();
        }
    }

    protected virtual void OnValueChanging() {
        onValueChanging.Invoke(value);
    }

    protected virtual void OnValueChanged() {
        onValueChanged.Invoke(value);
    }
}
