﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LerpInt : MonoBehaviour {

    public long targetValue;
    public long changeSpeed = 10;

    [System.Serializable]
    public class LerplongEvent : UnityEvent<long> { }
    public LerplongEvent onValueChanging = new LerplongEvent();
    public LerplongEvent onValueChanged = new LerplongEvent();

    public long value { get; private set; }

    private long fValue;

    private const float TOL = 1e-5f;

    public void SetValue(long value) {
        this.value = targetValue = value;
        fValue = value;
        OnValueChanging();
        OnValueChanged();
    }

    protected virtual void Update() {
        //float diff = Mathf.Abs(targetValue - value);
        //if (diff < Mathf.Abs(value) * TOL) {
        //    if (diff > 0) {
        //        value = targetValue;
        //        OnValueChanging();
        //        OnValueChanged();
        //    }
        //} else {
        //    fValue = Mathf.Lerp(fValue, targetValue, Time.deltaTime * changeSpeed);
        //    value = Mathf.RoundToInt(fValue);
        //    OnValueChanging();
        //}
        OnValueChanged();
    }

    protected virtual void OnValueChanging() {
        onValueChanging.Invoke(value);
    }

    protected virtual void OnValueChanged() {
        onValueChanged.Invoke(value);
    }
}
