﻿using System;
using UnityEngine;

public class EzMath {

    // Returns the sine of angle /f/ in radians.
    public static double Sin(double f) { return Math.Sin(f); }

    // Returns the cosine of angle /f/ in radians.
    public static double Cos(double f) { return Math.Cos(f); }

    // Returns the tangent of angle /f/ in radians.
    public static double Tan(double f) { return Math.Tan(f); }

    // Returns the arc-sine of /f/ - the angle in radians whose sine is /f/.
    public static double Asin(double f) { return Math.Asin(f); }

    // Returns the arc-cosine of /f/ - the angle in radians whose cosine is /f/.
    public static double Acos(double f) { return Math.Acos(f); }

    // Returns the arc-tangent of /f/ - the angle in radians whose tangent is /f/.
    public static double Atan(double f) { return Math.Atan(f); }

    // Returns the angle in radians whose ::ref::Tan is @@y/x@@.
    public static double Atan2(double y, double x) { return Math.Atan2(y, x); }

    // Returns square root of /f/.
    public static double Sqrt(double f) { return Math.Sqrt(f); }

    // Returns the absolute value of /f/.
    public static double Abs(double f) { return Math.Abs(f); }

    // Returns the absolute value of /value/.
    public static int Abs(int value) { return Math.Abs(value); }

    /// *listonly*
    public static double Min(double a, double b) { return a < b ? a : b; }
    // Returns the smallest of two or more values.
    public static double Min(params double[] values) {
        int len = values.Length;
        if (len == 0)
            return 0;
        double m = values[0];
        for (int i = 1; i < len; i++) {
            if (values[i] < m)
                m = values[i];
        }
        return m;
    }

    /// *listonly*
    public static int Min(int a, int b) { return a < b ? a : b; }
    // Returns the smallest of two or more values.
    public static int Min(params int[] values) {
        int len = values.Length;
        if (len == 0)
            return 0;
        int m = values[0];
        for (int i = 1; i < len; i++) {
            if (values[i] < m)
                m = values[i];
        }
        return m;
    }

    /// *listonly*
    public static double Max(double a, double b) { return a > b ? a : b; }
    // Returns largest of two or more values.
    public static double Max(params double[] values) {
        int len = values.Length;
        if (len == 0)
            return 0;
        double m = values[0];
        for (int i = 1; i < len; i++) {
            if (values[i] > m)
                m = values[i];
        }
        return m;
    }

    /// *listonly*
    public static int Max(int a, int b) { return a > b ? a : b; }
    // Returns the largest of two or more values.
    public static int Max(params int[] values) {
        int len = values.Length;
        if (len == 0)
            return 0;
        int m = values[0];
        for (int i = 1; i < len; i++) {
            if (values[i] > m)
                m = values[i];
        }
        return m;
    }

    // Returns /f/ raised to power /p/.
    public static double Pow(double f, double p) { return Math.Pow(f, p); }

    // Returns e raised to the specified power.
    public static double Exp(double power) { return Math.Exp(power); }

    // Returns the logarithm of a specified number in a specified base.
    public static double Log(double f, double p) { return Math.Log(f, p); }

    // Returns the natural (base e) logarithm of a specified number.
    public static double Log(double f) { return Math.Log(f); }

    // Returns the base 10 logarithm of a specified number.
    public static double Log10(double f) { return Math.Log10(f); }

    // Returns the smallest integer greater to or equal to /f/.
    public static double Ceil(double f) { return Math.Ceiling(f); }

    // Returns the largest integer smaller to or equal to /f/.
    public static double Floor(double f) { return Math.Floor(f); }

    // Returns /f/ rounded to the nearest integer.
    public static double Round(double f) { return Math.Round(f); }

    // Returns the smallest integer greater to or equal to /f/.
    public static long CeilToLong(double f) { return (long)Math.Ceiling(f); }

    // Returns the largest integer smaller to or equal to /f/.
    public static long FloorToLong(double f) { return (long)Math.Floor(f); }

    // Returns /f/ rounded to the nearest integer.
    public static long RoundToLong(double f) { return (long)Math.Round(f); }

    // Returns the sign of /f/.
    public static double Sign(double f) { return f >= 0 ? 1 : -1; }

    public const double E = Math.E;

    // The infamous ''3.14159265358979...'' value (RO).
    public const double PI = Math.PI;

    // A representation of positive infinity (RO).
    public const double Infinity = Double.PositiveInfinity;

    // A representation of negative infinity (RO).
    public const double NegativeInfinity = Double.NegativeInfinity;

    // Degrees-to-radians conversion constant (RO).
    public const double Deg2Rad = PI * 2 / 360;

    // Radians-to-degrees conversion constant (RO).
    public const double Rad2Deg = 1 / Deg2Rad;

    // A tiny doubleing point value (RO).
    public static readonly double Epsilon = Double.Epsilon;

    // Clamps a value between a minimum double and maximum double value.
    public static double Clamp(double value, double min, double max) {
        if (value < min)
            value = min;
        else if (value > max)
            value = max;
        return value;
    }

    // Clamps value between min and max and returns value.
    // Set the position of the transform to be that of the time
    // but never less than 1 or more than 3
    //
    public static int Clamp(int value, int min, int max) {
        if (value < min)
            value = min;
        else if (value > max)
            value = max;
        return value;
    }

    // Clamps value between 0 and 1 and returns value
    public static double Clamp01(double value) {
        return Clamp(value, 0, 1);
    }

    // Interpolates between /a/ and /b/ by /t/. /t/ is clamped between 0 and 1.
    public static double Lerp(double a, double b, double t) {
        return a + (b - a) * Clamp01(t);
    }

    // Interpolates between /a/ and /b/ by /t/ without clamping the interpolant.
    public static double LerpUnclamped(double a, double b, double t) {
        return a + (b - a) * t;
    }

    // Same as ::ref::Lerp but makes sure the values interpolate correctly when they wrap around 360 degrees.
    public static double LerpAngle(double a, double b, double t) {
        double delta = Repeat((b - a), 360);
        if (delta > 180)
            delta -= 360;
        return a + delta * Clamp01(t);
    }

    // Moves a value /current/ towards /target/.
    public static double MoveTowards(double current, double target, double maxDelta) {
        if (Abs(target - current) <= maxDelta)
            return target;
        return current + Sign(target - current) * maxDelta;
    }

    // Same as ::ref::MoveTowards but makes sure the values interpolate correctly when they wrap around 360 degrees.
    public static double MoveTowardsAngle(double current, double target, double maxDelta) {
        double deltaAngle = DeltaAngle(current, target);
        if (-maxDelta < deltaAngle && deltaAngle < maxDelta)
            return target;
        target = current + deltaAngle;
        return MoveTowards(current, target, maxDelta);
    }

    // Interpolates between /min/ and /max/ with smoothing at the limits.
    public static double SmoothStep(double from, double to, double t) {
        t = Clamp01(t);
        t = -2.0 * t * t * t + 3.0 * t * t;
        return to * t + from * (1 - t);
    }

    //*undocumented
    public static double Gamma(double value, double absmax, double gamma) {
        bool negative = false;
        if (value < 0)
            negative = true;
        double absval = Abs(value);
        if (absval > absmax)
            return negative ? -absval : absval;

        double result = Pow(absval / absmax, gamma) * absmax;
        return negative ? -result : result;
    }

    // Compares two doubleing point values if they are similar.
    public static bool Approximately(double a, double b) {
        // If a or b is zero, compare that the other is less or equal to epsilon.
        // If neither a or b are 0, then find an epsilon that is good for
        // comparing numbers at the maximum magnitude of a and b.
        // Floating points have about 7 significant digits, so
        // 1.000001f can be represented while 1.0000001f is rounded to zero,
        // thus we could use an epsilon of 0.000001f for comparing values close to 1.
        // We multiply this epsilon by the biggest magnitude of a and b.
        return Abs(b - a) < Max(0.000001 * Max(Abs(a), Abs(b)), Epsilon * 8);
    }

    public static double SmoothDamp(double current, double target, ref double currentVelocity, double smoothTime) {
        return SmoothDamp(current, target, ref currentVelocity, smoothTime, Infinity, Time.deltaTime);
    }

    public static double SmoothDamp(double current, double target, ref double currentVelocity, double smoothTime, double maxSpeed) {
        return SmoothDamp(current, target, ref currentVelocity, smoothTime, maxSpeed, Time.deltaTime);
    }

    // Gradually changes a value towards a desired goal over time.
    public static double SmoothDamp(double current, double target, ref double currentVelocity, double smoothTime, double maxSpeed, double deltaTime) {
        // Based on Game Programming Gems 4 Chapter 1.10
        smoothTime = Max(0.0001, smoothTime);
        double omega = 2 / smoothTime;

        double x = omega * deltaTime;
        double exp = 1 / (1 + x + 0.48 * x * x + 0.235 * x * x * x);
        double change = current - target;
        double originalTo = target;

        // Clamp maximum speed
        double maxChange = maxSpeed * smoothTime;
        change = Clamp(change, -maxChange, maxChange);
        target = current - change;

        double temp = (currentVelocity + omega * change) * deltaTime;
        currentVelocity = (currentVelocity - omega * temp) * exp;
        double output = target + (change + temp) * exp;

        // Prevent overshooting
        if (originalTo - current > 0.0 == output > originalTo) {
            output = originalTo;
            currentVelocity = (output - originalTo) / deltaTime;
        }

        return output;
    }

    public static double SmoothDampAngle(double current, double target, ref double currentVelocity, double smoothTime) {
        return SmoothDampAngle(current, target, ref currentVelocity, smoothTime, Infinity, Time.deltaTime);
    }

    public static double SmoothDampAngle(double current, double target, ref double currentVelocity, double smoothTime, double maxSpeed) {
        return SmoothDampAngle(current, target, ref currentVelocity, smoothTime, maxSpeed, Time.deltaTime);
    }

    // Gradually changes an angle given in degrees towards a desired goal angle over time.
    public static double SmoothDampAngle(double current, double target, ref double currentVelocity, double smoothTime, double maxSpeed, double deltaTime) {
        target = current + DeltaAngle(current, target);
        return SmoothDamp(current, target, ref currentVelocity, smoothTime, maxSpeed, deltaTime);
    }

    // Loops the value t, so that it is never larger than length and never smaller than 0.
    public static double Repeat(double t, double length) {
        return Clamp(t - Floor(t / length) * length, 0.0, length);
    }

    // PingPongs the value t, so that it is never larger than length and never smaller than 0.
    public static double PingPong(double t, double length) {
        t = Repeat(t, length * 2);
        return length - Abs(t - length);
    }

    // Calculates the ::ref::Lerp parameter between of two values.
    public static double InverseLerp(double a, double b, double value) {
        return (a != b) ? Clamp01((value - a) / (b - a)) : 0.0;
    }

    // Calculates the shortest difference between two given angles.
    public static double DeltaAngle(double current, double target) {
        double delta = Repeat(target - current, 360);
        if (delta > 180)
            delta -= 360;
        return delta;
    }
}
