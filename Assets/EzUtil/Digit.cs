﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Digit {

    public string name;
    public string symbol;
    public int place;
    public double unit;

    public Digit(string name, int place)
        : this(name, name, place) {
    }

    public Digit(string name, string symbol, int place) {
        this.name = name;
        this.symbol = symbol;
        this.place = place;
        unit = System.Math.Pow(10, place);
    }

    public static string Format(double value) {
        return Format(value, "G", DIGITS);
    }

    public static string Format(double value, Digit[] digits) {
        return Format(value, "G", digits);
    }

    public static string Format(double value, string format) {
        return Format(value, format, DIGITS);
    }

    public static string Format(double value, string format, Digit[] digits) {
        double v = 0;
        int i = 0;
        for (; i < digits.Length - 1; ++i) {
            if (System.Math.Abs(value) < digits[i + 1].unit) {
                if (i == 0) {
                    v = System.Math.Round(value / digits[i].unit);
                } else {
                    v = System.Math.Round(value / digits[i].unit, digits[i + 1].place - digits[i].place);
                }
                return v + digits[i].symbol;
            }
        }
        v = System.Math.Round(value / digits[i].unit, digits[i].place - digits[i - 1].place);
        return v.ToString(format) + digits[i].symbol;
    }

    public static Digit[] DIGITS {
        get {
            //if (Localization.GetLanguage() == Localization.LANGUAGE_CHINESE) {
            //    return DIGITS_CN;
            //} else if (Localization.GetLanguage() == Localization.LANGUAGE_CHINESE_TRADITIONAL) {
            //    return DIGITS_CT;
            //}
            return DIGITS_EN;
        }
    }

    public static readonly Digit[] DIGITS_EN = {
        new Digit("", 0),
        new Digit("Thousand",           "K",     3),
        new Digit("Million",            "M",     6),
        new Digit("Billion",            "B",     9),
        new Digit("trillion",           "t",     12),
        new Digit("quadrillion",        "q",     15),
        new Digit("Quintillion",        "Q",     18),
        new Digit("sextillion",         "s",     21),
        new Digit("Septillion",         "S",     24),
        new Digit("octillion",          "o",     27),
        new Digit("nonillion",          "n",     30),
        new Digit("decillion",          "d",     33),
        new Digit("Undecillion",        "U",     36),
        new Digit("Duodecillion",       "D",     39),
        new Digit("Tredecillion",       "T",     42),
        new Digit("Quattuordecillion",  "Qt",    45),
        new Digit("Quindecillion",      "Qd",    48),
        new Digit("Sexdecillion",       "Sd",    51),
        new Digit("Septendecillion",    "St",    54),
        new Digit("Octodecillion",      "O",     57),
        new Digit("Novemdecillion",     "N",     60),
        new Digit("vigintillion",       "v",     63),
    };

    public static readonly Digit[] DIGITS_CN = {
        new Digit("", 0),
        new Digit("万", 4),
        new Digit("亿", 8),
        new Digit("兆", 12),
        new Digit("京", 16),
        new Digit("垓", 20),
        new Digit("秭", 24),
        new Digit("穰", 28),
        new Digit("溝", 32),
        new Digit("澗", 36),
        new Digit("正", 40),
        new Digit("載", 44),
        new Digit("极", 48),
    };

    public static readonly Digit[] DIGITS_CT = {
        new Digit("", 0),
        new Digit("萬", 4),
        new Digit("億", 8),
        new Digit("兆", 12),
        new Digit("京", 16),
        new Digit("垓", 20),
        new Digit("秭", 24),
        new Digit("穰", 28),
        new Digit("溝", 32),
        new Digit("澗", 36),
        new Digit("正", 40),
        new Digit("載", 44),
        new Digit("極", 48),
    };
}
