﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EzAssetSpawner : Singleton<EzAssetSpawner> {

    private Dictionary<string, GameObject> prefabs = new Dictionary<string, GameObject>();

    [System.Serializable]
    public class OnCachedEvent : UnityEvent<GameObject> { }
    [SerializeField]
    private OnCachedEvent onCached = new OnCachedEvent();

    [System.Serializable]
    public class OnCacheProgress : UnityEvent<float> { }
    [SerializeField]
    private OnCacheProgress onCacheProgress = new OnCacheProgress();

    public GameObject Spawn(string url, string name) {
        GameObject prefab = GetCachedPrefab(url, name);
        if (prefab == null) {
            return null;
        }
        return Instantiate(prefab);
    }

    public T Spawn<T>(string url, string name) where T : Component {
        GameObject go = Spawn(url, name);
        return go != null ? go.GetComponent<T>() : null;
    }

    private static string GetKeyOfPrefab(string url, string name) {
        return url + ":" + name;
    }

    public bool IsCached(string url, string name) {
        return prefabs.ContainsKey(GetKeyOfPrefab(url, name));
    }

    public GameObject GetCachedPrefab(string url, string name) {
        string key = GetKeyOfPrefab(url, name);
        if (prefabs.ContainsKey(key)) {
            return prefabs[key];
        }
        return null;
    }

    public void Cache(string url, string name,
        UnityAction<GameObject> onComplete = null,
        UnityAction<float> onProgress = null) {
        if (!IsCached(url, name)) {
            if (onComplete != null) onCached.AddListener(onComplete);
            if (onProgress != null) onCacheProgress.AddListener(onProgress);
            StartCoroutine(AssetBundleLoader.DownloadObjectFromAssetBundle<GameObject>(url, name, 0, OnComplete, OnError, OnProgress));
        }
    }

    void OnComplete(GameObject prefab, string url, string name) {
        prefabs.Add(GetKeyOfPrefab(url, name), prefab);
        onCached.Invoke(prefab);
    }

    void OnProgress(float progress) {
        onCacheProgress.Invoke(progress);
    }

    void OnError(string error) {
        Debug.LogError(error);
    }

}
