﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class BoolPersistentProperty : BoolObservableProperty {

    private string name;
    private bool defaultValue;

    public BoolPersistentProperty(string name, bool defaultValue = false) {
        if (string.IsNullOrEmpty(name)) {
            Debug.LogWarning("empty name of persistent property");
            SetValue(defaultValue);
        } else {
            this.name = name;
            SetValue(EzPrefs.GetBool(name, defaultValue));
        }
    }

    protected override void SetValue(bool value) {
        base.SetValue(value);
        if (!string.IsNullOrEmpty(name)) {
            EzPrefs.SetBool(name, value);
        }
    }
}


[System.Serializable]
public class IntPersistentProperty : IntObservableProperty {

    private string name;
    private int defaultValue;

    public IntPersistentProperty(string name, int defaultValue = 0) {
        if (string.IsNullOrEmpty(name)) {
            Debug.LogWarning("empty name of persistent property");
            SetValue(defaultValue);
        } else {
            this.name = name;
            SetValue(EzPrefs.GetInt(name, defaultValue));
        }
    }

    protected override void SetValue(int value) {
        base.SetValue(value);
        if (!string.IsNullOrEmpty(name)) {
            EzPrefs.SetInt(name, value);
        }
    }
}


[System.Serializable]
public class FloatPersistentProperty : FloatObservableProperty {

    private string name;
    private float defaultValue;

    public FloatPersistentProperty(string name, float defaultValue = 0) {
        if (string.IsNullOrEmpty(name)) {
            Debug.LogWarning("empty name of persistent property");
            SetValue(defaultValue);
        } else {
            this.name = name;
            SetValue(EzPrefs.GetFloat(name, defaultValue));
        }
    }

    protected override void SetValue(float value) {
        base.SetValue(value);
        if (!string.IsNullOrEmpty(name)) {
            EzPrefs.SetFloat(name, value);
        }
    }
}


[System.Serializable]
public class LongPersistentProperty : LongObservableProperty {

    private string name;
    private long defaultValue;

    public LongPersistentProperty(string name, long defaultValue = 0) {
        if (string.IsNullOrEmpty(name)) {
            Debug.LogWarning("empty name of persistent property");
            SetValue(defaultValue);
        } else {
            this.name = name;
            SetValue(EzPrefs.GetLong(name, defaultValue));
        }
    }

    protected override void SetValue(long value) {
        base.SetValue(value);
        if (!string.IsNullOrEmpty(name)) {
            EzPrefs.SetLong(name, value);
        }
    }
}


[System.Serializable]
public class DoublePersistentProperty : DoubleObservableProperty {

    private string name;
    private double defaultValue;

    public DoublePersistentProperty(string name, double defaultValue = 0) {
        if (string.IsNullOrEmpty(name)) {
            Debug.LogWarning("empty name of persistent property");
            SetValue(defaultValue);
        } else {
            this.name = name;
            SetValue(EzPrefs.GetDouble(name, defaultValue));
        }
    }

    protected override void SetValue(double value) {
        base.SetValue(value);
        if (!string.IsNullOrEmpty(name)) {
            EzPrefs.SetDouble(name, value);
        }
    }
}


[System.Serializable]
public class StringPersistentProperty : StringObservableProperty {

    private string name;
    private string defaultValue;

    public StringPersistentProperty(string name, string defaultValue = "") {
        if (string.IsNullOrEmpty(name)) {
            Debug.LogWarning("empty name of persistent property");
            SetValue(defaultValue);
        } else {
            this.name = name;
            SetValue(EzPrefs.GetString(name, defaultValue));
        }
    }

    protected override void SetValue(string value) {
        base.SetValue(value);
        if (!string.IsNullOrEmpty(name)) {
            EzPrefs.SetString(name, value);
        }
    }
}


[System.Serializable]
public class Vector2PersistentProperty : Vector2ObservableProperty {

    private string name;
    private Vector2 defaultValue;

    public Vector2PersistentProperty(string name, Vector2 defaultValue = default(Vector2)) {
        if (string.IsNullOrEmpty(name)) {
            Debug.LogWarning("empty name of persistent property");
            SetValue(defaultValue);
        } else {
            this.name = name;
            SetValue(EzPrefs.GetVector2(name, defaultValue));
        }
    }

    protected override void SetValue(Vector2 value) {
        base.SetValue(value);
        if (!string.IsNullOrEmpty(name)) {
            EzPrefs.SetVector2(name, value);
        }
    }
}


[System.Serializable]
public class Vector3PersistentProperty : Vector3ObservableProperty {

    private string name;
    private Vector3 defaultValue;

    public Vector3PersistentProperty(string name, Vector3 defaultValue = default(Vector3)) {
        if (string.IsNullOrEmpty(name)) {
            Debug.LogWarning("empty name of persistent property");
            SetValue(defaultValue);
        } else {
            this.name = name;
            SetValue(EzPrefs.GetVector3(name, defaultValue));
        }
    }

    protected override void SetValue(Vector3 value) {
        base.SetValue(value);
        if (!string.IsNullOrEmpty(name)) {
            EzPrefs.SetVector3(name, value);
        }
    }
}


[System.Serializable]
public class ColorPersistentProperty : ColorObservableProperty {

    private string name;
    private Color defaultValue;

    public ColorPersistentProperty(string name, Color defaultValue = default(Color)) {
        if (string.IsNullOrEmpty(name)) {
            Debug.LogWarning("empty name of persistent property");
            SetValue(defaultValue);
        } else {
            this.name = name;
            SetValue(EzPrefs.GetColor(name, defaultValue));
        }
    }

    protected override void SetValue(Color value) {
        base.SetValue(value);
        if (!string.IsNullOrEmpty(name)) {
            EzPrefs.SetColor(name, value);
        }
    }
}


[System.Serializable]
public class RectPersistentProperty : RectObservableProperty {

    private string name;
    private Rect defaultValue;

    public RectPersistentProperty(string name, Rect defaultValue = default(Rect)) {
        if (string.IsNullOrEmpty(name)) {
            Debug.LogWarning("empty name of persistent property");
            SetValue(defaultValue);
        } else {
            this.name = name;
            SetValue(EzPrefs.GetRect(name, defaultValue));
        }
    }

    protected override void SetValue(Rect value) {
        base.SetValue(value);
        if (!string.IsNullOrEmpty(name)) {
            EzPrefs.SetRect(name, value);
        }
    }
}


[System.Serializable]
public class BoundsPersistentProperty : BoundsObservableProperty {

    private string name;
    private Bounds defaultValue;

    public BoundsPersistentProperty(string name, Bounds defaultValue = default(Bounds)) {
        if (string.IsNullOrEmpty(name)) {
            Debug.LogWarning("empty name of persistent property");
            SetValue(defaultValue);
        } else {
            this.name = name;
            SetValue(EzPrefs.GetBounds(name, defaultValue));
        }
    }

    protected override void SetValue(Bounds value) {
        base.SetValue(value);
        if (!string.IsNullOrEmpty(name)) {
            EzPrefs.SetBounds(name, value);
        }
    }
}
