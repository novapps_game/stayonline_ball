﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class ObservableProperty<T> {

    public T Value {
        get {
            return value;
        }
        set {
            if (!equalityComparer.Equals(this.value, value)) {
                SetValue(value);
                OnValueChanged(value);
            }
        }
    }
    public T ForceChangedValue {
        get {
            return value;
        }
        set {
            SetValue(value);
            OnValueChanged(value);
        }
    }
    [SerializeField, GetSet("ForceChangedValue")]
    private T value = default(T);

    static readonly IEqualityComparer<T> defaultEqualityComparer = EqualityComparer<T>.Default;

    protected virtual IEqualityComparer<T> equalityComparer { get { return defaultEqualityComparer; } }

    public class ValueChangeEvent : UnityEvent<T> { }

    static readonly UnityEvent<T> defaultValueChangeEvent = new ValueChangeEvent();

    protected virtual UnityEvent<T> valueChangeEvent { get { return defaultValueChangeEvent; } }

    public ObservableProperty() : this(default(T)) {
    }

    public ObservableProperty(T initialValue) {
        SetValue(initialValue);
    }

    public static implicit operator T(ObservableProperty<T> v) {
        return v.Value;
    }

    protected virtual void SetValue(T value) {
        this.value = value;
    }

    protected virtual void OnValueChanged(T value) {
        valueChangeEvent.Invoke(value);
    }

    public void AddObserver(UnityAction<T> observer) {
        valueChangeEvent.AddListener(observer);
    }

    public void RemoveObserver(UnityAction<T> observer) {
        valueChangeEvent.RemoveListener(observer);
    }

    public void RemoveAllObservers() {
        valueChangeEvent.RemoveAllListeners();
    }
}


[System.Serializable]
public class BoolObservableProperty : ObservableProperty<bool> {

    [System.Serializable]
    public class BoolValueChangeEvent : UnityEvent<bool> { }

    [SerializeField]
    private BoolValueChangeEvent onValueChanged = new BoolValueChangeEvent();

    protected override UnityEvent<bool> valueChangeEvent { get { return onValueChanged; } }
}


[System.Serializable]
public class IntObservableProperty : ObservableProperty<int> {

    [System.Serializable]
    public class IntValueChangeEvent : UnityEvent<int> { }

    [SerializeField]
    private IntValueChangeEvent onValueChanged = new IntValueChangeEvent();

    protected override UnityEvent<int> valueChangeEvent { get { return onValueChanged; } }
}


[System.Serializable]
public class FloatObservableProperty : ObservableProperty<float> {

    [System.Serializable]
    public class FloatValueChangeEvent : UnityEvent<float> { }

    [SerializeField]
    private FloatValueChangeEvent onValueChanged = new FloatValueChangeEvent();

    protected override UnityEvent<float> valueChangeEvent { get { return onValueChanged; } }
}


[System.Serializable]
public class LongObservableProperty : ObservableProperty<long> {

    [System.Serializable]
    public class LongValueChangeEvent : UnityEvent<long> { }

    [SerializeField]
    private LongValueChangeEvent onValueChanged = new LongValueChangeEvent();

    protected override UnityEvent<long> valueChangeEvent { get { return onValueChanged; } }
}


[System.Serializable]
public class DoubleObservableProperty : ObservableProperty<double> {

    [System.Serializable]
    public class DoubleValueChangeEvent : UnityEvent<double> { }

    [SerializeField]
    private DoubleValueChangeEvent onValueChanged = new DoubleValueChangeEvent();

    protected override UnityEvent<double> valueChangeEvent { get { return onValueChanged; } }
}


[System.Serializable]
public class StringObservableProperty : ObservableProperty<string> {

    [System.Serializable]
    public class StringValueChangeEvent : UnityEvent<string> { }

    [SerializeField]
    private StringValueChangeEvent onValueChanged = new StringValueChangeEvent();

    protected override UnityEvent<string> valueChangeEvent { get { return onValueChanged; } }
}


[System.Serializable]
public class Vector2ObservableProperty : ObservableProperty<Vector2> {

    static readonly Vector2EqualityComparer vector2EqualityComparer = new Vector2EqualityComparer();

    protected override IEqualityComparer<Vector2> equalityComparer { get { return vector2EqualityComparer; } }

    [System.Serializable]
    public class Vector2ValueChangeEvent : UnityEvent<Vector2> { }

    [SerializeField]
    private Vector2ValueChangeEvent onValueChanged = new Vector2ValueChangeEvent();

    protected override UnityEvent<Vector2> valueChangeEvent { get { return onValueChanged; } }
}


[System.Serializable]
public class Vector3ObservableProperty : ObservableProperty<Vector3> {

    static readonly Vector3EqualityComparer vector3EqualityComparer = new Vector3EqualityComparer();

    protected override IEqualityComparer<Vector3> equalityComparer { get { return vector3EqualityComparer; } }

    [System.Serializable]
    public class Vector3ValueChangeEvent : UnityEvent<Vector3> { }

    [SerializeField]
    private Vector3ValueChangeEvent onValueChanged = new Vector3ValueChangeEvent();

    protected override UnityEvent<Vector3> valueChangeEvent { get { return onValueChanged; } }
}


[System.Serializable]
public class Vector4ObservableProperty : ObservableProperty<Vector4> {

    static readonly Vector4EqualityComparer vector4EqualityComparer = new Vector4EqualityComparer();

    protected override IEqualityComparer<Vector4> equalityComparer { get { return vector4EqualityComparer; } }

    [System.Serializable]
    public class Vector4ValueChangeEvent : UnityEvent<Vector4> { }

    [SerializeField]
    private Vector4ValueChangeEvent onValueChanged = new Vector4ValueChangeEvent();

    protected override UnityEvent<Vector4> valueChangeEvent { get { return onValueChanged; } }
}


[System.Serializable]
public class ColorObservableProperty : ObservableProperty<Color> {

    static readonly ColorEqualityComparer colorEqualityComparer = new ColorEqualityComparer();

    protected override IEqualityComparer<Color> equalityComparer { get { return colorEqualityComparer; } }

    [System.Serializable]
    public class ColorValueChangeEvent : UnityEvent<Color> { }

    [SerializeField]
    private ColorValueChangeEvent onValueChanged = new ColorValueChangeEvent();

    protected override UnityEvent<Color> valueChangeEvent { get { return onValueChanged; } }
}


[System.Serializable]
public class RectObservableProperty : ObservableProperty<Rect> {

    static readonly RectEqualityComparer rectEqualityComparer = new RectEqualityComparer();

    protected override IEqualityComparer<Rect> equalityComparer { get { return rectEqualityComparer; } }

    [System.Serializable]
    public class RectValueChangeEvent : UnityEvent<Rect> { }

    [SerializeField]
    private RectValueChangeEvent onValueChanged = new RectValueChangeEvent();

    protected override UnityEvent<Rect> valueChangeEvent { get { return onValueChanged; } }
}


[System.Serializable]
public class BoundsObservableProperty : ObservableProperty<Bounds> {

    static readonly BoundsEqualityComparer boundsEqualityComparer = new BoundsEqualityComparer();

    protected override IEqualityComparer<Bounds> equalityComparer { get { return boundsEqualityComparer; } }

    [System.Serializable]
    public class BoundsValueChangeEvent : UnityEvent<Bounds> { }

    [SerializeField]
    private BoundsValueChangeEvent onValueChanged = new BoundsValueChangeEvent();

    protected override UnityEvent<Bounds> valueChangeEvent { get { return onValueChanged; } }
}


[System.Serializable]
public class QuaternionObservableProperty : ObservableProperty<Quaternion> {

    static readonly QuaternionEqualityComparer quaternionEqualityComparer = new QuaternionEqualityComparer();

    protected override IEqualityComparer<Quaternion> equalityComparer { get { return quaternionEqualityComparer; } }

    [System.Serializable]
    public class QuaternionValueChangeEvent : UnityEvent<Quaternion> { }

    [SerializeField]
    private QuaternionValueChangeEvent onValueChanged = new QuaternionValueChangeEvent();

    protected override UnityEvent<Quaternion> valueChangeEvent { get { return onValueChanged; } }
}


[System.Serializable]
public class Color32ObservableProperty : ObservableProperty<Color32> {

    static readonly Color32EqualityComparer color32EqualityComparer = new Color32EqualityComparer();

    protected override IEqualityComparer<Color32> equalityComparer { get { return color32EqualityComparer; } }

    [System.Serializable]
    public class Color32ValueChangeEvent : UnityEvent<Color32> { }

    [SerializeField]
    private Color32ValueChangeEvent onValueChanged = new Color32ValueChangeEvent();

    protected override UnityEvent<Color32> valueChangeEvent { get { return onValueChanged; } }
}


[System.Serializable]
public class SpriteObservableProperty : ObservableProperty<Sprite> {

    [System.Serializable]
    public class SpriteValueChangeEvent : UnityEvent<Sprite> { }

    [SerializeField]
    private SpriteValueChangeEvent onValueChanged = new SpriteValueChangeEvent();

    protected override UnityEvent<Sprite> valueChangeEvent { get { return onValueChanged; } }
}


#if UNITY_2017_2_OR_NEWER


[System.Serializable]
public class Vector2IntObservableProperty : ObservableProperty<Vector2Int> {

    static readonly Vector2IntEqualityComparer vector2IntEqualityComparer = new Vector2IntEqualityComparer();

    protected override IEqualityComparer<Vector2Int> equalityComparer { get { return vector2IntEqualityComparer; } }

    [System.Serializable]
    public class Vector2IntValueChangeEvent : UnityEvent<Vector2Int> { }

    [SerializeField]
    private Vector2IntValueChangeEvent onValueChanged = new Vector2IntValueChangeEvent();

    protected override UnityEvent<Vector2Int> valueChangeEvent { get { return onValueChanged; } }
}


[System.Serializable]
public class Vector3IntObservableProperty : ObservableProperty<Vector3Int> {

    static readonly Vector3IntEqualityComparer vector3IntEqualityComparer = new Vector3IntEqualityComparer();

    protected override IEqualityComparer<Vector3Int> equalityComparer { get { return vector3IntEqualityComparer; } }

    [System.Serializable]
    public class Vector3IntValueChangeEvent : UnityEvent<Vector3Int> { }

    [SerializeField]
    private Vector3IntValueChangeEvent onValueChanged = new Vector3IntValueChangeEvent();

    protected override UnityEvent<Vector3Int> valueChangeEvent { get { return onValueChanged; } }
}


[System.Serializable]
public class RangeIntObservableProperty : ObservableProperty<RangeInt> {

    static readonly RangeIntEqualityComparer rangeIntEqualityComparer = new RangeIntEqualityComparer();

    protected override IEqualityComparer<RangeInt> equalityComparer { get { return rangeIntEqualityComparer; } }

    [System.Serializable]
    public class RangeIntValueChangeEvent : UnityEvent<RangeInt> { }

    [SerializeField]
    private RangeIntValueChangeEvent onValueChanged = new RangeIntValueChangeEvent();

    protected override UnityEvent<RangeInt> valueChangeEvent { get { return onValueChanged; } }
}


[System.Serializable]
public class RectIntObservableProperty : ObservableProperty<RectInt> {

    static readonly RectIntEqualityComparer rectIntEqualityComparer = new RectIntEqualityComparer();

    protected override IEqualityComparer<RectInt> equalityComparer { get { return rectIntEqualityComparer; } }

    [System.Serializable]
    public class RectIntValueChangeEvent : UnityEvent<RectInt> { }

    [SerializeField]
    private RectIntValueChangeEvent onValueChanged = new RectIntValueChangeEvent();

    protected override UnityEvent<RectInt> valueChangeEvent { get { return onValueChanged; } }
}


[System.Serializable]
public class BoundsIntObservableProperty : ObservableProperty<BoundsInt> {

    static readonly BoundsIntEqualityComparer boundsIntEqualityComparer = new BoundsIntEqualityComparer();

    protected override IEqualityComparer<BoundsInt> equalityComparer { get { return boundsIntEqualityComparer; } }

    [System.Serializable]
    public class BoundsIntValueChangeEvent : UnityEvent<BoundsInt> { }

    [SerializeField]
    private BoundsIntValueChangeEvent onValueChanged = new BoundsIntValueChangeEvent();

    protected override UnityEvent<BoundsInt> valueChangeEvent { get { return onValueChanged; } }
}

#endif
