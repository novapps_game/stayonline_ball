﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EzRandom : MonoBehaviour {

    public static bool whether { get { return Random.Range(0, 2) > 0; } }

    public static float sign { get { return whether ? 1 : -1; } }

    public static float Near(float radius, float center = 0) {
        return Random.Range(center - radius, center + radius);
    }

    public static int Near(int radius, int center = 0) {
        return Random.Range(center - radius, center + radius + 1);
    }

    public static T Select<T>(T[] array) {
        return array[Random.Range(0, array.Length)];
    }

    public static T Select<T>(IList<T> array) {
        return array[Random.Range(0, array.Count)];
    }

    public static T Pick<T>(IList<T> list) {
        if (list.Count <= 0) return default(T);
        int index = Random.Range(0, list.Count);
        T value = list[index];
        list.RemoveAt(index);
        return value;
    }

    // return random permutation of [from, to)
    public static int[] Perm(int from, int to) {
        int[] perm = new int[to - from];
        for (int i = 0, j = from; j < to; ++i, ++j) {
            perm[i] = j;
        }
        System.Array.Sort(perm, new RandomComparer<int>());
        return perm;
    }

    public static int Roll(float[] chances, bool takeAway = false) {
        float result = 0;
        return GetRoll(chances, out result, takeAway);
    }

    public static int GetRoll(float[] chances, out float result, bool takeAway = false) {
        Vector2[] sections = new Vector2[chances.Length];
        float min = 0f, max = 0f;
        for (int i = 0; i < chances.Length; ++i) {
            max += chances[i];
            sections[i] = new Vector2(min, max);
            min = max;
        }
        result = Random.Range(0, max - Mathf.Epsilon);
        for (int j = 0; j < sections.Length; ++j) {
            if (result >= sections[j].x && result < sections[j].y) {
                if (takeAway) {
                    chances[j] = 0;
                }
                return j;
            }
        }
        return -1;
    }
    static System.Random r = new System.Random();
    public static void Shuffle<T>(IList<T> deck) {
        for (int n = deck.Count - 1; n > 0; --n) {
            int k = r.Next(n + 1);
            T temp = deck[n];
            deck[n] = deck[k];
            deck[k] = temp;
        }
    }

    public static Vector2 Position(Vector2 min, Vector2 max) {
        return new Vector2(
            Random.Range(min.x, max.x), 
            Random.Range(min.y, max.y));
    }

    public static Vector3 Position(Vector3 min, Vector3 max) {
        return new Vector3(
            Random.Range(min.x, max.x),
            Random.Range(min.y, max.y),
            Random.Range(min.z, max.z));
    }

    public static Vector2 Position(Vector2 min, Vector2 max, int cols, int rows) {
        Vector2 size = max - min;
        Vector2 cellSize = new Vector2(size.x / cols, size.y / rows);
        int col = Random.Range(0, cols);
        int row = Random.Range(0, rows);
        return new Vector2(min.x + cellSize.x * col, min.y + cellSize.y * row);
    }

    public static Vector3 Position(Vector3 min, Vector3 max, int cols, int layers, int rows) {
        Vector3 size = max - min;
        Vector3 cellSize = new Vector3(size.x / cols, size.y / layers, size.z / rows);
        int col = Random.Range(0, cols);
        int layer = Random.Range(0, layers);
        int row = Random.Range(0, rows); 
        return new Vector3(min.x + cellSize.x * col, min.y + cellSize.y * layer, min.z + cellSize.z * row);
    }
}
