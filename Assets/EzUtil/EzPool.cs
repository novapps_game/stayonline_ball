﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EzPool<T, P> : Singleton<P> where T : Component where P : Component {

    public GameObject prefab;
    public Transform rootTransform;
    public int maxSize = 0;

    protected Transform root { get { return rootTransform != null ? rootTransform : transform; } }

    public virtual T Create() {
        if (!gameObject.activeSelf) {
            return null;
        }
        if (prefab == null) {
            prefab = root.GetChild(0).gameObject;
        }
        Transform child = root.FindFirstInactiveChild();
        if (child == null) {
            if (maxSize <= 0 || root.childCount < maxSize) {
                child = Instantiate(prefab).transform;
                child.gameObject.name = prefab.name;
                child.SetParent(root, false);
            }
        }
        if (child != null) {
            child.gameObject.SetActive(true);
            T comp = child.GetComponent<T>();
            if (comp == null) {
                comp = child.gameObject.AddComponent<T>();
            }
            return comp;
        }
        return null;
    }

    public virtual void Recycle(T comp) {
        comp.gameObject.SetActive(false);
        comp.transform.SetParent(root);
    }

    public virtual void RecycleAll() {
        foreach (Transform child in root) {
            if (child.gameObject.activeSelf) {
                T comp = child.GetComponent<T>();
                Recycle(comp);
            }
        }
    }
}
