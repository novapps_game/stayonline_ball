﻿using UnityEngine;
using System.Collections;

public class HSVColor {

    public int h;
    public int s;
    public int v;

    public const int minH = 0;
    public const int maxH = 359;
    public const int minS = 0;
    public const int maxS = 255;
    public const int minV = 0;
    public const int maxV = 255;

    public Color rgb {
        get {
            return ToRGB();
        }
        set {
            Set(value);
        }
    }

    public float H {
        get { return ((float)h) / maxH; }
        set { h = Mathf.RoundToInt(value * maxH); }
    }
    public float S {
        get { return ((float)s) / maxS; }
        set { s = Mathf.RoundToInt(value * maxS); }
    }
    public float V {
        get { return ((float)v) / maxV; }
        set { v = Mathf.RoundToInt(value * maxV); }
    }

    private int signH = 1;
    private int signS = 1;
    private int signV = 1;

    public HSVColor() { }

    public HSVColor(int h, int s, int v) {
        Set(h, s, v);
    }

    public HSVColor(Color color) {
        Set(color);
    }

    public HSVColor(HSVColor color) {
        Set(color);
    }

    public override string ToString() {
        return "(" + h + ", " + s + ", " + v + ")";
    }

    public void Set(int h, int s, int v) {
        this.h = h;
        this.s = s;
        this.v = v;
    }

    public void Set(Color color) {
        float fH, fS, fV;
        Color.RGBToHSV(color, out fH, out fS, out fV);
        H = fH;
        S = fS;
        V = fV;
    }

    public void Set(HSVColor color) {
        Set(color.h, color.s, color.v);
    }

    public Color ToRGB() {
        return Color.HSVToRGB(H, S, V);
    }

    public void LoopH(int delta, int min = minH, int max = maxH) {
        h += delta;
        int size = max - min;
        while (h > max) h -= size;
        while (h < min) h += size;
    }

    public void PingpongH(int delta, int min = minH, int max = maxH) {
        int size = max - min;
        signH = ((delta / size) % 2 == 0) ? 1 : -1;
        delta = delta % size;
        h += signH * Mathf.Abs(delta);
    }

    public void LoopS(int delta, int min = minS, int max = maxS) {
        s += delta;
        int size = max - min;
        while (s > max) s -= size;
        while (s < min) s += size;
    }

    public void PingpongS(int delta, int min = minS, int max = maxS) {
        int size = max - min;
        signS = ((delta / size) % 2 == 0) ? 1 : -1;
        delta = delta % size;
        s += signS * Mathf.Abs(delta);
    }

    public void LoopV(int delta, int min = minV, int max = maxV) {
        v += delta;
        int size = max - min;
        while (v > max) v -= size;
        while (v < min) v += size;
    }

    public void PingpongV(int delta, int min = minV, int max = maxV) {
        int size = max - min;
        signV = ((delta / size) % 2 == 0) ? 1 : -1;
        delta = delta % size;
        v += signV * Mathf.Abs(delta);
    }

}
