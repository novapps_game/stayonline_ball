﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EzSpawner<T, P> : EzPool<T, P> where T : MonoBehaviour where P : Component {

    public float delay;
    public float interval = 1f;
    public float lifetime = -1;
    public int minSpawnCount = 1;
    public int maxSpawnCount = 1;
    public int spawnCount { get { return Random.Range(minSpawnCount, maxSpawnCount + 1); } }
    public int maxCount = -1;
    public float probability = 1f;
    public bool spawnOnStart = true;
    public bool running = false;
    public bool paused = false;

    protected int count = 0;
    protected float spawnTime;
    protected float elapsedTime;

    protected virtual void Start() {
        Reset();
    }

    protected virtual void Update() {
        if (running) {
            if (elapsedTime < spawnTime) {
                elapsedTime += Time.deltaTime;
            } else if (TrySpawn()) {
                elapsedTime -= spawnTime;
                spawnTime = interval;
            }
        }
    }

    protected bool TrySpawn() {
        if (paused) {
            return false;
        }
        for (int i = 0; i < spawnCount; ++i) {
            if (CanSpawn()) {
                Spawn();
            }
        }
        return true;
    }

    public virtual T Spawn() {
        T obj = Create();
        if (obj != null) {
            ++count;
            OnSpawn(obj);
            if (lifetime > 0) {
                obj.Invoke("Recycle", lifetime);
            }
        }
        return obj;
    }

    protected virtual bool CanSpawn() {
        return (maxCount <= 0 || count < maxCount) && (Random.value < probability);
    }

    protected virtual void OnSpawn(T obj) { }

    public virtual void Reset() {
        spawnTime = delay;
        running = spawnOnStart;
        count = 0;
        elapsedTime = 0;
    }

    public override void Recycle(T obj) {
        obj.CancelInvoke();
        --count;
        base.Recycle(obj);
    }

}
