﻿using System;

public abstract class TweenProperty {

	public bool Initialized { get; protected set; }

	protected bool relative;
	protected EzTween owner;

	protected Func<float, float, float, float, float> easeFunction;

	public TweenProperty(bool relative) {
		this.relative = relative;
	}

	public override int GetHashCode() {
		return base.GetHashCode();
	}

	public override string ToString() {
		return GetType().Name;
	}

	/// <summary>
	/// checks to see if a TweenProperty matches another. checks propertyNames of IGenericPropertys first then
	/// resorts to direct type checking
	/// </summary>
	public override bool Equals(object other) {
		// null check first
		if (other == null) {
			return false;
		}
		if (ToString() == other.ToString()) {
			return true;
		}
		return base.Equals(other);
	}

	/// <summary>
	/// clones the instance
	/// </summary>
	public TweenProperty Clone() {
		var clone = MemberwiseClone() as TweenProperty;
		clone.owner = null;
		clone.Initialized = false;
		clone.easeFunction = null;
		return clone;
	}

	/// <summary>
	/// called by a Tween just after this property is validated and added to the Tweens property list
	/// </summary>
	public virtual void Init(EzTween owner) {
		this.owner = owner;
		Initialized = true;

		// if we dont have an easeFunction use the owners type
		if (easeFunction == null) {
			SetEaseType(owner.easeType);
		}
	}

	/// <summary>
	/// sets the ease type for this tween property
	/// technically, this should be an internal method
	/// </summary>
	public void SetEaseType(EzEaseType easeType) {
		easeFunction = easeType.GetFunction();
	}

	public abstract bool ValidateTarget(object target);

	/// <summary>
	/// called when a Tween is initially started.
	/// subclasses should strongly type the start/end/target and handle isFrom with
	/// regard to setting the proper start/end values
	/// </summary>
	public abstract void Start();

	/// <summary>
	/// subclasses should get the eased time then set the new value on the object
	/// </summary>
	public abstract void Tick(float elapsed);
}
