using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public enum EzUpdateType {
    Update,
    LateUpdate,
    FixedUpdate,
    TimeScaleIndependentUpdate
}

public enum EzLoopType {
    RestartFromBeginning,
    PingPong
}

/// <summary>
/// base class shared by the Tween and TweenChain classes to allow a seemless API when controlling
/// either of them
/// </summary>
public abstract class EzTweenBase {
    public enum State {
        Running,
        Paused,
        Completed,
        Destroyed
    }

    public int id = 0; // optional id used for identifying this tween
    public State state { get; protected set; } // current state of the tween
    public float duration { get; protected set; } // duration for a single loop
    public float totalDuration { get; protected set; } // duration for all loops of this tween
    public float timeScale { get; set; } // time scale to be used by this tween

    public EzUpdateType updateType { get; protected set; }
    public EzLoopType loopType { get; protected set; }
    public int loops { get; protected set; } // set to -1 for infinite
    public int completedLoops { get; protected set; }

    public bool autoRemoveOnComplete { get; set; } // should we automatically remove ourself from the Go's list of tweens when done?
    public bool reversed { get; protected set; } // have we been reversed? this is different than a PingPong loop's backwards section
    public bool allowEvents { get; set; } // allow the user to surpress events.
    protected bool initilized; // flag to ensure event only gets fired once
    protected bool started; // flag to ensure event only gets fired once
    protected bool fireLoopBegin;
    protected bool fireLoopEnd;

    // internal state for update logic
    protected float elapsedTime; // elapsed time for the current loop iteration
    public float totalElapsedTime { get; protected set; } // total elapsed time of the entire tween

    public bool isLoopingBackOnPingPong { get; protected set; }

    protected bool didIterateLastFrame;
    protected bool didIterateThisFrame;
    protected int deltaLoops; // change in completed iterations this frame.

    // action event handlers
    protected Action<EzTweenBase> onInit; // executes before initial setup.
    protected Action<EzTweenBase> onStart; // executes when a tween starts.
    protected Action<EzTweenBase> onUpdate; // execute whenever a tween updates.
    protected Action<EzTweenBase> onComplete; // exectures whenever a tween completes
    protected Action<EzTweenBase> onLoopBegin; // executes whenever a tween starts an iteration.
    protected Action<EzTweenBase> onLoopEnd; // executes whenever a tween ends an iteration.

    public void OnInit(Action<EzTweenBase> action) {
        onInit = action;
    }

    public void OnStart(Action<EzTweenBase> action) {
        onStart = action;
    }

    public void OnUpdate(Action<EzTweenBase> action) {
        onUpdate = action;
    }

    public void OnComplete(Action<EzTweenBase> action) {
        onComplete = action;
    }

    public void OnLoopBegin(Action<EzTweenBase> action) {
        onLoopBegin = action;
    }

    public void OnLoopEnd(Action<EzTweenBase> action) {
        onLoopEnd = action;
    }

    /// <summary>
    /// called once per tween when it is first updated
    /// </summary>
    protected virtual void OnInit() {
        if (!allowEvents)
            return;

        if (onInit != null)
            onInit(this);

        initilized = true;
    }

    /// <summary>
    /// called whenever the tween is updated and the playhead is at the start (or end, depending on isReversed) of the tween.
    /// </summary>
    protected virtual void OnStart() {
        if (!allowEvents)
            return;

        if (reversed && totalElapsedTime != totalDuration)
            return;
        else if (!reversed && totalElapsedTime != 0f)
            return;

        if (onStart != null)
            onStart(this);

        started = true;
    }


    /// <summary>
    /// called once per iteration at the start of the iteration.
    /// </summary>
    protected virtual void OnLoopBegin() {
        if (!allowEvents)
            return;

        if (onLoopBegin != null)
            onLoopBegin(this);
    }

    /// <summary>
    /// called once per update, after the update has occured.
    /// </summary>
    protected virtual void OnUpdate() {
        if (!allowEvents)
            return;

        if (onUpdate != null)
            onUpdate(this);
    }

    /// <summary>
    /// called once per iteration at the end of the iteration.
    /// </summary>
    protected virtual void OnLoopEnd() {
        if (!allowEvents)
            return;

        if (onLoopEnd != null)
            onLoopEnd(this);
    }


    /// <summary>
    /// called when the tween completes playing.
    /// </summary>
    protected virtual void OnComplete() {
        if (!allowEvents)
            return;

        if (onComplete != null)
            onComplete(this);
    }


    /// <summary>
    /// tick method. if it returns true it indicates the tween is complete.
    ///   note: at it's base, AbstractGoTween does not fire events, it is up to the implementer to
    ///         do so. see GoTween and AbstractGoTweenCollection for examples.
    /// </summary>
    public virtual bool Tick(float deltaTime) {
        // increment or decrement the total elapsed time then clamp from 0 to totalDuration
        if (reversed)
            totalElapsedTime -= deltaTime;
        else
            totalElapsedTime += deltaTime;

        totalElapsedTime = Mathf.Clamp(totalElapsedTime, 0, totalDuration);

        didIterateLastFrame = didIterateThisFrame || 
            (!reversed && totalElapsedTime == 0) || 
            (reversed && totalElapsedTime == totalDuration);

        // we flip between ceil and floor based on the direction, because we want the iteration change
        // to happen when "duration" seconds has elapsed, not immediately, as was the case if you
        // were doing a floor and were going in reverse.
        if (reversed)
            deltaLoops = Mathf.CeilToInt(totalElapsedTime / duration) - completedLoops;
        else
            deltaLoops = Mathf.FloorToInt(totalElapsedTime / duration) - completedLoops;

        // we iterated this frame if we have done a goTo() to an iteration point, or we've passed over
        // an iteration threshold.
        didIterateThisFrame = !didIterateLastFrame && (deltaLoops != 0f || totalElapsedTime % duration == 0f);

        completedLoops += deltaLoops;

        // set the elapsedTime, given what we know.
        if (didIterateLastFrame) {
            elapsedTime = reversed ? duration : 0f;
        } else if (didIterateThisFrame) {
            // if we iterated this frame, we force the _elapsedTime to the end of the timeline.
            elapsedTime = reversed ? 0f : duration;
        } else {
            elapsedTime = totalElapsedTime % duration;

            // if you do a goTo(x) where x is a multiple of duration, we assume that you want
            // to be at the end of your duration, as this sets you up to have an automatic OnIterationStart fire
            // the next updated frame. the only caveat is when you do a goTo(0) when playing forwards,
            // or a goTo(totalDuration) when playing in reverse. we assume that at that point, you want to be
            // at the start of your tween.
            if (elapsedTime == 0f && ((reversed && totalElapsedTime == totalDuration) || (!reversed && totalElapsedTime > 0f))) {
                elapsedTime = duration;
            }
        }

        // we can only be looping back on a PingPong if our loopType is PingPong and we are on an odd numbered iteration
        isLoopingBackOnPingPong = false;
        if (loopType == EzLoopType.PingPong) {
            // due to the way that we count iterations, and force a tween to remain at the end
            // of it's timeline for one frame after passing the duration threshold,
            // we need to make sure that _isLoopingBackOnPingPong references the current
            // iteration, and not the next one.
            if (reversed) {
                isLoopingBackOnPingPong = completedLoops % 2 == 0;
                if (elapsedTime == 0f) {
                    isLoopingBackOnPingPong = !isLoopingBackOnPingPong;
                }
            } else {
                isLoopingBackOnPingPong = completedLoops % 2 != 0;
                if (elapsedTime == duration) {
                    isLoopingBackOnPingPong = !isLoopingBackOnPingPong;
                }
            }
        }

        // set a flag whether to fire the onIterationEnd event or not.
        fireLoopBegin = didIterateThisFrame || (!reversed && elapsedTime == duration) || (reversed && elapsedTime == 0f);
        fireLoopEnd = didIterateThisFrame;

        // check for completion
        if ((!reversed && loops >= 0 && completedLoops >= loops) || (reversed && totalElapsedTime <= 0))
            state = State.Completed;

        if (state == State.Completed) {
            // these variables need to be reset here. if a tween were to complete, 
            // and then get played again:
            // * The onIterationStart event would get fired
            // * tweens would flip their elapsedTime between 0 and totalDuration

            fireLoopBegin = false;
            didIterateThisFrame = false;

            return true; // true if complete
        }

        return false; // false if not complete
    }


    /// <summary>
    /// subclasses should return true if they are a valid and ready to be added to the list of running tweens
    /// or false if not ready.
    /// technically, this should be marked as internal
    /// </summary>
    public abstract bool IsValid();


    /// <summary>
    /// attempts to remove the tween property returning true if successful
    /// technically, this should be marked as internal
    /// </summary>
    public abstract bool RemoveTweenProperty(TweenProperty property);


    /// <summary>
    /// returns true if the tween contains the same type (or propertyName) property in its property list
    /// technically, this should be marked as internal
    /// </summary>
    public abstract bool ContainsTweenProperty(TweenProperty property);


    /// <summary>
    /// returns a list of all the TweenProperties contained in the tween and all its children (if it is
    /// a TweenChain or a TweenFlow)
    /// technically, this should be marked as internal
    /// </summary>
    public abstract List<TweenProperty> AllTweenProperties();


    /// <summary>
    /// removes the Tween from action and cleans up its state
    /// </summary>
    public virtual void Destroy() {
        state = State.Destroyed;
    }


    /// <summary>
    /// pauses playback
    /// </summary>
    public virtual void Pause() {
        state = State.Paused;
    }


    /// <summary>
    /// resumes playback
    /// </summary>
    public virtual void Play() {
        state = State.Running;
    }


    /// <summary>
    /// plays the tween forward. if it is already playing forward has no effect
    /// </summary>
    public void PlayForward() {
        if (reversed)
            Reverse();

        Play();
    }


    /// <summary>
    /// plays the tween backwards. if it is already playing backwards has no effect
    /// </summary>
    public void PlayBackward() {
        if (!reversed)
            Reverse();

        Play();
    }


    /// <summary>
    /// resets the tween to the beginning, taking isReversed into account.
    /// </summary>
    protected virtual void Reset(bool skipDelay = true) {
        GoTo(reversed ? totalDuration : 0, skipDelay);

        fireLoopBegin = true;
    }


    /// <summary>
    /// rewinds the tween to the beginning (or end, depending on isReversed) and pauses playback.
    /// </summary>
    public virtual void Rewind(bool skipDelay = true) {
        Reset(skipDelay);
        Pause();
    }


    /// <summary>
    /// rewinds the tween to the beginning (or end, depending on isReversed) and starts playback,
    /// optionally skipping delay (only relevant for Tweens).
    /// </summary>
    public void Restart(bool skipDelay = true) {
        Reset(skipDelay);
        Play();
    }


    /// <summary>
    /// reverses playback. if going forward it will be going backward after this and vice versa.
    /// </summary>
    public virtual void Reverse() {
        reversed = !reversed;

        completedLoops = reversed ? Mathf.CeilToInt(totalElapsedTime / duration) : Mathf.FloorToInt(totalElapsedTime / duration);

        // if we are at the "start" of the timeline, based on isReversed,
        // allow the onBegin/onIterationStart callbacks to fire again.
        if ((reversed && totalElapsedTime == totalDuration) || (!reversed && totalElapsedTime == 0f)) {
            started = false;
            fireLoopBegin = true;
        }
    }


    /// <summary>
    /// completes the tween. sets the playhead to it's final position as if the tween completed normally.
    /// takes into account if the tween was playing forward or reversed.
    /// </summary>
    public virtual void Complete() {
        if (loops < 0)
            return;

        // set full elapsed time and let the next iteration finish it off
        GoTo(reversed ? 0 : totalDuration, true);
    }


    /// <summary>
    /// goes to the specified time clamping it from 0 to the total duration of the tween. if the tween is
    /// not playing it can optionally be force updated to the time specified. delays are not taken into effect.
    /// (must be implemented by inherited classes.)
    /// </summary>
    public abstract void GoTo(float time, bool skipDelay = true);

    /// <summary>
    /// goes to the time and starts playback skipping any delays
    /// </summary>
    public void GoToAndPlay(float time, bool skipDelay = true) {
        GoTo(time, skipDelay);
        Play();
    }


    /// <summary>
    /// waits for either completion or destruction. call in a Coroutine and yield on the return
    /// </summary>
    public IEnumerator WaitForCompletion() {
        while (state != State.Completed && state != State.Destroyed)
            yield return null;

        yield break;
    }
}
