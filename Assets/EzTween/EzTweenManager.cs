using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class EzTweenManager : MonoBehaviour {
    // defaults used for all tweens/properties that are not explicitly set
    public static EzEaseType defaultEaseType = EzEaseType.Linear;
    public static EzLoopType defaultLoopType = EzLoopType.RestartFromBeginning;
    public static EzUpdateType defaultUpdateType = EzUpdateType.Update;

    // defines what we should do in the event that a TweenProperty is added and an already existing tween has the same
    // property and target
    public static EzDuplicatePropertyRuleType duplicatePropertyRule = EzDuplicatePropertyRuleType.None;
    public static EzLogLevel logLevel = EzLogLevel.Warn;

    // validates that the target object still exists each tick of the tween. NOTE: it is recommended
    // that you just properly remove your tweens before destroying any objects even though this might destroy them for you
    public static bool validateTargetObjectsEachTick = true;

    // Used to stop instances being created while the application is quitting
    private static bool applicationIsQuitting = false;

    private static List<EzTweenBase> tweens = new List<EzTweenBase>(); // contains Tweens, TweenChains and TweenFlows
    private bool timeScaleIndependentUpdateIsRunning;

    // only one Go can exist
    private static EzTweenManager instance = null;
    public static EzTweenManager Instance {
        get {
            // Don't allow new instances to be created when the application is quitting to avoid the GOKit object never being destroyed.
            // These dangling instances can't be found with FindObjectOfType and so you'd get multiple instances in a scene.
            if (!instance && !applicationIsQuitting) {
                // check if there is a GO instance already available in the scene graph
                instance = FindObjectOfType<EzTweenManager>();

                // possible Unity bug with FindObjectOfType workaround
                //_instance = FindObjectOfType( typeof( Go ) ) ?? GameObject.Find( "GoKit" ).GetComponent<Go>() as Go;

                // nope, create a new one
                if (!instance) {
                    var obj = new GameObject("EzTweenManager");
                    instance = obj.AddComponent<EzTweenManager>();
                    DontDestroyOnLoad(obj);
                }
            }

            return instance;
        }
    }


    /// <summary>
    /// loops through all the Tweens and updates any that are of updateType. If any Tweens are complete
    /// (the update call will return true) they are removed.
    /// </summary>
    private void HandleUpdateOfType(EzUpdateType updateType, float deltaTime) {
        // loop backwards so we can remove completed tweens
        for (var i = tweens.Count - 1; i >= 0; --i) {
            var t = tweens[i];

            if (t.state == EzTweenBase.State.Destroyed) {
                // destroy method has been called
                RemoveTween(t);
            } else {
                // only process tweens with our update type that are running
                if (t.updateType == updateType && t.state == EzTweenBase.State.Running && t.Tick(deltaTime * t.timeScale)) {
                    // tween is complete if we get here. if destroyed or set to auto remove kill it
                    if (t.state == EzTweenBase.State.Destroyed || t.autoRemoveOnComplete) {
                        RemoveTween(t);
                        t.Destroy();
                    }
                }
            }
        }
    }


    #region MonoBehaviour

    private void Update() {
        if (tweens.Count == 0)
            return;

        HandleUpdateOfType(EzUpdateType.Update, Time.deltaTime);
    }


    private void LateUpdate() {
        if (tweens.Count == 0)
            return;

        HandleUpdateOfType(EzUpdateType.LateUpdate, Time.deltaTime);
    }


    private void FixedUpdate() {
        if (tweens.Count == 0)
            return;

        HandleUpdateOfType(EzUpdateType.FixedUpdate, Time.deltaTime);
    }


    private void OnApplicationQuit() {
        instance = null;
        Destroy(gameObject);
        applicationIsQuitting = true;
    }

    #endregion


    /// <summary>
    /// this only runs as needed and handles time scale independent Tweens
    /// </summary>
    private IEnumerator TimeScaleIndependentUpdate() {
        timeScaleIndependentUpdateIsRunning = true;
        var time = Time.realtimeSinceStartup;

        while (tweens.Count > 0) {
            var elapsed = Time.realtimeSinceStartup - time;
            time = Time.realtimeSinceStartup;

            // update tweens
            HandleUpdateOfType(EzUpdateType.TimeScaleIndependentUpdate, elapsed);

            yield return null;
        }

        timeScaleIndependentUpdateIsRunning = false;
    }


    /// <summary>
    /// checks for duplicate properties. if one is found and the DuplicatePropertyRuleType is set to
    /// DontAddCurrentProperty it will return true indicating that the tween should not be added.
    /// this only checks tweens that are not part of an AbstractTweenCollection
    /// </summary>
    private static bool HandleDuplicatePropertiesInTween(EzTween tween) {
        // first fetch all the current tweens with the same target object as this one
        var allTweensWithTarget = GetTweensByTarget(tween.target);

        // store a list of all the properties in the tween
        var allProperties = tween.AllTweenProperties();

        // TODO: perhaps only perform the check on running Tweens?

        // loop through all the tweens with the same target
        foreach (var tweenWithTarget in allTweensWithTarget) {
            // loop through all the properties in the tween and see if there are any dupes
            foreach (var tweenProp in allProperties) {
                Warn("found duplicate TweenProperty {0} in tween {1}", tweenProp, tween);

                // check for a matched property
                if (tweenWithTarget.ContainsTweenProperty(tweenProp)) {
                    // handle the different duplicate property rules
                    if (duplicatePropertyRule == EzDuplicatePropertyRuleType.DontAddCurrentProperty) {
                        return true;
                    } else if (duplicatePropertyRule == EzDuplicatePropertyRuleType.RemoveRunningProperty) {
                        // TODO: perhaps check if the Tween has any properties left and remove it if it doesnt?
                        tweenWithTarget.RemoveTweenProperty(tweenProp);
                    }

                    return false;
                }
            }
        }

        return false;
    }


    #region Logging

    /// <summary>
    /// logging should only occur in the editor so we use a conditional
    /// </summary>
    [System.Diagnostics.Conditional("UNITY_EDITOR")]
    private static void Log(object format, params object[] paramList) {
        if (format is string)
            Debug.Log(string.Format(format as string, paramList));
        else
            Debug.Log(format);
    }


    [System.Diagnostics.Conditional("UNITY_EDITOR")]
    public static void Warn(object format, params object[] paramList) {
        if (logLevel == EzLogLevel.None || logLevel == EzLogLevel.Info)
            return;

        if (format is string)
            Debug.LogWarning(string.Format(format as string, paramList));
        else
            Debug.LogWarning(format);
    }


    [System.Diagnostics.Conditional("UNITY_EDITOR")]
    public static void Error(object format, params object[] paramList) {
        if (logLevel == EzLogLevel.None || logLevel == EzLogLevel.Info || logLevel == EzLogLevel.Warn)
            return;

        if (format is string)
            Debug.LogError(string.Format(format as string, paramList));
        else
            Debug.LogError(format);
    }

    #endregion


    #region public API

    /// <summary>
    /// helper function that creates a "to" Tween and adds it to the pool
    /// </summary>
    public static EzTween To(object target, float duration, EzTweenConfig config) {
        config.SetFromMode(false);
        var tween = new EzTween(target, duration, config);
        AddTween(tween);
        return tween;
    }


    /// <summary>
    /// helper function that creates a "from" Tween and adds it to the pool
    /// </summary>
    public static EzTween From(object target, float duration, EzTweenConfig config) {
        config.SetFromMode(true);
        var tween = new EzTween(target, duration, config);
        AddTween(tween);
        return tween;
    }


    /// <summary>
    /// adds an AbstractTween (Tween, TweenChain or TweenFlow) to the current list of running Tweens
    /// </summary>
    public static void AddTween(EzTweenBase tween) {
        // early out for invalid items
        if (!tween.IsValid())
            return;

        // dont add the same tween twice
        if (tweens.Contains(tween))
            return;

        // check for dupes and handle them before adding the tween. we only need to check for Tweens
        if (duplicatePropertyRule != EzDuplicatePropertyRuleType.None && tween is EzTween) {
            // if handleDuplicatePropertiesInTween returns true it indicates we should not add this tween
            if (HandleDuplicatePropertiesInTween(tween as EzTween))
                return;

            // if we became invalid after handling dupes dont add the tween
            if (!tween.IsValid())
                return;
        }

        tweens.Add(tween);

        // enable ourself if we are not enabled
        if (!Instance.enabled) // purposely using the static instace property just once for initialization
            instance.enabled = true;

        // if the Tween isn't paused and it is a "from" tween jump directly to the start position
        if (tween is EzTween && ((EzTween)tween).fromMode && tween.state != EzTweenBase.State.Paused)
            tween.Tick(0);

        // should we start up the time scale independent update?
        if (!instance.timeScaleIndependentUpdateIsRunning && tween.updateType == EzUpdateType.TimeScaleIndependentUpdate)
            instance.StartCoroutine(instance.TimeScaleIndependentUpdate());

#if UNITY_EDITOR
        instance.gameObject.name = string.Format("EzTween ({0} tweens)", tweens.Count);
#endif
    }


    /// <summary>
    /// removes the Tween returning true if it was removed or false if it was not found
    /// </summary>
    public static bool RemoveTween(EzTweenBase tween) {
        if (tweens.Contains(tween)) {
            tweens.Remove(tween);

#if UNITY_EDITOR
            if (instance != null && tweens != null)
                instance.gameObject.name = string.Format("EzTween ({0} tweens)", tweens.Count);
#endif

            if (instance != null && tweens.Count == 0) {
                // disable ourself if we have no more tweens
                instance.enabled = false;
            }

            return true;
        }

        return false;
    }


    /// <summary>
    /// returns a list of all Tweens, TweenChains and TweenFlows with the given id
    /// </summary>
    public static List<EzTweenBase> GetTweensById(int id) {
        List<EzTweenBase> list = null;

        foreach (var tween in tweens) {
            if (tween.id == id) {
                if (list == null)
                    list = new List<EzTweenBase>();
                list.Add(tween);
            }
        }

        return list;
    }


    /// <summary>
    /// returns a list of all Tweens with the given target. TweenChains and TweenFlows can optionally
    /// be traversed and matching Tweens returned as well.
    /// </summary>
    public static List<EzTween> GetTweensByTarget(object target, bool traverseCollections = false) {
        List<EzTween> list = new List<EzTween>();

        foreach (var item in tweens) {
            // we always check Tweens so handle them first
            var tween = item as EzTween;
            if (tween != null && tween.target == target)
                list.Add(tween);

            // optionally check TweenChains and TweenFlows. if tween is null we have a collection
            if (traverseCollections && tween == null) {
                var tweenCollection = item as EzTweenCollection;
                if (tweenCollection != null) {
                    var tweensInCollection = tweenCollection.TweensWithTarget(target);
                    if (tweensInCollection.Count > 0)
                        list.AddRange(tweensInCollection);
                }
            }
        }

        return list;
    }


    /// <summary>
    /// kills all tweens with the given target by calling the destroy method on each one
    /// </summary>
    public static void KillTweensByTarget(object target) {
        foreach (var tween in GetTweensByTarget(target, true))
            tween.Destroy();
    }

    #endregion

}
