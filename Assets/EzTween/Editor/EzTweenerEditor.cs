﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System.Reflection;
using System.Collections.Generic;

[CanEditMultipleObjects]
[CustomEditor(typeof(EzTweener))]
public class EzTweenerEditor : Editor {

    private EzTweener tweener;
    private System.Type targetType { get { return tweener.target.type; } }

    private ReorderableList list;

    private HashSet<System.Type> filter = new HashSet<System.Type>();

    private void SetFilter(params System.Type[] types) {
        filter.Clear();
        foreach (System.Type type in types) {
            filter.Add(type);
        }
    }

    class PropertyWapper {
        FieldInfo field;
        PropertyInfo prop;

        public System.Type type { get { return field != null ? field.FieldType : (prop != null ? prop.PropertyType : null); } }

        public PropertyWapper(System.Type type, string name) {
            field = type.GetField(name, BindingFlags.Instance | BindingFlags.Public);
            prop = type.GetProperty(name, BindingFlags.Instance | BindingFlags.Public);
        }

        public T Get<T>(object target) {
            if (field != null) {
                return (T)field.GetValue(target);
            } else if (prop != null) {
                return (T)prop.GetValue(target, null);
            }
            return default(T);
        }
    }

    private List<string> GetProperties() {
        System.Type type = targetType;
        BindingFlags flags = BindingFlags.Instance | BindingFlags.Public;
        FieldInfo[] fields = type.GetFields(flags);
        PropertyInfo[] props = type.GetProperties(flags);
        List<string> list = new List<string>();
        for (int i = 0; i < fields.Length; ++i) {
            FieldInfo field = fields[i];
            if (filter.Contains(field.FieldType)) {
                list.Add(field.Name);
            }
        }
        for (int j = 0; j < props.Length; ++j) {
            PropertyInfo prop = props[j];
            if (prop.CanRead && prop.CanWrite && filter.Contains(prop.PropertyType)) {
                list.Add(prop.Name);
            }
        }
        return list;
    }

    private string[] GetNames(List<string> properties, string current, out int index) {
        System.Type type = targetType;
        index = -1;
        string[] names = new string[properties.Count];
        for (int i = 0; i < properties.Count; ++i) {
            string name = properties[i];
            names[i] = name;
            if (index < 0 && name == current) {
                index = i;
            }
        }
        return names;
    }

    void DrawPropertyItem(Rect rect, int index, bool isActive, bool isFocused) {
        rect.y += 2;

        SerializedProperty itemProp = list.serializedProperty.GetArrayElementAtIndex(index);
        SerializedProperty nameProp = itemProp.FindPropertyRelative("name");
        SerializedProperty typeProp = itemProp.FindPropertyRelative("type");

        GUI.changed = false;
        EditorGUI.BeginDisabledGroup(nameProp.hasMultipleDifferentValues);

        string current = nameProp.stringValue;
        List<string> properties = GetProperties();
        int selectedIndex = 0;
        string[] names = GetNames(properties, current, out selectedIndex);
        Rect newRect = rect;
        newRect.width = EditorGUIUtility.labelWidth;
        newRect.height = EditorGUIUtility.singleLineHeight;
        EditorGUI.LabelField(newRect, "Property");
        newRect.x += EditorGUIUtility.labelWidth;
        newRect.width = rect.width - EditorGUIUtility.labelWidth;
        int choice = EditorGUI.Popup(newRect, selectedIndex, names);
        if (GUI.changed && choice >= 0) {
            nameProp.stringValue = properties[choice];
            typeProp.enumValueIndex = 0;
        }
        if (!string.IsNullOrEmpty(nameProp.stringValue)) {
            PropertyWapper pw = new PropertyWapper(targetType, nameProp.stringValue);
            System.Type type = pw.type;
            newRect.x = rect.x;
            newRect.y += EditorGUIUtility.singleLineHeight + 2;
            newRect.width = rect.width;
            if (type == typeof(int)) {
                SerializedProperty valueProp = itemProp.FindPropertyRelative("intValue");
                //if (typeProp.enumValueIndex != 1) {
                //    typeProp.enumValueIndex = 1;
                //    valueProp.intValue = pw.Get<int>(targetObject);
                //}
                typeProp.enumValueIndex = 1;
                valueProp.intValue = EditorGUI.IntField(newRect, "Int", valueProp.intValue);
            } else if (type == typeof(float)) {
                SerializedProperty valueProp = itemProp.FindPropertyRelative("floatValue");
                //if (typeProp.enumValueIndex != 2) {
                //    typeProp.enumValueIndex = 2;
                //    valueProp.floatValue = pw.Get<float>(targetObject);
                //}
                typeProp.enumValueIndex = 2;
                valueProp.floatValue = EditorGUI.FloatField(newRect, "Float", valueProp.floatValue);
            } else if (type == typeof(Vector2)) {
                SerializedProperty valueProp = itemProp.FindPropertyRelative("vector2Value");
                //if (typeProp.enumValueIndex != 3) {
                //    typeProp.enumValueIndex = 3;
                //    valueProp.vector2Value = pw.Get<Vector2>(targetObject);
                //}
                typeProp.enumValueIndex = 3;
                valueProp.vector2Value = EditorGUI.Vector2Field(newRect, "Vector2", valueProp.vector2Value);
            } else if (type == typeof(Vector3)) {
                SerializedProperty valueProp = itemProp.FindPropertyRelative("vector3Value");
                //if (typeProp.enumValueIndex != 4) {
                //    typeProp.enumValueIndex = 4;
                //    valueProp.vector3Value = pw.Get<Vector3>(targetObject);
                //}
                typeProp.enumValueIndex = 4;
                valueProp.vector3Value = EditorGUI.Vector3Field(newRect, "Vector3", valueProp.vector3Value);
            } else if (type == typeof(Color)) {
                SerializedProperty valueProp = itemProp.FindPropertyRelative("colorValue");
                //if (typeProp.enumValueIndex != 5) {
                //    typeProp.enumValueIndex = 5;
                //    valueProp.colorValue = pw.Get<Color>(targetObject);
                //}
                typeProp.enumValueIndex = 5;
                valueProp.colorValue = EditorGUI.ColorField(newRect, "Color", valueProp.colorValue);
            }
        }
        newRect.x = rect.x;
        newRect.y += EditorGUIUtility.singleLineHeight + 2;
        newRect.width = rect.width;
        SerializedProperty relativeProp = itemProp.FindPropertyRelative("relative");
        relativeProp.boolValue = EditorGUI.Toggle(newRect, "Relative", relativeProp.boolValue);

        EditorGUI.EndDisabledGroup();
    }

    private void OnEnable() {
        tweener = target as EzTweener;
        SetFilter(typeof(int), typeof(float), typeof(Vector2), typeof(Vector3), typeof(Color));
        list = new ReorderableList(serializedObject, serializedObject.FindProperty("propertyItems"));
        list.elementHeight = EditorGUIUtility.singleLineHeight * 3 + 2 * 4;
        list.drawElementCallback = DrawPropertyItem;
    }

    public override void OnInspectorGUI() {
        serializedObject.Update();

        EditorGUILayout.PropertyField(serializedObject.FindProperty("target"));
        if (targetType != null) list.DoLayoutList();
        EditorGUILayout.PropertyField(serializedObject.FindProperty("duration"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("delay"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("easeType"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("loopType"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("loops"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("timeScale"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("fromMode"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("playOnStart"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("playDelay"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("playBackward"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("onInit"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("onStart"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("onUpdate"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("onComplete"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("onLoopBegin"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("onLoopEnd"));

        serializedObject.ApplyModifiedProperties();
    }

}
