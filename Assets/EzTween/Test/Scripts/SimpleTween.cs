using UnityEngine;
using System.Collections;


public class SimpleTween : BaseDemoGUI {
    public Transform cube;


    void Start() {
        // setup a position and color tween that loops indefinitely. this will let us play with the
        // different ease types
        _tween = EzTweenManager.To(cube, 4, new EzTweenConfig()
            .Position(new Vector3(9, 4, 0))
            .MaterialColor(Color.green)
            .SetLoops(-1, EzLoopType.PingPong));
    }

}
