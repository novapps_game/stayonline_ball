﻿using UnityEngine;
using System.Collections;
using System;

public class TweenScale : Tween {

    public Vector3 scale = Vector3.one;

    public override void PlayNow() {
        iTween.ScaleTo(gameObject, iTween.Hash("scale", scale,
            "time", time, "delay", delay, "easetype", easeType, "looptype", loopType,
            "onupdate", "OnUpdate", "oncomplete", "OnComplete", "oncompleteparams", this));
    }

    public override string Type() {
        return "scale";
    }
    
}
