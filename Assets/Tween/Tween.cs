﻿using UnityEngine;
using System.Collections;
using System;

public abstract class Tween : MonoBehaviour {

    public bool playOnStart = true;
    public bool destroyOnComplete = false;

    public float time = 1;
    public float delay = 0;
    public float playDelay = 0;

    public iTween.EaseType easeType = iTween.EaseType.linear;
    public iTween.LoopType loopType = iTween.LoopType.none;

    public int loopTimes = 0;

    public TweenEvents.OnUpdateEvent onUpdate;
    public TweenEvents.OnCompleteEvent onComplete;
    public TweenEvents.OnCompleteEvent onAllComplete;

    private int playTimes = 0;

    // Use this for initialization
    protected virtual void Start() {
	    if (playOnStart) {
            playTimes = 0;
            Play();
        }
	}

    IEnumerator PlayDelayed() {
        yield return new WaitForSeconds(playDelay);
        PlayNow();
    }

    public virtual void Play() {
        if (playDelay > 0) {
            StartCoroutine(PlayDelayed());
        } else {
            PlayNow();
        }
    }

    public abstract void PlayNow();

    public abstract string Type();

    public void Stop() {
        playTimes = 0;
        iTween.Stop(gameObject, Type());
    }

    public void Pause() {
        iTween.Pause(gameObject, Type());
    }

    public void Resume() {
        iTween.Resume(gameObject, Type());
    }

    protected virtual void OnUpdate() {
        if (onUpdate != null) {
            onUpdate.Invoke(gameObject);
        }
    }

    protected virtual void OnComplete(Tween tween) {
        if (tween != this) return;
        if (onComplete != null) {
            onComplete.Invoke(gameObject);
        }
        if (loopType != iTween.LoopType.none) {
            if (loopTimes > 0) {
                if (++playTimes >= loopTimes) {
                    Stop();
                    if (onAllComplete != null) {
                        onAllComplete.Invoke(gameObject);
                    }
                }
            }
        } else if (destroyOnComplete) {
            Destroy(gameObject);
        }
    }
}
