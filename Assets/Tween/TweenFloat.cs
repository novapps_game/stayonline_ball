﻿using System;
using UnityEngine.Events;

public class TweenFloat : TweenValue<float> {

    [Serializable]
    public class OnValueUpdateEvent : UnityEvent<float> { }

    public OnValueUpdateEvent onValueUpdate;

    protected override void OnValueUpdate(float value) {
        onValueUpdate.Invoke(value);
    }
}
