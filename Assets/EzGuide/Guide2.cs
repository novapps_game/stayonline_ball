﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Guide2 : Guide {

    public override bool IsTimeNow() {
        return GameManager.instance.canguidestep4 && !IsCompleted() && GameManager.instance.currentScene == "Game";
    }

}
