﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuideStep1 : GuideStep {

    public override bool IsCompleted() {
        return GameManager.instance.guidestep1;
    }
    private int dialogueIndex = 0;

    // Use this for initialization
    void Start () {
        dialogues[0].SetActive(true);
        dialogues[1].SetActive(false);
        dialogues[2].SetActive(false);
        dialogues[3].SetActive(false);
        dialogues[4].SetActive(false);
        dialogues[5].SetActive(false);
        GameController.instance.isGameOver = true;
    }
	
    public GameObject[] dialogues;

    public void NextDialogue() {
        if(dialogueIndex < dialogues.Length-1) {
            dialogueIndex++;
            for(int i = 0; i < dialogues.Length; i++) {
                dialogues[i].SetActive(false);
            }
            if(dialogueIndex == 3) {
                dialogues[2].SetActive(true);
            }
            dialogues[dialogueIndex].SetActive(true);
        } else {
            GameManager.instance.GuideStep1Comple();
        }
       
    }
}
