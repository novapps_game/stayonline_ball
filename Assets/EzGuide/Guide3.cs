﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Guide3 : Guide {

    public override bool IsTimeNow() {
        return GameManager.instance.canguidestep6 && !IsCompleted() && GameManager.instance.currentScene == "Game";
    }

}
