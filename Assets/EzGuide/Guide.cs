﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Guide : MonoBehaviour {

    private GuideStep[] steps {
        get {
            if (_steps == null) {
                _steps = GetComponentsInChildren<GuideStep>(true);
            }
            return _steps;
        }
    }
    private GuideStep[] _steps;
    private int stepIndex;

    private GuideStep currentStep { get { return stepIndex < steps.Length ? steps[stepIndex] : null; } }

    void OnEnable() {
        if (currentStep != null) {
            currentStep.gameObject.SetActive(true);
        }
    }

    void Update() {
        if (currentStep != null && !currentStep.gameObject.activeSelf) {
            if (++stepIndex >= steps.Length) {
                gameObject.SetActive(false);

            } else {
                currentStep.gameObject.SetActive(true);
            }
        }
    }

    public virtual bool IsTimeNow() {
        return GameManager.instance.currentScene == "Game" && !IsCompleted();
    }

    public virtual bool IsCompleted() {
        foreach (GuideStep step in steps) {
            if (!step.IsCompleted()) {
                return false;   
            }
        }
        return true;
    }
}
