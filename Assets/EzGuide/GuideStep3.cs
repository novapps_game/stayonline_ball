﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GuideStep3 : GuideStep {

    public GameObject Handle;

    public override bool IsCompleted() {
        return GameManager.instance.guidestep3;
    }
    // Use this for initialization
    void Start () {
        MoveHandle();
        GameController.instance.isGameOver = false;
    }
	
    private void MoveHandle() {
        Handle.transform.DOLocalMoveX(55f, 0.5f);
        EzTiming.CallDelayed(0.51f, () => {
            Handle.transform.DOLocalMoveX(-55f, 0.5f);
            EzTiming.CallDelayed(0.51f, () => {
                MoveHandle();
            });
        });
    }
    bool isClickComple = false;
    public void GuideStep3Comple() {
        if (!isClickComple) {
            EzTiming.CallDelayed(3f, () => {
                GameManager.instance.GuideStep3Comple();
                GameManager.instance.CanGuideStep4();
            });
        }
        isClickComple = true;
    }
	
}
