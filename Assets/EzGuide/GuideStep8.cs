﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuideStep8 : GuideStep {

    private void Start() {
        //GameController.instance.isGameOver = true;
    }

    public override bool IsCompleted() {
        return GameManager.instance.guidestep8;
    }

    public void GuideStep8Comple() {
        GameManager.instance.GuideStep8Comple();
    }
   
}

