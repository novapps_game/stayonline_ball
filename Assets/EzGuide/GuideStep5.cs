﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GuideStep5 : GuideStep {

    public GameObject Handle;

    public override bool IsCompleted() {
        return GameManager.instance.guidestep5;
    }

    // Use this for initialization
    void Start () {
        ClickHandle();
    }
	


    private void ClickHandle() {
        Handle.transform.DOLocalRotate(new Vector3(30,0,0), 0.5f);
        EzTiming.CallDelayed(0.51f, () => {
            Handle.transform.DOLocalRotate(new Vector3(-30, 0, 0), 0.5f);
            EzTiming.CallDelayed(0.51f, () => {
                ClickHandle();
            });
        });
    }

    bool isClickComple = false;
    public void GuideStep5Comple() {
        if (!isClickComple) {
            EzTiming.CallDelayed(3f, () => {
                GameManager.instance.GuideStep5Comple();
                GameManager.instance.CanGuideStep6();
            });
        }
        isClickComple = true;
    }
}
