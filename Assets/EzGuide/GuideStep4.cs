﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuideStep4 : GuideStep {
    public override bool IsCompleted() {
        return GameManager.instance.guidestep4;
    }

    // Use this for initialization
    void Start () {
		
	}
    bool isClickComple = false;
    public void GuideStep4Comple() {
        if (!isClickComple) {
            GameManager.instance.GuideStep4Comple();
        }
        isClickComple = true;
    }
}
