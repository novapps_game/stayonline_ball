﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuideStep2 : GuideStep {
    public override bool IsCompleted() {
        return GameManager.instance.guidestep2;
    }

    public void GuideStep2Comple() {
        GameManager.instance.GuideStep2Comple();
    }

}
