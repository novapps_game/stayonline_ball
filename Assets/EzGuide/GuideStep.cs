﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public abstract class GuideStep : MonoBehaviour {

    public string[] targetPaths;
    public string hintPrefabPath;

    protected GameObject hint;
    protected List<GameObject> hilightTargets = new List<GameObject>();

    void Update() {
        if (IsCompleted()) {
            GuideMask.Hide();
            Destroy(hint);
            hint = null;
            gameObject.SetActive(false);
            foreach (GameObject target in hilightTargets) {
                OnUnhilightTarget(target);
            }
            hilightTargets.Clear();
        } else if (hint == null) {
            foreach (string path in targetPaths) {
                GameObject target = GameObject.Find(path);
                if (target != null) {
                    OnHilightTarget(target);
                }
            }
            if (hilightTargets.Count == targetPaths.Length) {
                GuideMask.Show();
                hint = GameManager.instance.Instantiate(hintPrefabPath);
                if (hint != null) {
                    hint.transform.SetParent(GuideMask.GetInstance().transform, false);
                }
            }
        }
    }

    public abstract bool IsCompleted();

    protected virtual void OnHilightTarget(GameObject target) {
        Canvas canvas = target.AddComponent<Canvas>();
        if (canvas != null) {
            canvas.overrideSorting = true;
            canvas.sortingOrder = 5;
            target.AddComponent<GraphicRaycaster>();
            hilightTargets.Add(target);
        }
    }

    protected virtual void OnUnhilightTarget(GameObject target) {
        Destroy(target.GetComponent<GraphicRaycaster>());
        Destroy(target.GetComponent<Canvas>());
    }
}
