﻿using UnityEngine;

public class GuideMask : PrefabSingleton<GuideMask> {

    public static void Show() {
        instance.gameObject.SetActive(true);
    }
        
    public static void Hide() {
        instance.gameObject.SetActive(false);
    }
    
}
