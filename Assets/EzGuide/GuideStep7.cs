﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GuideStep7 : GuideStep {


    public GameObject Handle;

    public override bool IsCompleted() {
        return GameManager.instance.guidestep7;
    }

    private void Start() {
        MoveHandle();
    }


    private void MoveHandle() {
        Handle.transform.DOLocalMoveY(58, 0.5f);
        EzTiming.CallDelayed(0.51f, () => {
            Handle.transform.DOLocalMoveY(0, 0.5f);
            EzTiming.CallDelayed(0.51f, () => {
                MoveHandle();
            });
        });
    }
}
