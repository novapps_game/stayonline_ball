﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuideStep6 : GuideStep {
    private void Start() {
        GameController.instance.isGameOver = true;
    }

    public override bool IsCompleted() {
        return GameManager.instance.guidestep6;
    }

    public void GuideStep6Comple() {
        GameManager.instance.GuideStep6Comple();
    }
}
