﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GuideStep9 : GuideStep {

 
    public GameObject Handle;

    public override bool IsCompleted() {
        //if (GameManager.instance.ballAttackLevel > 1 && GameController.instance != null) {
        //    GameController.instance.isGameOver = false;
        //}
        return GameManager.instance.ballAttackLevel > 1;
    }

    private void Start() {
        MoveHandle();
    }


    private void MoveHandle() {
        Handle.transform.DOLocalMoveY(-620, 0.5f);
        EzTiming.CallDelayed(0.51f, () => {
            Handle.transform.DOLocalMoveY(-598, 0.5f);
            EzTiming.CallDelayed(0.51f, () => {
                MoveHandle();
            });
        });
    }

}
