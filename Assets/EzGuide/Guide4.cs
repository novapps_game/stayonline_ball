﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Guide4 : Guide {
    public override bool IsTimeNow() {
        return GameManager.instance.canguidestep8 && !IsCompleted() && GameManager.instance.currentScene == "Game";
    }

}
