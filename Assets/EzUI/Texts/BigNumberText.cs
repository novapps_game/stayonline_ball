﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class BigNumberText : LerpDouble {

    public string format = "{0}";
    public string formatKey;

    public bool displayPositiveSymbol;

    private Text text;
    private ContentSizeFitter fitter;

    void Awake() {
        if (!string.IsNullOrEmpty(formatKey)) {
            format = Localization.GetMultilineText(formatKey);
            formatKey = "";
        }
        text = GetComponent<Text>();
        fitter = GetComponent<ContentSizeFitter>();
        UpdateText();
    }

    protected virtual void UpdateText() {
        if (text != null) {
            string newText = GetTextOfValue();
            if (text.text != newText) {
                text.text = newText;
                if (fitter != null) {
                    fitter.SetLayoutHorizontal();
                }
            }
        }
    }

    protected virtual string GetTextOfValue() {
        return (displayPositiveSymbol && value > 0)
            ? string.Format(format, "+" + Digit.Format(value))
            : string.Format(format, Digit.Format(value));
    }

    protected override void OnValueChanging() {
        UpdateText();
        base.OnValueChanging();
    }

}
