﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class BestScore : MonoBehaviour {

    public bool updatePerFrame = true;
    private IntText intText;

    void Awake() {
        intText = GetComponent<IntText>();
        intText.SetValue(GameManager.instance.bestScore);
    }

    void OnEnable() {
        UpdateText();
    }

    // Update is called once per frame
    void Update() {
        if (updatePerFrame) {
            UpdateText();
        }
    }

    void UpdateText() {
        intText.targetValue = GameManager.instance.bestScore;
    }
}
