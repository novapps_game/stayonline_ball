﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tips : MonoBehaviour {

    public string tipsPrefix = "Tips";
    public int maxTips = 1;

	void OnEnable() {
        string tipsKey = tipsPrefix + Random.Range(1, maxTips + 1);
        GetComponent<Text>().text = Localization.GetMultilineText(tipsKey);
    }
}
