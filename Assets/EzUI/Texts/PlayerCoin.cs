﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(IntText))]
public class PlayerCoin : MonoBehaviour {

    public bool updatePerFrame = true;
    private IntText intText;
    private BigNumberText bigNum;

    void Awake() {
        intText = GetComponent<IntText>();
        bigNum = GetComponent<BigNumberText>();
        //intText.SetValue(GameManager.instance.coins);
        bigNum.SetValue(GameManager.instance.coins);
    }

    void OnEnable() {
        intText.SetValue(GameManager.instance.coins);
        UpdateText();
    }

    // Update is called once per frame
    void Update() {
        if (updatePerFrame) {
            UpdateText();
        }
    }

    void UpdateText() {
        //intText.targetValue = GameManager.instance.coins;
        bigNum.SetValue(GameManager.instance.coins); 
    }
}
