﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class IntText : LerpInt {

    public string format = "{0:D}";
    public string formatKey;

    private Text text;
    private ContentSizeFitter fitter;

    protected virtual void Awake() {
        if (!string.IsNullOrEmpty(formatKey)) {
            format = Localization.GetMultilineText(formatKey);
            formatKey = "";
        }
        text = GetComponent<Text>();
        fitter = GetComponent<ContentSizeFitter>();
        UpdateText();
    }

    protected virtual void UpdateText() {
        if (text != null) {
            string newText = GetTextOfValue();
            if (text.text != newText) {
                text.text = newText;
                if (fitter != null) {
                    fitter.SetLayoutHorizontal();
                }
            }
        }
    }

    protected virtual string GetTextOfValue() {
        return string.Format(format, value);
    }

    protected override void OnValueChanging() {
        UpdateText();
        base.OnValueChanging();
    }

}
