﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CountdownTime : MonoBehaviour {

    public int initSeconds = 300;
    public string textFormat = "{0}";
    public string textFormatKey;
    public int warnSeconds = 10;
    public Color warnColor = Color.red;
    public string warnSound;
    public bool launchOnAwake = true;

    [System.Serializable]
    public class FinishEvent : UnityEvent { }
    public FinishEvent onFinish;

    public int seconds;
    private Text text;
    private Color originColor;
    public float countTime = 0;
    private bool running = false;
    private bool warning = false;

	void Awake() {
        text = GetComponent<Text>();
        if (!string.IsNullOrEmpty(textFormatKey)) {
            textFormat = Localization.GetMultilineText(textFormatKey);
        }
        originColor = text.color;
        if (launchOnAwake) {
            Launch();
        }
    }

    public void Reset() {
        //seconds = initSeconds;
        warning = false;
        text.color = originColor;
        UpdateText();
        countTime = 0;
    }

    public void Launch() {
        Reset();
        running = true;
        warning = false;
    }

    void Update() {
        if (running) {
            countTime += Time.deltaTime;
            if (countTime >= 1) {
                countTime -= 1;
                Count();
                if (seconds <= 0) {
                    running = false;
                    iTween.Stop(gameObject);
                    text.color = originColor;
                    if (onFinish != null) {
                        onFinish.Invoke();
                    }
                }
            }
        }
    }

    public void Pause() {
        running = false;
    }

    public void Resume() {
        running = true;
    }

    public void Stop() {
        running = false;
        Reset();
    }

    void Count() {
        if (--seconds < warnSeconds && !warning) {
            warning = true;
            iTween.FadeTo(gameObject, iTween.Hash("color", warnColor, "time", 0.2f,
                "easetype", iTween.EaseType.easeInOutSine, "looptype", iTween.LoopType.pingPong));
        }
        if (warning && !string.IsNullOrEmpty(warnSound)) {
            GameManager.instance.PlaySound(warnSound);
        }
        if (!warning) {
            iTween.FadeTo(gameObject, iTween.Hash("color", Color.white, "time", 0.2f,
               "easetype", iTween.EaseType.easeInOutSine, "looptype", iTween.LoopType.pingPong));
        }
        UpdateText();
    }

    void UpdateText() {
        System.TimeSpan ts = new System.TimeSpan(0, 0, Mathf.RoundToInt(seconds));
        if (ts.Hours > 0) {
            text.text = string.Format(textFormat, ts.ToString());
        } else {
            text.text = string.Format(textFormat, ts.ToString().Substring(3));
        }
    }
}
