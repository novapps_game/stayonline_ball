﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class ProgressText : FloatText {

    public float maxValue;

    public float progress { get { return value / maxValue; } }

    public void SetProgress(float current) {
        SetValue(Mathf.Clamp(current, 0f, maxValue));
    }

    public void SetProgress(float current, float max) {
        maxValue = max;
        SetValue(Mathf.Clamp(current, 0f, maxValue));
    }

    protected override string GetTextOfValue() {
        return string.Format(format, value, maxValue);
    }
}
