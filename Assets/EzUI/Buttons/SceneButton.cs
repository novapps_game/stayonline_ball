﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneButton : MonoBehaviour {

	public string targetScene = "Game";
    public bool reload = false;
    public bool useLoadingScreen = false;

	// Use this for initialization
	void Start() {
		GetComponent<Button>().onClick.AddListener(() => {
            if (reload) {
                GameManager.instance.ReloadScene(useLoadingScreen);
            } else {
                GameManager.instance.LoadScene(targetScene, useLoadingScreen);
            }
		});
	}

	// Update is called once per frame
	void Update() {
		
	}
}
