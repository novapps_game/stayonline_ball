﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementButton : MonoBehaviour {

    // Use this for initialization
    void Start() {
        GetComponent<Button>().onClick.AddListener(() => {
            GameManager.ShowAchievements();
        });
    }

    // Update is called once per frame
    void Update() {

    }
}
