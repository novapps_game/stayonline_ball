﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RevivePanel : Panel {

	public void Revive() {
		OnClick("Revive");
        Time.timeScale = 0;
        EzAds.ShowRewardedVideo(OnVideoRewarded, OnVideoCanceled);
	}

    void OnVideoRewarded() {
        Close();
        Debug.Log("video rewarded for revive");
        EzAnalytics.LogEvent("Revive", "Succeeded");
        GameController.GetInstance().Revive();
    }

    void OnVideoCanceled() {
        Cancel();
        Debug.Log("video canceled for revive");
        EzAnalytics.LogEvent("Revive", "Failed");
    }

	protected override void OnCancel() {
		base.OnCancel();
        GameManager.instance.GameOver();
	}

    protected override void OnClose() {
        Time.timeScale = 1;
        base.OnClose();
    }
}
