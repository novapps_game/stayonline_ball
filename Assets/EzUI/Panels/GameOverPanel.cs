﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class GameOverPanel : Panel {

    private Transform noAdsButton;

    public TweenMove tm;

    protected override void OnAwake() {
        base.OnAwake();
        noAdsButton = transform.Find("NoAds");
    }

    protected override void OnUpdate() {
        base.OnUpdate();
		if (noAdsButton != null) {
        	noAdsButton.gameObject.SetActive(!GameManager.instance.noAds);
		}
    }

    public void ClickConfirm() {
        tm.Play();
        tm.transform.GetComponent<Image>().DOFade(0, 1);
        tm.transform.DOScale(0, 2.5f);
        //LevelUpManager.instance.ShakeAnima();
        EzTiming.CallDelayed(1.1f, () => {
            GameController.instance.ShowReBoss();
            Panel.Close("GameOverPanel");
        });
        Boss.instance.FailNextBoss();
        GameController.instance.isGameOver = false;
        //LevelUpManager.instance.ShowCanLevelUpHint();
    }
}
