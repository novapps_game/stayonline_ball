﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Panel : MonoBehaviour, IPointerDownHandler {

    public static string parentName = "Canvas";

	public static bool IsOpened(string name) {
        GameObject parent = GameObject.Find(parentName);
        if (parent == null) return false;
		Transform transform = parent.transform.Find(name);
		return transform != null;
	}

	public static Panel Open(GameObject prefab, Transform parent = null, System.Action<string> onClick = null) {
        if (parent == null) {
            GameObject parentObj = GameObject.Find(parentName);
            if (parentObj == null) return null;
            parent = parentObj.transform;
        }
		Transform transform = parent.Find(prefab.name);
        if (transform != null) {
            transform.SetAsLastSibling();
        } else if (prefab != null) {
			transform = Instantiate(prefab).transform;
			transform.gameObject.name = prefab.name;
			transform.SetParent(parent, false);
		}
        if (transform == null) return null; 
		transform.gameObject.SetActive(false);

		Panel panel = transform.GetComponent<Panel>();
		panel.Open(onClick);

		return panel;
	}

	public static Panel Open(string name, Transform parent = null, System.Action<string> onClick = null) {
        if (parent == null) {
            GameObject parentObj = GameObject.Find(parentName);
            if (parentObj == null) return null;
            parent = parentObj.transform;
        }
        Transform transform = parent.Find(name);
        if (transform != null) {
            transform.SetAsLastSibling();
        } else {
            GameObject panelObject = GameManager.instance.Instantiate(name);
            if (panelObject == null) {
                return null;
            }
			transform = panelObject.transform;
			transform.SetParent(parent, false);
		}
        if (transform == null) {
            Debug.LogWarning("Cannot find " + name + " prefab!");
            return null;
        }
		transform.gameObject.SetActive(false);
		Panel panel = transform.GetComponent<Panel>();
		panel.Open(onClick);
		return panel;
	}

	public static void Close(string name, Transform parent = null) {
        if (parent == null) {
            GameObject parentObj = GameObject.Find(parentName);
            if (parentObj == null) return;
            parent = parentObj.transform;
        }
        Transform transform = parent.Find(name);
		if (transform != null) {
			Panel panel = transform.GetComponent<Panel>();
			panel.Close();
		}
	}

    public static void Cancel(string name, Transform parent = null) {
        if (parent == null) {
            GameObject parentObj = GameObject.Find(parentName);
            if (parentObj == null) return;
            parent = parentObj.transform;
        }
        Transform transform = parent.Find(name);
        if (transform != null) {
            Panel panel = transform.GetComponent<Panel>();
            panel.Cancel();
        }
    }

    public static bool CancelByBack(Transform parent = null) {
        if (parent == null) {
            GameObject parentObj = GameObject.Find(parentName);
            if (parentObj == null) return false;
            parent = parentObj.transform;
        }
        Panel[] panels = parent.GetComponentsInChildren<Panel>();
		for (int i = panels.Length - 1; i >= 0; --i) {
			Panel panel = panels[i];
			if (panel.cancelByBack) {
				panel.Cancel();
				return true;
			}
		}
		return false;
	}

    public static void CheckBackButton(System.Action finalAction = null) {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            if (!CancelByBack()) {
                if (finalAction != null) {
                    finalAction.Invoke();
                }
            }
        }
    }

	public bool destroyOnClosed = true;
	public bool cancelByBack = true;
	public bool cancelByClickBlank = false;

    protected System.Action<string> onClick;

	void Awake() {
		OnAwake();
	}

	// Use this for initialization
	void Start() {
		OnStart();
	}

	// Update is called once per frame
	void Update() {
		OnUpdate();
	}

	protected virtual void OnAwake() {
	}

	protected virtual void OnStart() {
	}

	protected virtual void OnUpdate() {
	}

	public virtual bool IsOpened() {
		return gameObject.activeSelf;
	}

	public virtual void Open(System.Action<string> onClick = null) {
        this.onClick = onClick;
		if (!IsOpened()) {
			OnOpen();
			gameObject.SetActive(true);
            transform.SetAsLastSibling();
		}
	}

	public virtual void Close() {
		if (IsOpened()) {
			OnClose();
			if (destroyOnClosed) {
				Destroy(gameObject);
			} else {
				gameObject.SetActive(false);
			}
		}
	}

	public virtual void Cancel() {
		if (IsOpened()) {
			OnCancel();
			Close();
		}
	}

	protected virtual void OnOpen() {
		EzAnalytics.LogEvent(name, "Open");
	}

	protected virtual void OnClose() {
		EzAnalytics.LogEvent(name, "Close");
	}

	protected virtual void OnCancel() {
		OnClick("Cancel");
	}

	public void OnClick(string button) {
		EzAnalytics.LogEvent(name, "Click", button);
        if (onClick != null) {
            onClick.Invoke(button);
        }
	}

	public virtual void OnPointerDown(PointerEventData eventData) {
		if (cancelByClickBlank && eventData.pointerEnter == gameObject) {
			Cancel();
		}
	}
}
