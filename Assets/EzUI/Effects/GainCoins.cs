﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GainCoins : EzPool<CoinEffect, GainCoins> {

    public Transform target;

    public float explodeDelay = 0f;
    public float explodeTime = 0.5f;
    public float moveDelay = 0.5f;
    public float moveInterval = 0.01f;
    public float moveTime = 1f;
    public float scaleTime = 0.5f;
    public float minScale = 1f;
    public float maxScale = 2f;
    public float endScale = 0.8f;
    public float spawnInterval = 0f;
    public float minSpawnRadius = 10f;
    public float maxSpawnRadius = 150f;

    private int effectCount;

    public void Gain(int coins, int coinsPerEffect, string reason = "RewardedVideo") {
        StartCoroutine(SpawnLoop(coins, coinsPerEffect, reason));
    }

    IEnumerator SpawnLoop(int coins, int coinsPerEffect, string reason) {
        effectCount = 0;
        int effects = coins / coinsPerEffect;
        while (effectCount++ < effects) {
            if (spawnInterval > 0) {
                yield return new WaitForSeconds(spawnInterval);
            }
            SpawnEffect(coinsPerEffect, reason);
        }
        int leftCoins = coins % coinsPerEffect;
        if (leftCoins > 0) {
            if (spawnInterval > 0) {
                yield return new WaitForSeconds(spawnInterval);
            }
            SpawnEffect(leftCoins, reason);
        }
    }

    void SpawnEffect(int coins, string reason) {
        CoinEffect effect = Create();
        effect.Play(coins, reason, Vector2.zero.RandomPositionInSector(maxSpawnRadius, minSpawnRadius), 
            Vector3.one * Random.Range(minScale, maxScale), target.position, Vector3.one * endScale, 
            explodeDelay, explodeTime, moveDelay + moveInterval * effectCount, moveTime, scaleTime);
    }
}
