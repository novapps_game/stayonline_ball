﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinEffect : MonoBehaviour {

    public GameObject[] iconSprites;

    public Image icon;

    private RectTransform rectTransform;
    private int coins;
    private string reason;

    private float moveDelay;
    private float moveTime;
    private Vector3 toPosition;
    private float scaleTime;
    private Vector3 toScale;

    void Awake() {
        rectTransform = GetComponent<RectTransform>();
    }

    public void Play(int coins, string reason, Vector2 fromPosition, Vector3 fromScale, 
        Vector3 toPosition, Vector3 toScale, float explodeDelay, float explodeTime, 
        float moveDelay, float moveTime, float scaleTime) {
        icon.sprite = EzRandom.Select(iconSprites).GetComponent<SpriteRenderer>().sprite;
        this.coins = coins;
        this.reason = reason;
        this.toPosition = toPosition;
        this.toScale = toScale;
        this.moveDelay = moveDelay;
        this.moveTime = moveTime;
        this.scaleTime = scaleTime;
        rectTransform.localPosition = Vector3.zero;
        rectTransform.localScale = Vector3.one * 0.1f;
        Explode(fromPosition, fromScale, explodeTime, explodeDelay);
        
    }

    void Explode(Vector2 position, Vector3 scale, float time, float delay) {
        iTween.MoveUITo(gameObject, iTween.Hash("position", position, "time", time,
            "delay", delay, "easetype", iTween.EaseType.easeOutCirc));
        iTween.ScaleTo(gameObject, iTween.Hash("scale", scale, "time", time,
            "delay", delay, "oncomplete", "MoveAndScale"));
    }

    void MoveAndScale() {
        iTween.Stop(gameObject);
        iTween.MoveTo(gameObject, iTween.Hash("position", toPosition, "time", moveTime,
            "delay", moveDelay, "easetype", iTween.EaseType.easeOutQuart));
        iTween.ScaleTo(gameObject, iTween.Hash("scale", toScale, "time", scaleTime,
            "delay", moveDelay, "oncomplete", "GainCoins"));
    }

    void GainCoins() {
        GameManager.instance.GainCoins(coins, reason);
        iTween.Stop(gameObject);
        gameObject.SetActive(false);
        GameManager.instance.PlaySound("gain_coin");
    }
}
