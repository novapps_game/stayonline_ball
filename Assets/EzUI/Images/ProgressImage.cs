﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class ProgressImage : LerpFloat {

    protected Image image;

    protected virtual void Awake() {
        image = GetComponent<Image>();
        image.type = Image.Type.Filled;
    }

    protected override void OnValueChanging() {
        image.fillAmount = value;
        base.OnValueChanging();
    }
}
