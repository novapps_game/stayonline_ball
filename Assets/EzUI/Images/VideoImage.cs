﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoImage : MonoBehaviour {

    public string url;
    public bool autoPlayOnPrepared = true;
    public bool audioEnabled = true;
    public bool isLooping = true;
    public VideoAspectRatio aspectRatio = VideoAspectRatio.FitInside;

    public bool runInBackground;

    [SerializeField]
    private UnityEvent onVideoPrepared = new UnityEvent();

    [SerializeField]
    private UnityEvent onVideoFinished = new UnityEvent();

    [System.Serializable]
    public class VideoPlayingEvent : UnityEvent<int, int> { }
    [SerializeField]
    private VideoPlayingEvent onVideoPlaying = new VideoPlayingEvent();

    public bool isPrepared { get { return videoPlayer != null && videoPlayer.isPrepared; } }
    public bool isPlaying { get { return videoPlayer != null && videoPlayer.isPlaying; } }

    public int videoWidth { get { return (videoPlayer != null && videoPlayer.texture != null) ? videoPlayer.texture.width : 0; } }
    public int videoHeight { get { return (videoPlayer != null && videoPlayer.texture != null) ? videoPlayer.texture.height : 0; } }
    public int videoLength { get { return isPrepared ? Mathf.RoundToInt(videoPlayer.frameCount / videoPlayer.frameRate) : 0; } }

    private RectTransform rectTransform;
    private VideoPlayer videoPlayer;
    private AudioSource audioSource;
    private RawImage rawImage;

    private WaitForSeconds waitTime = new WaitForSeconds(1);

    // Use this for initialization
    protected virtual void Awake() {
        rectTransform = GetComponent<RectTransform>();
        Application.runInBackground = runInBackground;
        videoPlayer = gameObject.AddComponent<VideoPlayer>();
        videoPlayer.playOnAwake = false;
        videoPlayer.source = VideoSource.Url;
        videoPlayer.isLooping = isLooping;
        videoPlayer.loopPointReached += OnVideoFinished;
        videoPlayer.waitForFirstFrame = true;
        videoPlayer.aspectRatio = aspectRatio;
        if (audioEnabled) {
            audioSource = gameObject.AddComponent<AudioSource>();
            audioSource.playOnAwake = false;
            audioSource.Pause();
            videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;
            videoPlayer.SetTargetAudioSource(0, audioSource);
            videoPlayer.EnableAudioTrack(0, true);
            videoPlayer.controlledAudioTrackCount = 1;
        }
	}

    protected virtual void Start() {
        Prepare();
    }

    public void Prepare() {
        if (!string.IsNullOrEmpty(url)) {
            videoPlayer.url = url;
            videoPlayer.Prepare();
            StartCoroutine(PreparingVideo());
        }
    }

    public void Prepare(string url) {
        this.url = url;
        Prepare();
    }
	
	IEnumerator PreparingVideo() {
        int seconds = 0;
        while (!videoPlayer.isPrepared) {
            yield return waitTime;
            Debug.Log("Preparing video ... " + (++seconds) + "s");
        }
        yield return waitTime;
        OnVideoPrepared();
    }

    protected virtual void OnVideoPrepared() {
        Debug.Log("Video prepared.");
        videoPlayer.targetTexture = new RenderTexture((int)rectTransform.sizeDelta.x, (int)rectTransform.sizeDelta.y, 24);
        if (rawImage == null) {
            rawImage = gameObject.AddComponent<RawImage>();
        }
        rawImage.texture = videoPlayer.targetTexture;
        onVideoPrepared.Invoke();
        if (autoPlayOnPrepared) {
            Play();
        }
    }

    public void Play() {
        if (!isPlaying) {
            if (isPrepared) {
                videoPlayer.Play();
                if (audioSource != null) {
                    audioSource.Play();
                }
                StartCoroutine(PlayingVideo());
            } else {
                Prepare();
                autoPlayOnPrepared = true;
            }
        }
    }

    IEnumerator PlayingVideo() {
        int length = videoLength;
        Debug.Log("Start playing video of length: " + length);
        while (videoPlayer != null && videoPlayer.isPlaying) {
            OnVideoPlaying(Mathf.RoundToInt((float)videoPlayer.time), length);
            yield return waitTime;
        }
    }

    protected virtual void OnVideoPlaying(int current, int length) {
        System.TimeSpan curSpan = new System.TimeSpan(current * System.TimeSpan.TicksPerSecond);
        System.TimeSpan totalSpan = new System.TimeSpan(length * System.TimeSpan.TicksPerSecond);
        Debug.Log("Video playing: (" + curSpan + "/" + totalSpan + ")");
        onVideoPlaying.Invoke(current, length);
    }

    protected virtual void OnVideoFinished(VideoPlayer vp) {
        Debug.Log("Video finished.");
        onVideoFinished.Invoke();
    }

    public void Pause() {
        if (isPlaying) {
            videoPlayer.Pause();
            if (audioSource != null) {
                audioSource.Pause();
            }
        }
    }
}
