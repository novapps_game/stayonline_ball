﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ColorProgressImage : ProgressImage {

    [System.Serializable]
    public class ColorSection : Section<Color, float> { }
    public ColorSection[] colorSections;

    private Color originColor;

    protected override void Awake() {
        base.Awake();
        originColor = image.color;
        OnValueChanging();
    }

    protected override void OnValueChanging() {
        image.color = ColorSection.GetValue(colorSections, value, FloatCondition.NoLess, originColor);
        base.OnValueChanging();
    }

}
