﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PromoVideoImage : VideoImage {

    protected override void Awake() {
        base.Awake();
       // url = GameManager.instance.promoVideo;
    }

    protected override void OnVideoPrepared() {
        base.OnVideoPrepared();
        gameObject.AddComponent<Button>().onClick.AddListener(OnClick);
    }

    void OnClick() {
        GameManager.instance.GotoPromo();
    }
}
