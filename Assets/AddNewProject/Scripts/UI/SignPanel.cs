﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SignPanel : Panel {
    public Text SignDay;


    // Use this for initialization
    void Start () {
        SignDay.text = "已连续签到" +GameManager.instance.continuousSignCount+"天";
        if(Localization.GetLanguage() == "CN") {
            SignDay.text = "已连续签到" + GameManager.instance.continuousSignCount + "天";
        } else if(Localization.GetLanguage() == "CT") {
            SignDay.text = "已連續簽到" + GameManager.instance.continuousSignCount + "天";
        } else {
            SignDay.text = "Continuous sign in " + GameManager.instance.continuousSignCount + "days";
        }
        System.DateTime currentTime = System.DateTime.Now;
        if (!GameManager.instance.IsSignedToday() && (currentTime - GameManager.instance.GetOfflineTime()).TotalMinutes >= 3) {
            Panel.Open("SignPanel");
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void UpdateSignDay() {
        if (Localization.GetLanguage() == "CN") {
            SignDay.text = "已连续签到" + GameManager.instance.continuousSignCount + "天";
        } else if (Localization.GetLanguage() == "CT") {
            SignDay.text = "已連續簽到" + GameManager.instance.continuousSignCount + "天";
        } else {
            SignDay.text = "Continuous sign in " + GameManager.instance.continuousSignCount + "days";
        }
    }

    protected override void OnClose() {
        GameManager.instance.ShowOfflinePanel();
        base.OnClose();
        
    }
}
