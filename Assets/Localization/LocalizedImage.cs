﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class LocalizedImage : MonoBehaviour {

    public LocalSprite[] localSprites;

    private Dictionary<string, Sprite> sprites;

	// Use this for initialization
	void Start() {
        sprites = new Dictionary<string, Sprite>();
        foreach (var localSprite in localSprites) {
            sprites.Add(localSprite.language.ToUpper(), localSprite.sprite);
        }
        string language = Localization.GetLanguage();
        if (sprites.ContainsKey(language)) {
            Image image = GetComponent<Image>();
            if (image != null) {
                image.sprite = sprites[language];
            } else {
                SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
                if (spriteRenderer != null) {
                    spriteRenderer.sprite = sprites[language];
                }
            }
        }
    }
}
